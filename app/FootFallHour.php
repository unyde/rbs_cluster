<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Common;
class FootFallHour extends Model
{
    use SoftDeletes;
    protected $table = 'footfall_hour';
    protected $fillable=['zone_id','start_time','end_time','first_in','last_in','ct_last_id','status','foot_fall'];
   
    /**
     * Create empty record for time interval
     */
    static function createFootFall($zone_id,$time_intervals){
        if(empty($zone_id)) return ;
        foreach($time_intervals as $time_interval){
            $start_time=$time_interval['start_time'];
            $end_time=$time_interval['end_time'];
            $row =FootFallHour::select('id')->where('zone_id',$zone_id)
                                    ->where('start_time','>=',$start_time)
                                    ->where('end_time',"<=",$end_time)
                                    ->exists();
                                    
            if($row){
                //
            }     
            else{
                $data=[];
                $data['zone_id']=$zone_id;
                $data['start_time']=$start_time;
                $data['end_time']=$end_time;
                $data['ct_last_id']=0;
                FootFallHour::create($data);
            }                   

        }
    }

    /**
     * Find average foot fall by zone id and selected date
     */
    static function averageFootFall($selectedZoneId,$selectedDate=null){
        if(empty($selectedZoneId)) return;
        if(empty($selectedDate)){
            $selectedDate=Common::currentDate();
        }

        $where=[];
        $where['zone_id']=$selectedZoneId;
        $where['status']=1;
       // $where['zone_id']=$selectedZoneId;
        
         $foot_fall=FootFallHour::where($where)
                      ->whereDate('created_at','=',$selectedDate)
                      ->avg('foot_fall');
         if($foot_fall){
             $foot_fall=round($foot_fall);
         }             
         else{
             $foot_fall="-";
         }            
         return $foot_fall;
    } 

}
