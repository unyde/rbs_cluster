<?php
namespace App\Helpers;

use Carbon\Carbon;
use Config;
use Log;
use App\Models\Settings;
use App\Models\ClusterMaster;
use App\Models\NotificationsSent;
use Storage;
function SayHello(){
    echo "ss";
}
class Common
{
    static function queryLog(){
        dd('Your code exit');
    }
    /**
     * Convert array into string with key=value&
     */
    static function getStringFromArray($data){
        $updatedData=[];
        foreach($data as $k => $v) { $updatedData[] = "$k"."="."$v"; }
        return implode('&', $updatedData);
    }

    /**
     * Check file exist or not for chart data
     * If not exist then return default file path
     */
    static function fetchFileUrl($fileName){
         $baseUrl=Config::get('app.app_storage_url');
        $fileName  = "data/$fileName";
        
        $defaultFile ="";// "storage/app/public/"; 
        if(Storage::exists("public/".$fileName)) {
           $defaultFile.= $fileName;
        }
        else{
           $defaultFile.="data/default.csv"; 
        }
        $url = route('chart_content')."?chart_file_path=".urlencode($defaultFile);
        return $url; 
        
        //echo $defaultFile; exit; 
        // return  $baseUrl.$defaultFile."?v1=".time(); 
        // echo $storagePath =url('/');
        //  exit; 
        // Storage::url($defaultFile); exit; 
        // $defaultFile=asset($defaultFile);
        // return $defaultFile; 
        
    }

    /**
     * File create for chart 
     */
    static function fileCreateOrUpdate($dataArray){
        //$id,$fileNameStart,$fileNameEnd,$branchTime,$data
        $id=@$dataArray['id'];
        $file_name_start=@$dataArray['file_name_start'];
        $file_name_end=@$dataArray['file_name_end'];
        $start_time=@$dataArray['start_time'];
        $data=@$dataArray['data'];

        $filename    =$file_name_start."_".$id."_".$file_name_end.".csv";
        $currentDate=date('d-m-Y');
        $folderName  = "data/$currentDate/";
        $filename    = $folderName.$filename;
        $baseRoot="public/";
       
        $fileContent   = "Time,Total";
        if($file_name_end=="occupancy"){
            $fileContent   = "Time, Occupancy";
        }
        else if($file_name_end=="footfall"){
            $fileContent   = "Time, Foot Fall";
        }
        else if($file_name_end=="violation"){
            $fileContent   = "Time, Violation";
        }

        if($start_time){
            $fileContent  .="\n".$start_time.',0';
        }
        $contents="";
        if($data){
            foreach($data as $key=>$value){
                $contents  .="\n".$key.",$value";
            }
        }
 

        $baseRoot="public/";
        if(Storage::exists($baseRoot.$filename)){
           $fileContent=Storage::get($baseRoot.$filename);
           
        //    $storage=Storage::append($baseRoot.$filename, $contents);
        //    print_r($storage);
          }else{
            
            // $fileContent.=$contents;
            // Storage::disk('public')->put($filename, $fileContent);
        }
        
        $fileContent.=$contents;
       // echo $filename; exit; 
        // echo $fileContent; exit; 
        Storage::disk('public')->put($filename, $fileContent);

    
        
       

    }

    /**
     * Find all 1 hour interval between two time
     */
    static function hourInterval($start_time,$end_time){
        if(!$start_time && !$end_time) return ;
       
        $hour_intervals=[];
        $previous_time=$start_time;
        for($i=0;$i<24;$i++){

            $ending_time = date("Y-m-d H:i:s", strtotime("+60 minutes", strtotime($previous_time)));
            if($ending_time>$end_time){
                $ending_time=$end_time;
            }
            $hour_intervals[]=['start_time'=>$previous_time,'end_time'=>$ending_time];
            $previous_time=$ending_time;
            if($ending_time>=$end_time){
                $ending_time=$end_time;
                break;
            }
        }
        return $hour_intervals; 
    }

//     // public function getPoint($cx, $cy, $ang, $dist)
//     // {
//     //     // Convert angle from degrees to radians
//     //     $ang = deg2rad($ang);
//     //        $x = $cx + (float)($dist * (cos($ang))); 
//     //     $y = $cy + (float)($dist * (sin($ang)));
//     //     return array($x, $y);
//     // }
    
//     function getPoint($latitude,$longitude,$distance){
//         $la1=deg2rad($latitude);
//         $lo1=deg2rad($longitude);
//         $d=$distance*.001; //Km
//         $R = 6378.1; #Radius of the Earth
//         $Ad= $d/$R;
//         $bearing=deg2rad(rand(1,360));
//         $la2 =  asin(sin($la1) * cos($Ad)  + cos($la1) * sin($Ad) * cos($bearing
//    ));
//         $lo2 = $lo1 + atan2(sin($bearing)*sin($Ad)*cos($la1),cos($Ad)-sin($la1)*sin($la2));
//         return array(rad2deg($la2), rad2deg($lo2));

//    // echo rad2deg($la2); exit; 
//    }


//     public function getPointww($cx, $cy,$dist=null)
//     {
//         $ang=rand(1,360);
//         //1 meter convert to 0.001km
//         $dist=1;//$dist*0.001;
//         $R = 6378.1; #Radius of the Earth
//     $brng =deg2rad($ang);//  1.57; #Bearing is 90 degrees converted to radians.
//     $d = $dist; #Distance in km
    
//     #lat2  52.20444 - the lat result I'm hoping for
//     #lon2  0.36056 - the long result I'm hoping for.
    
//     $lat1 = deg2rad($cx); #Current lat point converted to radians
//     $lon1 = deg2rad($cy); #Current long point converted to radians
    
//     $lat2 = asin( sin($lat1)*cos($d/$R) +
//          cos($lat1)*sin($d/$R)*cos($brng));
//         //  $lat2 = rad2deg($lat2);
//         //  echo $lat2; exit; 
//     $lon2 = $lon1 + atan2(sin($brng)*sin($d/$R)*cos($lat1),
//                  cos($d/$R)-sin($lat1)*sin($lat2));
    
//     $lat2 = rad2deg($lat2);
//     $lon2 = rad2deg($lon2);
    
//     // print_r($lat2);
//     // echo "==";
//     // print_r($lon2);
    
//     	return array($lat2, $lon2);
//     }
// //     public function getPoint3($la1, $lo1, $ang, $dist)
// //     {
// //         $R = 6378.1; #Radius of the Earth
// //     $brng =deg2rad($ang);//  1.57; #Bearing is 90 degrees converted to radians.
// //     $d = $dist; #Distance in km
// //     $Ad= $d/$R;
    
// //     $la2 =  asin(sin($la1) * cos($Ad)  + cos($la1) * sin($Ad) * cos($brng));
// //     // $lo2 = $lo1 + atan2(sin($brng) * sin($Ad) * cos($la1), cos($Ad) – sin($la1)*sin($la2));
// //     print_r($la2); exit;
// //     $lo2 = $lo1 + atan2(sin($brng)*sin($Ad)*cos($la1),cos($Ad)-sin($la1)*sin($la2));
    
    
    
    
    
// //         return array($la2, $lo2);
// //     }
    

    

// //     public function getPoint($la1, $lo1, $ang=null, $dist=null)
// // {
// //     $ang=rand(1,360);
// //     $dist=1;
// // 	$R = 6378.1; #Radius of the Earth
// // $brng =deg2rad($ang);//  1.57; #Bearing is 90 degrees converted to radians.
// // $d = $dist; #Distance in km
// // $Ad= $d/$R;

// // $la2 =  asin(sin($la1) * cos($Ad)  + cos($la1) * sin($Ad) * cos($brng));
// // // $lo2 = $lo1 + atan2(sin($brng) * sin($Ad) * cos($la1), cos($Ad) – sin($la1)*sin($la2));

// // $lo2 = $lo1 + atan2(sin($brng)*sin($Ad)*cos($la1),cos($Ad)-sin($la1)*sin($la2));

// // return array($la2, $lo2);
// // }

    public static function SayHello()
    {
        return "SayHello";
    }

    /*
     * Rtuen date in 2018-07-16 13:03:35 format
     */
    public static function getTodayDate()
    {
    	return Carbon::now()->toDateTimeString();
    }
    
    /*
     * Rtuen date from 2018-07-16 13:03:35 format to 2018-07-16 format
     */
    public static function extractDateFromDateTime($str)
    {
        $dt = new Carbon($str);
        //return $dt->toFormattedDateString();
        return $dt->toDateString();
    }

    // returns Thu, Dec 25, 1975 2:15 PM
    // input 2018-09-05 11:13:50
    public static function formatDateFromDateTime($str)
    {
        $dt = new Carbon($str);
        return $dt->toDayDateTimeString();
    }

    public static function getTimeStamp()
    {
        return Carbon::now()->timestamp;
    }

    public static function sendSMS($recepient, $content,$template_id)
    {
        $content=urlencode($content);
       
        $data = [
            'workingkey' => 'A3d5a3523365172a31bafdc1196ee83b4',
            'sender' => 'WOOGLY',
            'to' => $recepient,
            'message' => $content
        ];
        
        $data = [
            'method'=>'sms',
            'api_key' => 'A3d5a3523365172a31bafdc1196ee83b4',
            'sender' => 'UNYDES',
            'to' => $recepient,
            'message' => $content,
            'entity_id'=>'1201159671558187672',
            'template_id'=>$template_id
        ];

       // $url = "http://trans.kapsystem.com/api/web2sms.php";
       $url="http://trans.kapsystem.com/api/v5/index.php";
        /** 
         * 
         * http://trans.kapsystem.com/api/v5/index.php?
         * method=sms
         * &message=hello test API submission Bulk submission
         * &to=98996XXXX
         * &sender=WOOGLY
         * &api_key={{api_key}}
         * &entity_id=1201159671558187672
         * &template_id=
         * 
        */
       
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url."?".http_build_query($data),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
       // print_r($response);
//        if ($err) {
//          echo "cURL Error #:" . $err;
//        } else {
//          echo $response;
//        }
    }
    

    public static function sendMessageThroughFCM($arr) {
        //Google Firebase messaging FCM-API url
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = (array) $arr;
        //define("GOOGLE_API_KEY","AAAAMmRTCHM:APA91bF-MtWGZqoElmDd2KY8IheC6rpT8uuf8ZRST1Y3f-zxb9NuJFyyXoApuRTpRYymLugJMEs9VtSc7tfBmz16bDHxiMODtqvWbknvQJJvjz11nS1X8hyfC-i44JcJKGbuH8ZFKf0YEASjq4oC6btD1dCbRg0-GQ");
        $headers = array(
            'Authorization: key=' . Config::get('app.google_api_key'),
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);               
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public static function logInfo($message, $messageType = 'info', $displayLog = true) {
        if($displayLog) {
            switch ($messageType) {
                case "emergency": \Log::emergency($message); break;
                case "alert": \Log::alert($message); break;
                case "critical": \Log::critical($message); break;
                case "error": \Log::error($message); break;
                case "warning": \Log::warning($message); break;
                case "notice": \Log::notice($message); break;
                case "info": \Log::info($message); break;
                case "debug": \Log::debug($message); break;
                default: \Log::debug($message); break;
            }
        }
    }

    

    // public static function getDistance($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
    //     if(is_null($point1_lat) || (is_null($point1_long)) || (is_null($point2_lat)) || (is_null($point2_long))) {
    //         return null;
    //     }
    //     // Calculate the distance in degrees
    //     $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

    //     // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
    //     switch($unit) {
    //         case 'km':
    //             $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
    //             break;
    //         case 'mi':
    //             $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
    //             break;
    //         case 'nmi':
    //             $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
    //     }
    //     //return round($distance, $decimals);
    //     return number_format($distance, $decimals,".","");
    // }

    // public static function getLatLngByRange( $lat, $lng, $distance = 50, $unit = 'mi' ) {
    //     // radius of earth; @note: the earth is not perfectly spherical, but this is considered the 'mean radius'
    //     if( $unit == 'km' ) { $radius = 6371.009; }
    //     elseif ( $unit == 'mi' ) { $radius = 3958.761; }

    //     // latitude boundaries
    //     $maxLat = ( float ) $lat + rad2deg( $distance / $radius );
    //     $minLat = ( float ) $lat - rad2deg( $distance / $radius );

    //     // longitude boundaries (longitude gets smaller when latitude increases)
    //     $maxLng = ( float ) $lng + rad2deg( $distance / $radius) / cos( deg2rad( ( float ) $lat ) );
    //     $minLng = ( float ) $lng - rad2deg( $distance / $radius) / cos( deg2rad( ( float ) $lat ) );

    //     $max_min_values = array(
    //         'min_latitude' => $minLat,
    //         'max_latitude' => $maxLat,
    //         'min_longitude' => $minLng,
    //         'max_longitude' => $maxLng
    //     );

    //     return $max_min_values;
    // }

    public static function logDBQueries() {
        $queries = \DB::getQueryLog();
        \Log::critical($queries);
    }

    public function status(){
        return [1=>'Active'];
    }


    public static function currentTime()
    {
    	return Carbon::now()->toDateTimeString();
    }
    
    public static function currentDate()
    {
    	return Carbon::now()->format('Y-m-d');
    }

    public static function addTime($time,$add="minute")
    {
    	return Carbon::now()->add($time,$add);
    }

    public static function beforeTime($minute=5)
    {
    	return  Carbon::now()->subMinutes($minute)->toDateTimeString();
    }

    /**
     * Convert time into date time  
     */
    public static function  getDateFromTime($selectedTime){
        if(empty($selectedTime)) return;
         return date('Y-m-d H:i:s',strtotime($selectedTime));
    }

    /**
     * Time differnce b/w two time
     */
    public static function diffInMinute($star_time){
        if(empty($star_time)) return; 
        $dt=Carbon::parse($star_time);
       
        return $dt->diffInMinutes(Common::currentTime());

    }
    
    
   
}