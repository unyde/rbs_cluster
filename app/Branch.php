<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;
    protected $fillable=['company_id','branch_name','branch_address','contact_number','status','start_time','end_time','mobile','email'];

    static function branchByCompanyId($companyId){
       if(empty($companyId)){ return [];} 
      return Branch::where(['company_id'=>$companyId])->pluck('branch_name','id');
      
    }
    static function branchStartTime($branchId,$withDate=true){
      if(empty($branchId)){ return;} 
      $start_time="";
      $branch=Branch::select('start_time')->where('id',$branchId)->first();
      if($branch){
        $start_time=$branch->start_time;
        if($withDate){
          $start_time = date('Y-m-d H:i:s',strtotime($start_time));
        }
      }
      return $start_time;
    }

    static function branchTiming($branchId){
      if(empty($branchId)){ return;} 
      $start_time="";
      $end_time="";
      $branch=Branch::select('start_time','end_time')->where('id',$branchId)->first();
      if($branch){
        $start_time=$branch->start_time;
        $end_time=$branch->end_time;
        $start_time = date('Y-m-d H:i:s',strtotime($start_time));
        $end_time = date('Y-m-d H:i:s',strtotime($end_time));
        
      }
      return ['start_time'=>$start_time,'end_time'=>$end_time];
    }

}
