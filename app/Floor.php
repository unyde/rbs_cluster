<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    public static function getFloors(){
        return Floor::where('status',1)->pluck('floor_name','id');
    }
}
