<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Camera;

use App\CameraTransaction;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Helpers\Common;
class Zone extends Model
{
  use SoftDeletes;
    protected $fillable=['company_id','branch_id','floor_id','building_name','zone_name','max_occupancy','sanitisation','visit_limit','sub_title'];
    static function zoneListByBranch($branchId){
        $zones = Zone::select('zones.*','floor_name')->where(['branch_id'=>$branchId])
      ->leftJoin('floors','floor_id','=','floors.id')
      ->where(function($query) use ($branchId){
          if($branchId){
              $query->where('zones.branch_id',$branchId);
          }
          
      })
      ->get();
      $data=[];
      if($zones){
          foreach($zones as $key=>$zone){
              $zone_name=[];
              if($zone->building_name){
                $zone_name[]=$zone->building_name;
              }
              if($zone->floor_name){
                $zone_name[]=$zone->floor_name;
              }
              
              if($zone->zone_name){
                $zone_name[]=$zone->zone_name;
              }

              $data[$zone->id]=implode(",",$zone_name);

          }
      }
      return $data;
    }

    /**
     * Zone occupancy count by zone
     */
    static function zoneOccupancyCount($zoneId,$filterDate){

       $cameras=Camera::select('device_id')->where('zone_id',$zoneId)->get();
       $occupancyCount=0;
       if($cameras){
         foreach($cameras as $camera){
        $device=$camera->device_id; 
        $cameraTransaction=CameraTransaction::select('current_occupancy')
        ->where('device',$device)
        ->where(function($query) use ($filterDate){
          if($filterDate){
          $query->whereDate('camera_transactions.created_at','=',$filterDate);
          }
      })

        ->orderBy('id','desc')->first();
        if($cameraTransaction){
          $occupancyCount += $cameraTransaction->current_occupancy;
        }
      }
       }
       return $occupancyCount; 
    }

    static function Occupancy($where,$filterDate,$zone_id){
      $data=Zone::select('company_name','branch_name','zone_name','zone_name','max_occupancy','zones.id')
        ->join('companies','company_id','=','companies.id')
        ->join('branches','branch_id','=','branches.id')
        ->where($where)
        ->where(function($query) use ($zone_id){
          if($zone_id){
              $query->where('zone_id',$zone_id);
          }
      })
        ->orderBy('zone_name','asc')
        ->get()->toArray();
        if($data){
            foreach($data as $key=>$row){
                 $zone_id=$row['id'];
                 $current_occupancy=Zone::zoneOccupancyCount($zone_id,$filterDate);
                 $max_occupancy=$row['max_occupancy']; 
                 $occupancy_percentage=0;
                 if($max_occupancy){
                  $occupancy_percentage=($current_occupancy/$max_occupancy)*100;
                }
                $data[$key]['current_occupancy']=$current_occupancy;
                $data[$key]['occupancy_percentage']=$occupancy_percentage;
                

            }
        }
        return $data;
    }

    static function getBranchIdByZone($zone_id){
     
      $branchId=0;
      if(empty($zone_id)){
        $zone = Zone::select('branch_id')
        ->first();
        $branchId=$zone->branch_id;
         
    }
    else{
        $zone = Zone::select('branch_id')
        ->where(['id'=>$zone_id])->first();
        $branchId=$zone->branch_id; 
    }
    return $branchId;
    }
    
    static function getZonesList($branchId,$zoneId){
      $result=Zone::where('branch_id',$branchId)
                ->where(function($query) use ($zoneId){
                    if($zoneId){
                        $query->where('id',$zoneId);
                    }

                })->get()->toArray();
                return $result;
    }
    
    static function zoneByBranchId($branchId){
      if(empty($branchId)) return; 
      $result=Zone::select('id','zone_name','sub_title')->where('branch_id',$branchId)
                   ->get();
                return $result;
    }

    /**
     * Return zone list based pn logged in user 
     * Use this code occupancy,foot fall,violation list zone wise
     */
    static function zoneListByUser($file_start,$file_end,$selectedDate,$zone_id=null){
      $selectedDate=$selectedDate?$selectedDate:date('Y-m-d');
      if($selectedDate){
                  $selectedDate=date('Y-m-d',strtotime($selectedDate));
      }

        $userInfo = User::getUserInfo();
        $branch_id = $userInfo['branch_id'];
        $where=[];
        $data=['zone_names'=>[],'zones'=>[]];
        $zones=[];
        if($branch_id){
            $where['zones.branch_id'] = $branch_id;
            $data['zone_names'] = Zone::where($where)->pluck('zone_name','id');
            if($zone_id){
              $where['id']=$zone_id;
            }
            $zones=Zone::select('id','zone_name','max_occupancy')->where($where)->get();
            $selectedDateFormat = date('d-m-Y',strtotime($selectedDate));
            if($zones){
              foreach($zones as $key=>$zone){
                  $zoneId=$zone->id;
                  $filePath= "$selectedDateFormat/$file_start"."_$zoneId"."_".$file_end;
                  $zones[$key]->file_url=Common::fetchFileUrl($filePath);
                  if($file_end=="footfall.csv"){
                    $zones[$key]->total_device = Camera::where(['zone_id'=>$zoneId])->get()->count();
                  } 
              }
              $data['zones']=$zones;
            }
          }
        return $data; 
    }


    /**
     * Return device list by zone id
     * Use this code for foot fall by device
     */
    static function deviceListByZone($file_start,$file_end,$selectedDate,$zone_id){
      $selectedDate=$selectedDate?$selectedDate:date('Y-m-d');
      if($selectedDate){
                  $selectedDate=date('Y-m-d',strtotime($selectedDate));
      }
      $data=[];
      $where=[];
      $where['zone_id']=$zone_id;
      $rows =Camera::select('id','device_name','device_id')->where($where)->get();
      $selectedDateFormat = date('d-m-Y',strtotime($selectedDate));
      $camera_names=[];
      if($rows){
        foreach($rows as $key=>$row){
            $id=$row->id;
            $device_name=$row->device_name;
            $device_id=$row->device_id;
            if($device_name && $device_id){
              $name=$device_name." ( $device_id )";
            }
            else{
              $name=$device_id;
            }
            $camera_names[$id]=$device_id;

            $filePath= "$selectedDateFormat/$file_start"."_$id"."_".$file_end;
            $rows[$key]->file_url=Common::fetchFileUrl($filePath);
        }
        $data['cameras']=$rows;
      }
      $data['camera_names'] = $camera_names;
      return $data; 
    }

    /**
     * Fetch zone list by where condition
     */
    static function fetchZoneList($where){
      $zones = Zone::select('zones.id','zone_name','max_occupancy','occupancy_last_id','occupancy_last_time','footfall_last_id','footfall_last_time','violation_last_id','violation_last_time','start_time','end_time')->
                join('branches','branches.id','=','branch_id')->where($where)->get();
      return $zones;
    }

    /**
     * Fetch zone with branch start time and end_time
     */
    static function zoneWithBranch(){
      $rows = Zone::select('zones.id','start_time','end_time')
                  ->join('branches','branch_id','=','branches.id')
                  ->where(['zones.status'=>1,'branches.status'=>1])
                  ->get();
                  
      
      if($rows){
        foreach($rows as $key=>$row){
          $rows[$key]->start_time=Common::getDateFromTime($row['start_time']);
          $rows[$key]->end_time=Common::getDateFromTime($row['end_time']);
        }
      } 
      return $rows; 
   }

}
