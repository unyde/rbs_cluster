<?php

namespace App\Exports;

use App\User;

use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\FromArray;

class UsersExport implements FromCollection,WithHeadings
{
    private $search;
    
    public function __construct($search)
    {
        $this->search = $search;
    }

    public function collection()
    {
        $search=$this->search; 
        return User::where(function($query) use ($search) {
            if($search){
                        $query->where('name','like',"%$search%");
                        $query->orWhere('email','like',"%$search%");
            }
        })->select('id','name','email')->get();
    }

    public function headings():array
    {
        return ["Id", "Name", "Email"];
    }

    

}
