<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use Illuminate\Cache\NullStore;
use Cache;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;

use Config;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
        //     // filter oauth ones
        //         Log::debug($query->sql . ' - ' . serialize($query->bindings));
           
        // });
        // \DB::listen(function($sql) {
        //     die(\Illuminate\Support\Str::replaceArray('?', $sql->bindings, $sql->sql));
        //   });
        if(Config::get('app.query_log')){
        \DB::listen(function($sql) {
            $queryLog=\Illuminate\Support\Str::replaceArray('?', $sql->bindings, $sql->sql);
            Log::debug($queryLog);
        });
    }

        

     



    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cache::extend( 'none', function( $app ) {
            return Cache::repository( new NullStore );
        } );
        
        if(env('FORCE_HTTPS',false)) { // Default value should be false for local server
            URL::forceScheme('https');
        }
        
        app('view')->composer('layouts.admin', function ($view) {
            $action = app('request')->route()->getAction();
    
            $controller = class_basename($action['controller']);
           
            list($controller, $action) = explode('@', $controller);
    
            $view->with(compact('controller', 'action'));
        });

    }
}
