<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TagUser extends Model
{
    use SoftDeletes;
    protected $fillable=['company_id','branch_id','username','password','status'];
}
