<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Common;
class PeakFootFall extends Model
{
    use SoftDeletes;
    protected $table = 'peak_foot_fall';
    protected $fillable=['zone_id','peak_foot_fall','peak_time','last_update','status'];
    
    /**
     * Find peak foot fall by data
     */
    static function peakFootFall($selectedZoneId,$selectedDate=null){
        if(empty($selectedZoneId)) return; 
        if(empty($selectedDate)){
            $selectedDate=Common::currentDate();
        }
        $where=['zone_id'=>$selectedZoneId];
        $row=PeakFootFall::select('peak_foot_fall','peak_time')
                           ->where($where)
                           ->whereDate('created_at','=',$selectedDate)
                           ->first();  
                          
        $peak_foot_fall="-"; 
        $peak_time=null;
        if($row){
            $peak_foot_fall=$row->peak_foot_fall;
            $peak_time=$row->peak_time;
        }
        return ['peak_foot_fall'=>$peak_foot_fall,'peak_time'=>$peak_time];

    }
}
