<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Carbon\Carbon;
use App\CameraTransaction;
use App\Helpers\Common;
class Camera extends Model
{
    use SoftDeletes;
    protected $fillable=['counter_restart','last_request','height','device_mac','reboot','company_id','branch_id','zone_id','device_name','device_id','device_mac','reboot_type'];
    static function getWeekData($where,$start_date,$end_date){
        $weekDays=[];
        $currentWeekData=[];
        for($i=0;$i<=6;$i++){$currentWeekData[$i]=0;}

        for($i=0;$i<=6;$i++){
            $from_time=date('Y-m-d',strtotime("+$i day",strtotime($start_date)));
            if($from_time>$end_date){ break;}
            $nextday=date('w',strtotime("+$i day",strtotime($start_date)));
            $weekDays[$nextday]=$from_time;
        }
        if(is_array($weekDays) && count($weekDays)){
            foreach($weekDays as $key=>$weekDate){
                $from_time=$weekDate." 00:00:00";
                $to_time=$weekDate." 23:59:59";
                $cameraTodayFallTrends=Camera::select('in')->join('camera_transactions','device_id','=','device')
                                        ->where($where)
                                        ->where('camera_transactions.created_at','>=',$from_time)
                                        ->where('camera_transactions.created_at','<=',$to_time)
                                        ->whereIn('camera_transactions.id', function($q) use ($from_time,$to_time){
                                            $q->select(DB::raw("MAX(camera_transactions.id) FROM camera_transactions where camera_transactions.created_at>='$from_time' && camera_transactions.created_at<='$to_time' GROUP BY device"));
                                        })
                                        ->groupBy('device')
                                        ->orderBy('camera_transactions.id','desc')
                                        ->get();
                $totalIn=0;
                foreach($cameraTodayFallTrends as $cameraTodayFallTrend){
                    $totalIn+=$cameraTodayFallTrend->in;
                }
                $currentWeekData[$key]=$totalIn;
            }
        }
        return $currentWeekData;
    }
    static function getFallTrendMonthData($where,$month,$year,$totalDay){
        $monthData=[];
              for($i=1;$i<=$totalDay;$i++){
            $total=0;
            $monthDay=$year."-".$month."-".str_pad($i,2,0,STR_PAD_LEFT);
            $monthData[$i]=0;
            $rows=Camera::select('in')->join('camera_transactions','device_id','=','device')
            ->where($where)
            ->whereDate('camera_transactions.created_at','=',$monthDay)
            ->whereIn('camera_transactions.id', function($q) use ($monthDay){
                 $q->select(DB::raw("MAX(camera_transactions.id) FROM camera_transactions where date(camera_transactions.created_at)='$monthDay' GROUP BY device"));
            })
            ->groupBy('device')
            ->orderBy('camera_transactions.id','desc')
            ->get();
            foreach($rows as $row){
                $total+=$row->in;
            }
            $monthData[$i]=$total;
                    
         }
         return $monthData;
         
    }

    static function getSpendTime($where,$timeArray,$condition='time'){
        $data=[];
        if($condition=='time'){
            $data[0]=0;
        }    
       
        foreach($timeArray as $item){
            $total=0;
            $total_duration=0;
            $total_occupancy=0;
            
            $from_time     = $item['from_time'];
            $to_time       = $item['to_time'];
            if($from_time && $to_time){
            $rows=Camera::select('total_duration','current_occupancy')
            ->join('camera_transactions','device_id','=','device')
            ->where($where)
            ->where('total_duration','!=',0)
            ->where(function($query) use ($condition,$from_time,$to_time){
                if($condition=='time'){
                    $query->where('camera_transactions.created_at','>=',$from_time);
                    $query->where('camera_transactions.created_at','<=',$to_time);
                }
                else{
                    $query->whereDate('camera_transactions.created_at','=',$from_time);

          
                }
            })
            ->get();
            foreach($rows as $row){
                if($row->total_duration<0){
                    $total_duration+=0;
                }
                else{
                    $total_duration+=$row->total_duration;
                }
                if($row->current_occupancy<0){
                    $total_occupancy+=0;
                }
                else{
                    $total_occupancy+=$row->current_occupancy;
                }
            }
            if($total_duration && $total_occupancy){
                $total=$total_duration/$total_occupancy;
                $total=$total/60;//For min
                $total=number_format($total,2);

            }
            $data[]=$total;
                    
         }
        }
         return $data;
         
    }

    static function getDateBetweenTwoDate($start_date,$end_date){
        $weekDays=[];
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date);
        $diff_in_days = $to->diffInDays($from);
       
        //=6
        for($i=0;$i<=$diff_in_days;$i++){
          $from_time=date('Y-m-d',strtotime("+$i day",strtotime($start_date)));
            if($from_time>$end_date){ break;}
            $nextday=date('w',strtotime("+$i day",strtotime($start_date)));
            //echo $nextday; exit;
            $weekDays[]=$from_time;
        }
     
        return $weekDays;
    } 
    static function getDateArray($dates){
        $allDates=[];
        foreach($dates as $item){
            $allDates[]=['from_time'=>$item,'to_time'=>$item];
        }
        return $allDates;
    }

    static function getCurrentOccupancy($zoneId,$selectedDate){
        $data=[];
        $occupancyCount=0;
        $footFall=0;
        $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
        foreach($cameras as $camera){
                $device=$camera->device_id; 
                $cameraTransaction=CameraTransaction::select('*')
                                    ->where('device',$device)
                                    ->whereDate('camera_transactions.created_at','=',$selectedDate)
                                    ->orderBy('id','desc')
                                    ->first();
                if($cameraTransaction){
                  $occupancyCount+=  $cameraTransaction->current_occupancy;
                  $footFall+=$cameraTransaction->in; 
                }
               
               
        }
        //$data=['occupancy'=>$occupancyCount,'footFall'=>$footFall]; 
        return ['currentOccupancyTotal'=>$occupancyCount,'totalFootFall'=>$footFall];
        //return $data; 
    
     }
    static function getCurrentOccupancyOld($where,$date=''){
        
        $currentDate = date('Y-m-d');
        if($date){
            $currentDate=$date;
        }
        $currentOccupancyTotal=0;
        $totalFootFall=0; 
        if($where){
        $currentOccupancies=Camera::select('current_occupancy','in')
        ->join('camera_transactions','device_id','=','device')
        ->where($where)
        ->whereDate('camera_transactions.created_at','=',$currentDate)
        ->whereIn('camera_transactions.id', function($q){
          $q->select(DB::raw('MAX(camera_transactions.id) FROM camera_transactions GROUP BY device'));
         })
        ->groupBy('device')
        ->orderBy('camera_transactions.id','desc')
        ->get();
         foreach($currentOccupancies as $row){
            $currentOccupancyTotal+=$row->current_occupancy; 
            $totalFootFall+=$row->in; 
          }
          if($currentOccupancyTotal<0){
              $currentOccupancyTotal=0;
          }
          if($totalFootFall<0){
            $totalFootFall=0;
        }
        }
        return ['currentOccupancyTotal'=>$currentOccupancyTotal,'totalFootFall'=>$totalFootFall];

    }

    static function getCurrentViolation($where,$date=''){
        $currentDate=date('Y-m-d');
        return Camera::select('camera_transactions.id')
                      ->join('camera_transactions','device_id','=','device')
                        ->where($where)
                        ->where('occupancy_violation','=',1)
                        ->whereDate('camera_transactions.created_at','=',$currentDate)
                        ->get()
                        ->count();

    }
    
    static function footFall($where,$dateArray){
        $total=0;
        if(is_array($dateArray)){  
        foreach($dateArray as $date){    
        $rows = Camera::select('in')->join('camera_transactions','device_id','=','device')
                                        ->where($where)
                                        ->whereDate('camera_transactions.created_at','=',$date)
                                        ->whereIn('camera_transactions.id', function($q) use ($date){
                                            $q->select(DB::raw("MAX(camera_transactions.id) FROM camera_transactions where date(camera_transactions.created_at)='$date' GROUP BY device"));
                                        })
                                        ->groupBy('device')
                                        ->orderBy('camera_transactions.id','desc')
                                        ->get();
        foreach($rows as $rows){
            $total+=$rows->in;
        }                               
        }
       }
       return $total;

    }
    function getDeviceStatus($date,$device_id){
        $cameraTransaction = CameraTransaction::select('created_at')->where('device',$device_id)
                             ->whereDate('created_at','=',$date)
                             ->first();    
        $created_at="";
        if($cameraTransaction){
            $created_at = $cameraTransaction->created_at;

        } 
       
        return $created_at;                    
    }

    static function getOccupancyViolation($where,$timeArray,$condition='time'){
        $data=[];
        foreach($timeArray as $key=>$item){
            $data[$key]=0;
            $from_time="";
            $to_time="";
            if(is_array($item)){
                $from_time = $item['from_time'];
                $to_time = $item['to_time'];
               
              
            }
            else{
                $from_time=$item;
            }
            if($from_time && $to_time && $condition=='time'){

            }
            else if($condition=='day'){

            }
            else{
                continue;
            }
            
            $data[$key]=Camera::select('camera_transactions.id')->join('camera_transactions','device_id','=','device')
                                ->where($where)
                                ->where(function($query) use ($condition,$from_time,$to_time){
                                    if($condition=='time'){
                                        $query->where('camera_transactions.created_at','>=',$from_time);
                                        $query->where('camera_transactions.created_at','<=',$to_time);
                                    }
                                    else{
                                        $query->whereDate('camera_transactions.created_at','=',$from_time);
                    
                            
                                    }
                                })
                                ->where('occupancy_violation',1)        
                                ->get()
                                ->count();
                              
                                
        }

        return $data;
    }

    function getDeviceLastRecord($device,$condition=false){
        $cameraTransaction=CameraTransaction::select('in','out','created_at')->where('device','=',$device)
                           ->orderBy('id','desc')
                           ->first();
                           
        $lastRecord=['in'=>0,'out'=>0];

        if($condition && $cameraTransaction){
            $lastRecord=['in'=>$cameraTransaction->in,'out'=>$cameraTransaction->out];
        }
        else{
        if($cameraTransaction){
            $created_at=$cameraTransaction->created_at;
            if($created_at){
                $created_at=date('d-m-y H:i a',strtotime($created_at));
            }
            $lastRecord="IN=".$cameraTransaction->in.", OUT=".$cameraTransaction->out.", DateTime=".$created_at;
        }    
    }           
   
        return $lastRecord;
    }

    static function getTotalFallIn($where,$date){
        $total=0;
        if($date){
        $rows = Camera::select('in')->join('camera_transactions','device_id','=','device')
                                  ->where($where)
                                  ->whereDate('camera_transactions.created_at','=',$date)
                                  ->whereIn('camera_transactions.id', function($q) use ($date){
                                     $q->select(DB::raw("MAX(camera_transactions.id) FROM camera_transactions where date(camera_transactions.created_at)='$date' GROUP BY device"));
                                   })
                                  ->groupBy('device')
                                  ->orderBy('camera_transactions.id','desc')
                                  ->get();
        foreach($rows as $row){
        $total+=$row->in; 
         }
        }  
        return $total;                         
    }

    /**
     * Get last request from camera
     */
    function getLastRequest($device){
        $where=[];
        $where['device']=$device;
        $row =CameraTransaction::select('created_at')
            ->where($where)
            ->orderBy('id','desc')
            ->first();
        if($row){
            return $row->created_at->format('d M Y, h:i a');
        }
    }
    /**
     * Update device status
     */
    static function deviceUpdate($device){
        if($device){
        $last_request = Common::currentTime();

        // DB::enableQueryLog();
        $query=Camera::where('device_id',$device)
            ->update(['last_request'=>$last_request]);
        }
//         $query = DB::getQueryLog();
//         $query = end($query);
// dd($query);
// dd($query);
        return true;
    }

    /**
     * Device status by camera id 
     */
    function deviceStatus($last_time){
        if(empty($last_time)) return false;

        $diffInMinute=Common::diffInMinute($last_time);
       
        if($diffInMinute>5){ //If time greater then 5 minute
            return false;
        }
        else{
            return true;
        }
        // echo $diffInMinute;
        // exit;
        // $where=[];
        // $where['device']=$device;
        // $row =CameraTransaction::select('id')
        //     ->where($where)
        //     ->where('created_at','>=',Common::beforeTime())
        //     ->get()
        //     ->count();
        // return $row; 
    }

    /**
     * Fetch camera list by where condition
     */
    static function fetchCameraList($where){
       $rows= Camera::select('cameras.id','device_id','ct_last_id','start_time','end_time')->
                  join('branches','branches.id','=','branch_id')
                  ->where($where)->get();
               return $rows;
       
      }


   

}
