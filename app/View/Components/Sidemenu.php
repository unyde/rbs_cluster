<?php

namespace App\View\Components;
use Illuminate\Http\Request;
use Illuminate\View\Component;
use App\User;
use App\Zone;

class Sidemenu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $controller,$action,$zones,$zone_id;
    public function __construct(Request $request,$controller,$action)
    {
        $this->zone_id=$request->zone_id;
        $this->controller=$controller;
        $this->action=$action;
        $userInfo = User::getUserInfo();
        if(is_array($userInfo) && array_key_exists('branch_id',$userInfo)){
           $branch_id=$userInfo['branch_id'];
           if($branch_id){
               $this->zones =Zone::zoneByBranchId($branch_id);
              
               if(!$this->zone_id){
                $where['branch_id']=$branch_id;   
                $zone = Zone::select('id','zone_name')
                ->where($where)->first();
                if($zone){
                    $this->zone_id=$zone->id;
                }
                
               }

           }
          
        }
       
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.sidemenu');
    }
}
