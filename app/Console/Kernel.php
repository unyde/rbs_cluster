<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\PeakOccupanyMinute::class,
        Commands\AverageOccupancyDaily::class,
        Commands\AverageOccupancyMinute::class,
        Commands\FootFallMinute::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        

        $schedule->command('average_occupancy:daily')
        ->daily();

     
        $schedule->command('peak_occupancy:minute')
            ->everyMinute();

        $schedule->command('average_occupancy:minute')
        ->everyMinute();    
        
        $schedule->command('foot_fall:minute')
        ->everyMinute();

        




    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
