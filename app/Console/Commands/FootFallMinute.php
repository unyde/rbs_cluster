<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\FootFallHour;
use App\Helpers\Common;
use App\CameraTransaction;
use App\Zone;
use App\PeakFootFall;
class FootFallMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foot_fall:minute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update foot fall for all zone per minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentDate=Common::currentDate();
        $rows=FootFallHour::select('id','zone_id','start_time','end_time','ct_last_id','status')
               ->whereDate('created_at','=',$currentDate)->get();
        if($rows){
            $start_time="";
            $previousFootFallIn=0;
            foreach($rows as $row){
                $zone_id=$row->zone_id;
                //$start_time=$row->start_time;
                $end_time=$row->end_time;
                $ct_last_id=$row->ct_last_id;
                $status=$row->status;
                if($status){
                    $ct_last_id=$row->ct_last_id;
                }
                else{
                    $ct_last_id=0;
                }
                $where=[];
                $where['id']=$ct_last_id;
                $where['zone_id']=$zone_id;
                $where['start_time']=$start_time;
                $where['end_time']=$end_time;
                $cTransactions=CameraTransaction::fetchFootFall($where);
                if($cTransactions){
                    $foot_fall_in=$cTransactions->in;
                    $row->first_in=$previousFootFallIn;
                    $row->last_in=$foot_fall_in;
                    $row->foot_fall=$foot_fall_in-$previousFootFallIn;
                    $row->ct_last_id=$cTransactions->id;
                    $row->status=1;
                    $row->save();
                    $previousFootFallIn=$foot_fall_in;
                }

                $start_time=$end_time;

            }
        }       


        /**
         * Peak Foot Fall Update
         */
        $zones=Zone::select('id')->get();
        if($zones){
            foreach($zones as $zone){
               $zone_id=$zone->id; 
               $where=[];
               $where['zone_id']=$zone_id;
               $where['status']=1;
               $row =FootFallHour::select('*')->where($where)
                                   ->whereDate('start_time','=',$currentDate)
                                   ->orderBy('foot_fall','desc')
                                   ->first();
               if($row){
                $foot_fall=$row->foot_fall;
                $start_time= $row->start_time;
                $end_time= $row->end_time;
                $peak_hour="";
                if($start_time){
                    $peak_hour.=date('h:i',strtotime($start_time));
                }
                if($end_time){
                    if($peak_hour){
                        $peak_hour.=" - ";
                    }
                    $peak_hour.=date('h:i',strtotime($end_time));
                }

                $PeakFootFall= PeakFootFall::where($where)
                            ->whereDate('created_at','=',$currentDate)
                            ->first();
                            if($PeakFootFall){
                                $PeakFootFall->peak_foot_fall=$foot_fall;
                                $PeakFootFall->peak_time=$peak_hour;
                                 $PeakFootFall->save();
                            }
                            else{
                                $data=['zone_id'=>$zone_id,'peak_foot_fall'=>$foot_fall,'peak_time'=>$peak_hour];
                                PeakFootFall::create($data); 
                            }
               }                    
                                   
                                
            }
        }
       
        return 0;
    }
}
