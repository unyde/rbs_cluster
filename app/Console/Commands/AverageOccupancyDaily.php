<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\Common;
use App\AverageOccupancy;
use App\Zone;
use App\FootFallHour;
class AverageOccupancyDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'average_occupancy:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create empty occupany for all zone with branch timing start and end time - 1 hour interval';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $zones = Zone::zoneWithBranch();
       
        if($zones){
            foreach($zones as $zone){
                $zone_id    = $zone->id;
                $start_time =  $zone->start_time;
                $end_time =  $zone->end_time;
                $time_intervals=Common::hourInterval($start_time,$end_time);
                AverageOccupancy::createAverageOccupancy($zone_id,$time_intervals);
                FootFallHour::createFootFall($zone_id,$time_intervals);
            }
        }
        return 0;
    }
}
