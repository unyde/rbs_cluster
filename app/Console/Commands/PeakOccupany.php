<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PeakOccupancy;
use App\Zone;
use App\Helpers\Common;
use App\CameraTransaction;
class PeakOccupanyMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'peak_occupancy:minute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update peak occupancy for all zone per minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         $zones = Zone::zoneWithBranch();
        if($zones){
            foreach($zones as $zone){
                $zone_id=$zone->id;
                $start_time = $zone->start_time;
                $end_time = $zone->end_time;
                if(!$start_time || !$end_time){
                    continue;
                }
                $peak_occupancy=0;
                $ct_last_id=0;
                $peakOccupancy=PeakOccupancy::fetchPeakOccupancy($zone_id);
                // dd($peakOccupancy);
       
                if($peakOccupancy){
                    if($peakOccupancy->peak_occupancy){
                        $peak_occupancy=$peakOccupancy->peak_occupancy;
                    }
                    if($peakOccupancy->ct_last_id){
                        $ct_last_id=$peakOccupancy->ct_last_id;
                    }
                }
              
                //Where condition for camera transaction
                $where=[];
                $where['zone_id']=$zone_id;
                $where['id']=$ct_last_id;
                $where['start_time']=$start_time;
                $where['end_time']=$end_time;
                $peak_time="";
                $updateZone=[];

                $currentTime=Common::currentTime();
                $peak_time="";
                
                $cameraTransactions=CameraTransaction::fetchOccupancy($where);
             
                if($cameraTransactions){
                    foreach($cameraTransactions as $ct){
                        $current_occupancy=$ct->current_occupancy;
                        $ct_last_id=$ct->id;
                        if($current_occupancy>$peak_occupancy){
                            $peak_occupancy=$current_occupancy;
                            $peak_time=$ct->created_at;
                            if($peakOccupancy){
                                $peakOccupancy->peak_occupancy=$peak_occupancy;
                                $peakOccupancy->peak_time=$peak_time;
                            }
                        }
                    }
                }


                //save zone
               // echo "last_update=$last_update";
                 if($peakOccupancy){
                    $peakOccupancy->last_update=$currentTime;
                    if($ct_last_id){
                        $peakOccupancy->ct_last_id=$ct_last_id;
                    }
                    $peakOccupancy->save();
                }
                else{
                    $peakData=[];
                    $peakData['zone_id']=$zone_id;
                    $peakData['peak_occupancy']=$peak_occupancy;
                    if($peak_time){
                        $peakData['peak_time']=$peak_time; 
                    }
                    if($ct_last_id){
                        $peakData['ct_last_id']=$ct_last_id;
                    }
                    $peakData['last_update']=$currentTime;

                    PeakOccupancy::create($peakData);
                }
                 
                
               
                


            }
        }
           
        
        return 0;
    }
}
