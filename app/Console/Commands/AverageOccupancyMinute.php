<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AverageOccupancy;
use App\Helpers\Common;
use App\CameraTransaction;
use App\AverageOccupanyDaily;
use App\Zone;
class AverageOccupancyMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'average_occupancy:minute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update average occupancy for all zone per minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   $currentTime=Common::currentDate();
        $currentDate=$currentTime;
        $rows=AverageOccupancy::select('id','zone_id','start_time','end_time','ct_last_id','status','total_occupancy','total_record')
               ->whereDate('created_at','=',$currentTime)->get();
        if($rows){
            foreach($rows as $row){
                $zone_id=$row->zone_id;
                $start_time=$row->start_time;
                $end_time=$row->end_time;
                $average_occupancy=$row->average_occupancy;
                $ct_last_id=$row->ct_last_id;

                $total_occupancy=$row->total_occupancy;
                $total_record=$row->total_record;
                
                $status=$row->status;
                if($status){
                    $ct_last_id=$row->ct_last_id;
                }
                else{
                    $ct_last_id=0;
                }
                $where=[];
                $where['id']=$ct_last_id;
                $where['zone_id']=$zone_id;
                $where['start_time']=$start_time;
                $where['end_time']=$end_time;
                
                // $total=0;
                // $counter=0;
                $cTransactions=CameraTransaction::fetchOccupancy($where);
                if($cTransactions){
                    foreach($cTransactions as $cTransaction){
                        $total_occupancy+=$cTransaction->current_occupancy;
                        $ct_last_id=$cTransaction->id;
                        $total_record++;
                    }
                }
             
               if($total_record){
                    $average_occupancy=$total_occupancy/$total_record;
                    $row->average_occupancy=round($average_occupancy);
                    $row->ct_last_id=$ct_last_id;
                    $row->status=1;
                    $row->total_occupancy=$total_occupancy;
                    $row->total_record=$total_record;
                }
                $row->save();
               
                
               

                
                
            }
        }




        
        //Average Occupancy Daily
        $zones=Zone::select('id')->get();
        if($zones){
            foreach($zones as $zone){
               $zone_id=$zone->id;  
               $rows=AverageOccupancy::select('id','zone_id','average_occupancy')
                    ->whereDate('created_at','=',$currentTime)
                    ->where(['status'=>1,'zone_id'=>$zone_id])
                    ->get();
                   
                 

                if($rows){
                    $total_occupancy=0;
                    $total_record=0;
                    foreach($rows as $row){
                        $total_occupancy+=$row->average_occupancy;
                        $total_record++;
                    }
                  
                    if($total_record){
                    
                       $average=round($total_occupancy/$total_record);
                       $AverageOccupanyDaily = AverageOccupanyDaily::where(['zone_id'=>$zone_id])
                       ->whereDate('created_at','=',$currentDate)->first();
                    if($AverageOccupanyDaily){
                           $AverageOccupanyDaily->average_occupancy=$average;
                           $AverageOccupanyDaily->save();
                       }
                       else{
                           AverageOccupanyDaily::create(['zone_id'=>$zone_id,'average_occupancy'=>$average]);
                       }
                    }
                }  
            }
        }
                            
        return 0;
    }
}
