<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Common;
class AverageOccupanyDaily extends Model
{
    use SoftDeletes;
    protected $table = 'average_occupancy_daily';
    protected $fillable=['zone_id','average_occupancy'];
    /**
     * Find selected zone average occupancy by daily base
     */
    static function averageOccupancy($selectedZoneId,$selectedDate=null){
        if(empty($selectedZoneId)) return 0;
        if(empty($selectedDate)){
            $selectedDate=Common::currentDate();
        } 
        $row=AverageOccupanyDaily::select('average_occupancy')
                                  ->where('zone_id',$selectedZoneId)
                                  ->whereDate('created_at','=',$selectedDate)
                                  ->first();
        $average_occupancy=0;
        if($row){
            $average_occupancy = $row->average_occupancy; 
        }
        return $average_occupancy; 
                               
    }

}
