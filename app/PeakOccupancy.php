<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Common;
class PeakOccupancy extends Model
{
    use SoftDeletes;
    protected $table = 'peak_occupany';
    protected $fillable=['zone_id','peak_occupancy','peak_time','last_update','ct_last_id'];

    /**
     * Fetch peak occupancy by zone
     * @zone_id - Id of the zone
     * @selectedId - Pass any date
     */
    static function fetchPeakOccupancy($zone_id,$selectedDate=null){
        if(empty($zone_id)) return;
        if(empty($selectedDate)){
            $selectedDate=Common::currentDate();
        }
        $where=[];
        $where['zone_id']=$zone_id;
        $row = PeakOccupancy::select('id','peak_occupancy','ct_last_id')
              ->where($where)
              ->whereDate('last_update','=',$selectedDate)
              ->first();
        return $row;       
    }
}
