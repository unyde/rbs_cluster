<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Storage;
use Pusher\Pusher;
class ApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $response = $next($request);
       

        $logWrite = config('app.log_write');
        if ($logWrite) {
            $log = [
                'URI' => $request->getUri(),
                'METHOD' => $request->getMethod(),
                'REQUEST_BODY' => $request->all(),
                'RESPONSE' => $response->getContent()
            ];
            $message="\n";
            foreach($request->all() as $key=>$value){
                $message.=$key.":".$value."\n";
            }
            $message.="=======================\n";
            
            // Log::info(json_encode($log));
            // Log::info($message);
            $requestContent=date('d-m-y H:i:s');
            $requestContent.="\n".json_encode($log);
            $requestContent.=$message;
            $filename="customlog";
            $device_id=$request->device_id;
            if(empty($device_id)){
                $device_id=$request->device;
            }
            if($device_id){
                $filename=$device_id;
            }
            $filename="api/".$filename."-".date('d-m-y').".log";
            $previousContent="";
            if(Storage::exists($filename)) {
                $previousContent=Storage::get($filename);
            } 
            $previousContent=$requestContent.$previousContent;
            
            Storage::put($filename, $previousContent);

            //Live log 
        //     $options = array(
        //         'cluster' => 'ap2',
        //         'useTLS' => true
        //     );
            
        //     $pusher =  new Pusher(
        //         '7a79691af7c99ed6f2e7',
        //         '3b46431165df648bc0c6',
        //         '1186677',
        //         $options
        //     );
        //     $data=[];
        //     $data['device']=$device_id;
        //     $data['content']=$requestContent;
        //    // $pusher->trigger('my-channel', 'log', $data);

             
        }

        return $response;
    }
}
