<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Company;
use App\User;
use DB;
use App\Camera;
use App\Branch;
use App\TagUser;
use App\Floor;
use App\Zone;
use App\CameraTransaction;
use Config;
use App\Helpers\Common;
use App\PeakOccupancy;
use App\AverageOccupanyDaily;
use App\PeakFootFall;
use App\FootFallHour;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){
        $where=[];
        $data=[];
        $selectedZoneId = $request->zone_id;
        $selectedDate   = date('Y-m-d');
        $userInfo     = User::getUserInfo();
        $branch_id    = $userInfo['branch_id'];
        if($branch_id){
            $where['branch_id'] = $branch_id;
        }
        if($selectedZoneId){
            $where['id']=$selectedZoneId;
        }
        //If selected zone empty then fetch first zone from this branch
        $zone = Zone::select('id','zone_name','max_occupancy')
                ->where($where)->first();
        if($zone && empty($selectedZoneId)){
            $selectedZoneId=$zone->id;
        }
        if($zone){
            $request->session()->put('zone_name', $zone->zone_name);
        }
        $max_occupancy=$zone->max_occupancy;
        
       

        //Fetch data for home page widget section
        $zoneWhere=[];
        $zoneWhere['zone_id']=$selectedZoneId;
        $branchTime =Branch::branchTiming($branch_id);
        
        $totalOccupancy =CameraTransaction::lastTransaction('current_occupancy',$zoneWhere,$branchTime);
        $totalFootFall =CameraTransaction::lastTransaction('zone_total_in',$zoneWhere,$branchTime);
        $totalViolation =CameraTransaction::violation('occupancy_violation',$zoneWhere,$branchTime);
       
        //Zone file url 
        $selectedDateFormat = date('d-m-Y',strtotime($selectedDate));
        $filePath= "$selectedDateFormat/zone_$selectedZoneId";
        $data['occupancyFileUrl'] =Common::fetchFileUrl($filePath."_occupancy.csv");
        $data['footFallFileUrl'] =Common::fetchFileUrl($filePath."_footfall.csv");
        $data['violationFileUrl'] =Common::fetchFileUrl($filePath."_violation.csv");
        
        $data['selectedZoneId']  = $selectedZoneId;
        $data['selectedDate']    = $selectedDate;
        $data['totalOccupancy']  = $totalOccupancy;
        $data['totalFootFall']   = $totalFootFall;
        $data['totalViolation']  = $totalViolation;

        //Fetch peak occupancy
        $PeakOccupancy=PeakOccupancy::fetchPeakOccupancy($selectedZoneId);
        $peak_occupancy=0;
        if($PeakOccupancy){
            $peak_occupancy=$PeakOccupancy->peak_occupancy;
        }
        $data['peak_occupancy']   = $peak_occupancy;

        //Fetch average Occupancy
        $data['average_occupancy'] =AverageOccupanyDaily::averageOccupancy($selectedZoneId);
      
        $data['peak_foot_fall'] =PeakFootFall::peakFootFall($selectedZoneId);
        $data['average_foot_fall']=FootFallHour::averageFootFall($selectedZoneId);
        $updatedZoneWhere=$zoneWhere;
        if(is_array($branchTime)){
            $updatedZoneWhere=array_merge($zoneWhere,$branchTime);
        
        }
        $zoneWhereString =Common::getStringFromArray($updatedZoneWhere);
        $data['zone_where']=$zoneWhereString;
     
        
        //
       // print_r($data); exit; 
        return view('home',compact('zone','data','max_occupancy'));
   
       
    }

    function homePage(Request $request){
        $zoneWhere=[];
        $branchTime=[];
        $zone_id=$request->zone_id;
        $zoneWhere['zone_id']=$request->zone_id;
        $branchTime['start_time'] = $request->start_time;
        $branchTime['end_time'] = $request->end_time;
       
        $totalOccupancy=0;
        $totalFootFall=0;
        $totalViolation=0;
        
        if(empty($zoneWhere['zone_id']) || empty($branchTime['start_time']) || empty($branchTime['end_time'])){

        }
        else{
            $totalOccupancy =CameraTransaction::lastTransaction('current_occupancy',$zoneWhere,$branchTime);
            $totalFootFall =CameraTransaction::lastTransaction('zone_total_in',$zoneWhere,$branchTime);
            $totalViolation =CameraTransaction::violation('occupancy_violation',$zoneWhere,$branchTime);
           
        }
        $data=[];
        $data['totalOccupancy']  = $totalOccupancy;
        $data['totalFootFall']   = $totalFootFall;
        $data['totalViolation']  = $totalViolation;
        $data['average_occupancy'] =AverageOccupanyDaily::averageOccupancy($zone_id);
        $data['peak_foot_fall'] =PeakFootFall::peakFootFall($zone_id);
        $data['average_foot_fall']=FootFallHour::averageFootFall($zone_id);
        return $data;
       
    }

    // public function indexOld(Request $request)
    // {   
    //     $where=[];
    //     $zones=[];
    //     $timeSlots=[];
    //     $selectedZone =$request->zone_id;
    //     $selectedDate=$request->filter_date?$request->filter_date:date('Y-m-d');
    //     if($selectedDate){
    //         $selectedDate=date('Y-m-d',strtotime($selectedDate));
    //     }
    //     $userInfo = User::getUserInfo();
    //     $company_id = $userInfo['company_id'];
    //     $branch_id = $userInfo['branch_id'];
    //     $adminBranchId="";
    //     // if(empty($branch_id)){
    //     //     // if(empty($selectedZone)){
    //     //     //     $zone = Zone::select('branch_id')
    //     //     //     ->first();
    //     //     //     $adminBranchId=$zone->branch_id;
                 
    //     //     // }
    //     //     // else{
    //     //     //     $zone = Zone::select('branch_id')
    //     //     //     ->where(['id'=>$selectedZone])->first();
    //     //     //     $adminBranchId=$zone->branch_id; 
    //     //     // }
    //     // }

    //     ///$companyInfo = Company::select('*')->where('id',$company_id)->first();
    //     $selectedInterval=$request->interval?$request->interval:Config::get('app.chart_interval');
    //     $interval=$selectedInterval; //minute

        
        
    //     // print_r($intervalBreak); exit; 

    //     if($branch_id){
    //         $where['zones.branch_id'] = $branch_id;
    //         $intervalBreakData=CameraTransaction::getTimeSlot($branch_id,$selectedDate,$interval);
    //     $intervalBreak=@$intervalBreakData['timeBreak'];
    //     }
    //     else{
    //         $adminBranchId =Zone::getBranchIdByZone($selectedZone);
           
    //         $intervalBreakData=CameraTransaction::getTimeSlot($adminBranchId,$selectedDate,$interval);
    //         $intervalBreak=@$intervalBreakData['timeBreak'];
    //     }
        
    //     $zones=Zone::where($where)->pluck('zone_name','id');

    //     //$zone_id=$request->zone_id?$request->zone_id:11;
    //     if(empty($selectedZone)){
    //         $zone = Zone::select('id')
    //                 ->where($where)->first();
    //         if($zone){
    //             $selectedZone=$zone->id;
    //         }        
    //     }
    //     $occupanyChartData=CameraTransaction::zoneByOccupancyCount($selectedZone,$intervalBreak,$interval,'current_occupancy');
    //     $footFallChartData=CameraTransaction::zoneWiseFootFall($selectedZone,$intervalBreak,$interval,'enter');
    //     $occupancyViolationChartData=CameraTransaction::zoneWiseViolation($selectedZone,$intervalBreak,$interval,'occupancy_violation');

        
    //     $occupancyCount=CameraTransaction::ZoneWiseCount($selectedZone,$selectedDate,'current_occupancy');
    //     $footFallCount=CameraTransaction::ZoneWiseCount($selectedZone,$selectedDate,'in');
    //     $occupancyViolationCount=CameraTransaction::zoneWiseViolationCount($selectedZone,$intervalBreak,$interval,'occupancy_violation');

    //     return view('home',compact('zones','selectedZone','selectedDate','occupancyCount','footFallCount','occupancyViolationCount','intervalBreakData','occupanyChartData','footFallChartData','occupancyViolationChartData'));
    // }
    

    // function getWeeks($date, $rollover)
    // {
    //     $cut = substr($date, 0, 8);
    //     $daylen = 86400;

    //     $timestamp = strtotime($date);
    //     $first = strtotime($cut . "00");
    //     $elapsed = ($timestamp - $first) / $daylen;

    //     $weeks = 1;

    //     for ($i = 1; $i <= $elapsed; $i++)
    //     {
    //         $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
    //         $daytimestamp = strtotime($dayfind);

    //         $day = strtolower(date("l", $daytimestamp));

    //         if($day == strtolower($rollover))  $weeks ++;
    //     }

    //     return $weeks;
    // }


    // function weeks_in_month($month, $year) {
    //     // Start of month
    //     $start = mktime(0, 0, 0, $month, 1, $year);
        
    //     // End of month
    //     $end = mktime(0, 0, 0, $month, date('t', $start), $year);
    //     // Start week
    //     $start_week = date('W', $start);
    //     // End week
    //     $end_week = date('W', $end);
       
    //     if ($end_week < $start_week) { // Month wraps
    //       return ((52 + $end_week) - $start_week) + 1;
    //     }
       
    //     return ($end_week - $start_week) + 1;
    //    }


    // function cameraCount(){
    //      $camera=DB::table('options')->where('id',1)->first();
    //      $cameraData=$camera->option_value;
    //      $cameraData=json_decode($cameraData,true);
    //      $current_occupancy=$cameraData['in']-$cameraData['out'];
    //      if($current_occupancy<1){
    //          $current_occupancy=0;
    //      }
    //      $data['in']=$cameraData['in'];
    //      $data['out']=$cameraData['out'];
    //      $data['current_occupancy']=$current_occupancy;
    //      return json_encode($data);
    // }
    // function weekOfMonth($date) {
    //     //Get the first day of the month.
    //     $firstOfMonth = strtotime(date("Y-m-01", $date));
    //     //Apply above formula.
    //     return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
    // }
    
    // function weekOfYear($date) {
    //     $weekOfYear = intval(date("W", $date));
    //     if (date('n', $date) == "1" && $weekOfYear > 51) {
    //         // It's the last week of the previos year.
    //         $weekOfYear = 0;    
    //     }
    //     return $weekOfYear;
    // }

    // function postZoneSanitisation(Request $request){
    //         $zone_id=$request->zone_id;
    //         $zone_notification_id=$request->zone_notification_id;
    //         $remark=$request->remark;
    //         $status=$request->status;

    //         //Find device
    //         $cameras = Camera::select('device_id')->where('zone_id',$zone_id)->get();
    //         if($cameras){
    //             foreach($cameras as $camera){
    //                 $device_id = $camera->device_id;
    //                 $cameraTransaction = CameraTransaction::select('id','in')->where('device',$device_id)->orderBy('id','desc')->first();
    //                 if($cameraTransaction){
    //                     $cameraTransaction->base_value =$cameraTransaction->in;
    //                     $cameraTransaction->save();
    //                 }
    //             }
    //         }

    //         $zoneUpdate=[];
    //         $currentTime=date('Y-m-d H:i:s');
    //         $zoneUpdate['last_sanitisation'] =  $currentTime;
    //         $zoneUpdate['zone_notification_id']="";
    //         $zoneUpdate['sanitisation_alert']=0;
    //         //$zoneUpdate['notification_sent']="";
    //         Zone::where('id',$zone_id)->update($zoneUpdate);

    //         //Zone notification update
    //         $zoneNotificationUpdate=[];
    //         $zoneNotificationUpdate['remark']=$remark;
    //         $zoneNotificationUpdate['status']=$status;
    //         $zoneNotificationUpdate['resolve_time']=$currentTime;
    //         if($zone_notification_id){
    //             DB::table('zone_notification')->where('id',$zone_notification_id)->update($zoneNotificationUpdate);
    //         }    
    //         return redirect()->back();
    // }

    
}

