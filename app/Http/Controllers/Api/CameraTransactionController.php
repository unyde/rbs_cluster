<?php

namespace App\Http\Controllers\Api;
use Carbon\Carbon;
use Pusher\Pusher;
use App\User;
use App\Helpers\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;
use App\CameraTransaction;
use App\Camera;
use App\Zone;
use App\Branch;
use Illuminate\Support\Facades\DB;
use Config;
use Storage;
class CameraTransactionController extends ApiController
{
    private $defaultInterval;
    function __construct(){
        $this->defaultInterval=Config::get('app.chart_interval');
    }
    /**
 * @ Add camera request
 */
function addCameraTransaction(Request $request){
    $camera_input=Config::get('app.camera_input');
    if(!$camera_input){
        return "Camera request off. Please try again.";
    }

    $in       = $request->in;
    $out      = $request->out;
    $totalIn  = $in;
    $totalOut = $out;
    $totalNegative=0;
    $device   = $request->device;
    $device_mac=$request->device_mac;
    $current_occupancy =0;
    $zone_id  ="";
    $camera_id ="";
    $branch_id=0; 
    $branchStartTime="";
    $zone_total_in=0;
    $zone_total_enter=0;
    if(!$device){
        return "Device id missing";
    }
    // Camera::deviceUpdate($device);
    //  exit; 

    //Fetch device from camera table - Check device exist in db or not
    $camera = Camera::select('cameras.id','zone_id','reboot_type','max_occupancy','zones.branch_id','start_time','end_time')
    ->join('zones','zone_id','=','zones.id')
    ->join('branches','zones.branch_id','=','branches.id')    
    ->where('device_id',$device)->first();
        
    if($camera){
        $zone_id=$camera->zone_id;
        $reboot_type = $camera->reboot_type;
        $camera_id   = $camera->id;
        $max_occupancy=$camera->max_occupancy;
        $branch_id=$camera->branch_id;
        $branchStartTime=Common::getDateFromTime($camera->start_time);
        $branchTiming=[];
        $branchTiming['start_time']=$branchStartTime;
        $branchTiming['end_time']=Common::getDateFromTime($camera->end_time);;

        if(empty($start_time)){
            $this->notFound('Branch start time not set.');
        }
        
        $currentTime=Common::currentTime();
        if($currentTime>=$branchTiming['start_time'] && $currentTime<=$branchTiming['end_time']){

        }
        else{
            return "Current time not valid with site time";
        }
        //Save device status in db 
        // $camera->last_request=Common::currentTime();
        // $camera->save();
        


        //If device reboot type is interrupt then data is not insert. Return false
        if($reboot_type=='interrupt'){
            $data['status']  = false;
            $data['message'] = "Rebot type - interrupt. Transaction is not added.";
            $data['data']    = [];
            return $this->sendResponse($data);
        }
    }
    else{
        return "Device not found"; 
    }
   //End Fetch device from camera table 

    //Fetch zone max_occupancy
    // $zoneRow =Zone::select('max_occupancy','branch_id')
    //             ->join('branches','branch_id','=','branches.id')
    //             ->where('zones.id',$zone_id)->first();   
            
    // if(!$zoneRow){
    //     $this->notFound('Invalid zone id. Please try again!');
    // }                   
    // $max_occupancy=$zoneRow->max_occupancy;
    // $branch_id=$zoneRow->branch_id;
    // if(empty($branch_id)){
    //     $this->notFound('Invalid branch id. Please try again!');
    // }
    //End Fetch zone max_occupancy
    
    //Fetch branch start time
    // if($branch_id){
    //     $branchStartTime=Branch::branchStartTime($branch_id);
    // }
    // if(empty($branchStartTime)){
    //     $this->notFound('Branch start time not set!');
    // }
    
    //Fetch all device id for this zone except current device
    $cameras =Camera::select('id')->where('zone_id',$zone_id)
                     ->where('id','<>',$camera_id)
                     ->get();
                     
   
    if($cameras){
        foreach($cameras as $cameraDevice){
            //Fetch camera id
            $cameraDeviceId = $cameraDevice->id;
            
            //Fetch this device last entry in camera transaction table
            if($cameraDeviceId){
                $cameraTransaction = CameraTransaction::select('in','out')
                                    ->where('camera_id',$cameraDeviceId)
                                    ->where('created_at','>=',$branchStartTime)
                                    ->orderBy('id','desc')
                                    ->first();
                                   
                //If record exists then fetch in,out and sum in totalIn,totalOut
                if($cameraTransaction){
                    $totalIn+=$cameraTransaction->in;
                    $totalOut+=$cameraTransaction->out;
                }                    
            } 
        }
    }
 
    
    //Fetch all negative value for this zone greater then branch start time and value less than 0
    $totalNegative = CameraTransaction::where('zone_id',$zone_id)
                                    ->where('delta_in_out','<',0)
                                    ->where('created_at','>=',$branchStartTime)
                                    ->sum('delta_in_out');
                                   
                                   
                             

    //Calculate occupancy = totalIn-(totalOut+totalNegative)
    $calculatedOccupancy  = $totalIn-($totalOut+$totalNegative); 
    $current_occupancy    = $calculatedOccupancy; 
    $delta_in_out         = $calculatedOccupancy; 

    //Zone total in 
    $zone_total_in = $totalIn;
    
    //Calculate zone total enter= $zone_total_in- find last entry from this zone
    $zoneLastTransaction = CameraTransaction::select('zone_total_in')
                            ->where('zone_id',$zone_id)
                            ->where('created_at','>=',$branchStartTime)
                            ->orderBy('id','desc')
                            ->first();
                           
                            
    if($zoneLastTransaction){
        $previousZoneTotalIn=$zoneLastTransaction->zone_total_in;
        $zone_total_enter=$zone_total_in-$previousZoneTotalIn;
        if($zone_total_enter<0){
            $zone_total_enter=0;
        }
    }    
   
    // If current occupancy less than 0 then set 0
    if($current_occupancy<0){
        $current_occupancy=0;
    }

    $occupancy_violation=0;
    $data=[];
   // echo $current_occupancy."=".$max_occupancy; exit; 
    //If zone has has set max occupancy then go to if block
    if($max_occupancy){
        //If current_occupancy greater than zone max occupancy
         if($current_occupancy>$max_occupancy){
            // $occupancy_violation=1;
             $violationCheck = CameraTransaction::where(['zone_id'=>$zone_id,'occupancy_violation'=>1])
                               ->whereNull('violation_end')    
                               ->orderBy('id','desc')
                               ->take(1)
                               ->get()
                               ->count(); 
                              // Common::queryLog();
                      
             if($violationCheck){
                $occupancy_violation=2;
             }                   
             else{
                $occupancy_violation=1;
                $data['violation_start']=date('Y-m-d H:i:s');
             }
         }
         else{
            $occupancy_violation=0;

            // If current_occupancy less than zone max occupancy
            $violationCheck = CameraTransaction::where(['zone_id'=>$zone_id,'occupancy_violation'=>1])
                                ->whereNull('violation_end')    
                                ->orderBy('id','desc')
                                ->first();
                              
            if($violationCheck){
                $violationCheck->id;
                $currentTime=date('Y-m-d H:i:s');
                $violationCheck->violation_end=$currentTime;
               
                $from_time=strtotime($violationCheck->violation_start); 
                $to_time=strtotime($currentTime);
                $duration = round(abs($to_time - $from_time) / 60,2);
                $violationCheck->violation_duration=$duration;
                $violationCheck->save();    
               
            }                   
             $occupancy_violation=0;
         }  

    }
   
    //Find last total entry
    //Total entry last entry - current in 
    $lastEntry =CameraTransaction::findLastRecord($device,'in');
   

    $entry = $in-$lastEntry; 
    if($entry<0){
        $entry=0;
    }
    
    $data['enter']=$entry;
    $data['in']=$in;
    $data['out']=$out;
    $data['current_occupancy']=$current_occupancy;
    $data['device']=$device;
    $data['device_mac']=$device_mac;
    $data['occupancy_violation']=$occupancy_violation;
    $data['zone_id']=$zone_id;
    $data['camera_id']=$camera_id;
    // $data['base_in']=$base_in;
    // $data['base_out']=$base_out;
    $data['delta_in_out']=$delta_in_out;
    $data['zone_total_in']=$zone_total_in;
    $data['zone_total_enter']=$zone_total_enter;
    
    //Save data in file for chart
    // $branch=Branch::select('start_time','end_time')
    // ->where('id',$branch_id)
    // ->first();
    
    // $start_time="";
    // $end_time="";
    // if($branch){
    //     $start_time=$branch->start_time;
    //     $end_time=$branch->end_time;
    //     if($start_time){
    //       $start_time = date('Y-m-d H:i:s',strtotime($start_time));
    //     }
    //     if($end_time){
    //         $end_time = date('Y-m-d H:i:s',strtotime($end_time));
    //     }
    // }
    // $branchTiming=[];
    // $branchTiming['start_time']=$start_time;
    // $branchTiming['end_time']=$end_time;
    

   
    /**
     * Write data in file for chart display- 
     */
    //  $currentTime=date('Y-m-d H:i:s');
    //  $currentDate=date('d-m-Y');
    //  $occupancyContent ="";
    //  $folderName="data/$currentDate/";
    //  $zoneOccupancyFileName=$folderName."zone_$zone_id"."_"."occupancy.csv";
    //  $previousOccupancyContent="Time, Total";
    //  $baseRoot="public/";
    //  if(Storage::exists($baseRoot.$zoneOccupancyFileName)) {
    //     $previousOccupancyContent=Storage::get($baseRoot.$zoneOccupancyFileName);
    //  }
    //  $previousOccupancyContent.="\n".$currentTime.','.$current_occupancy;
    //  Storage::disk('public')->put($zoneOccupancyFileName, $previousOccupancyContent);
    
     

  
    //rebot type innert
    
    //Find last record by device id
        // $cameraTransaction = CameraTransaction::select('created_at','id','current_occupancy')
        //                         ->where(['zone_id'=>$zone_id])
        //                             ->orderBy('id','desc')
        //                             ->first();
        // if($cameraTransaction){
        //     $created_at = $cameraTransaction->created_at;
        //     $currentTime = date('Y-m-d H:i:s');
        //     $startTime = Carbon::parse($created_at);
        //     $finishTime = Carbon::parse($currentTime);
        //     $duration = $finishTime->diffInSeconds($startTime);
        //     $total_duration = $duration*$cameraTransaction->current_occupancy;
        //     if($total_duration<0){$total_duration=0;}
        //     //Update duration in last record
        //     $updateData=[];
        //     $cameraTransaction->duration=$duration;
        //     $cameraTransaction->total_duration=$total_duration;
        //     $cameraTransaction->save();
        //  }
        // //End find last record by device id                            
        
        // //Find base value
        // $cT=CameraTransaction::where('device',$device)->where('base_value','>',0)->orderBy('id','desc')->first();
        // if($cT){
        //     $baseValue =  $cT->base_value;
        //     $visit_count = $in-$baseValue;
        //     if($visit_count<0){
        //         $visit_count=0;
        //     }
        //     $data['visit_count']=$visit_count;
        // }  
        // else{
        //     $data['visit_count']=$in;
        // }                          
        
        // $cameratransaction=CameraTransaction::create($data);

        // //Zone sanitisation alert
        // //$this->zoneSanitisationAlert($device);     
   # print_r($data); exit; 
    $cameratransaction=CameraTransaction::create($data);  
   
    //File write Off 
    $this->addDataInFile("zone_$zone_id"."_footfall.csv",$zone_total_enter,$branchTiming);
   
    // if($occupancy_violation==2){
    //     $occupancy_violation=1;
    // }
    if($max_occupancy){
        $this->addDataInFile("zone_$zone_id"."_violation.csv",$occupancy_violation,$branchTiming);
    }
    
    $this->addDataInFile("device_$camera_id"."_footfall.csv",$entry,$branchTiming);
    $this->addDataInFile("zone_$zone_id"."_occupancy.csv",$current_occupancy,$branchTiming);
   
    
    $response['status']  =true;
    $response['message'] = "Save data successfully!";
    $response['data']    = ['id'=>$cameratransaction->id];
    //Common::queryLog();
   
    //$this->addSocket($zone_id,$camera_id,$device);
    return $this->sendResponse($response);
}

    /**
     * Add data in file for chart display. This file store in storage/public/data
     */
    function addDataInFile($fileName,$data,$branchTiming){
        // return; 
        if(!Config::get('app.camera_input_file')){
            return;
        }
        // if(empty($selectedDate)){
        //     $selectedDate=date('Y-m-d H:i:s');
        // }

       // $selectedDate=date('Y-m-d H:i:s');
        $currentTime = date('Y-m-d H:i:s',strtotime($selectedDate));
        $currentDate = date('d-m-Y',strtotime($selectedDate));
        $start_time  = @$branchTiming['start_time'];
        $end_time    = @$branchTiming['end_time'];
        if($currentTime>=$start_time && $currentTime<=$end_time){
            //Code 
        }
        else{
            $this->notFound('current time less than or greater than branch.');
        }
        
      //  $nextTime   = date("Y-m-d H:i:s", strtotime("+30 second", strtotime($selectedDate)));
        $fileName  = "data/$currentDate/".$fileName;
        
        //$fileName    = $folderName.$fileName;
        
        $fileStart   = "Time, Total";
        //$fileStart  .="\n".$start_time.',0';
        
        // $fileEnd     = "\n".$nextTime.',0';
        // $fileEnd    .= "\n".$end_time.',0';

        $fileContent="";
        $baseRoot    = "public/";
        if(Storage::exists($baseRoot.$fileName)) {
            $fileContent=Storage::get($baseRoot.$fileName);
           // $newContent=explode(PHP_EOL,$fileContent);
            // array_pop($newContent);
            // array_pop($newContent);
            //array_shift($newContent);
           // array_shift($newContent);
            //$fileContent="\n".implode(PHP_EOL,$newContent);
            // print_r($newContent);
            // echo $fileContent;
            //  exit; 
        }
        
        // $fileFinalContent=$fileStart;
        // if($appendData){
            $fileContent.="\n".$currentTime.','.$data;
            //$fileFinalContent.=$fileContent; 
        // }
   // $fileFinalContent.=$fileEnd;
        //$fileFinalContent=$fileStart.$fileContent."\n".$end_time.',0';
        //echo $fileFinalContent; exit; 
       
        Storage::disk('public')->put($fileName, $fileContent);
        
    }

    function getDeviceRebootStatus(Request $request){
    //     $validator = Validator::make($request->all(), [
    //     'device_id' => 'required',
    //   ]);
    // if ($validator->fails()) {
    //     $response['status']  =false;
    //     $response['message'] = trans('message.validation');;
    //     $response['data']    = $validator->errors();
    //     return $this->sendResponse($response);
    // }
    $device_id=$request->device_id;
    $device =Camera::select('id','reboot','counter_restart','height')->where('device_id',$device_id)->first();
    $response['status']  =true;
    $response['message'] = "Added";
    $cameraObj=new Camera();
    $response['data']    = ['device'=>$device,'deviceCount'=>$cameraObj->getDeviceLastRecord($device_id,true)];
    Camera::deviceUpdate($device_id);
    return $this->sendResponse($response);

    }

    function updateDeviceRebootStatus(Request $request){
    //     $validator = Validator::make($request->all(), [
    //     'id' => 'required',
    //   ]);
    // if ($validator->fails()) {
    //     $response['status']  =false;
    //     $response['message'] = trans('message.validation');;
    //     $response['data']    = $validator->errors();
    //     return $this->sendResponse($response);
    // }
    $device_id=$request->device_id;
    $last_reboot=date('Y-m-d H:i:s');
    $reboot_type=$request->reboot_type;
    $device =Camera::where('device_id',$device_id)->first();
    if(!$device){
        $response['status']  =false;
        $response['message'] = "Device id not found".$device_id;
        $response['data']    = [];
        return $this->sendResponse($response);     
    }

    $reboot=$device->reboot;
    $counter_restart=$device->counter_restart;

    if($reboot_type=='interrupt'){
        $reboot= 1;
        $counter_restart= 0;
    }
    else{
        $reboot= 0;
    }
    
    //In
    //reboot
    //counter resta=0

    $device =Camera::where('device_id',$device_id)
    ->update(['reboot'=>$reboot,'counter_restart'=>$counter_restart,'last_reboot'=>$last_reboot,'reboot_type'=>$reboot_type]);
    $response['status']  =true;
    $response['message'] = "Updated successfully";
    $response['data']    = [];
    Camera::deviceUpdate($device_id);
    return $this->sendResponse($response);

    }


function addSocket($zone_id,$camera_id,$device_id){
    $currentDate=date('Y-m-d');
    // $previousDate=date('Y-m-d',strtotime('-1 day',strtotime($currentDate)));
    // $lastWeekDate = date('Y-m-d',strtotime('-7 day',strtotime($currentDate)));
    // $lastMonthDate = date('Y-m-d',strtotime('-30 day',strtotime($currentDate)));
      

    //Socket code for home page
    $options = array(
        'cluster' => 'ap2',
        'useTLS' => true
    );
    
    $pusher =  new Pusher(
        Config::get('app.pusher_app_key'),
        Config::get('app.pusher_app_secret') ,
        Config::get('app.pusher_app_id') ,
        $options
    );
    $data=[];
    $where=[];
    $data['zone_id']=$zone_id;
    $data['new_zone_id']=$zone_id;
    $data['camera_id']=$camera_id;
    $camera=Camera::where('device_id',$device_id)->first();
    if($camera){
       $company_id=$camera->company_id;
       $branch_id=$camera->branch_id;
       $where['cameras.company_id']=$company_id;
       $where['cameras.branch_id']=$branch_id;
       $where['camera_transactions.zone_id']=$zone_id;
       #$where['camera_id']=$camera_id;
       $data['company_id']=$company_id;
       $data['branch_id']=$branch_id;
    }
    else{
        echo "Not found";
        exit;
    }   
   
    $interval=$this->defaultInterval;//Minute
    $intervalBreakData=CameraTransaction::getTimeSlot($branch_id,$currentDate,$interval);
    $intervalBreak=@$intervalBreakData['timeBreak'];
    
    //$homeData=Camera::getCurrentOccupancy($where);
    #$homeData=Camera::getCurrentOccupancy($zone_id,$currentDate);
    $occupancyCount=CameraTransaction::ZoneWiseCount($zone_id,$currentDate,'current_occupancy');
    $footFallCount=CameraTransaction::ZoneWiseCount($zone_id,$currentDate,'in');
   // $occupancyViolationCount=CameraTransaction::ZoneWiseCount($zone_id,$currentDate,'occupancy_violation');
    
   $occupancyViolationCount=CameraTransaction::zoneWiseViolationCount($zone_id,$intervalBreak,$interval,'occupancy_violation');


    $data['current_occupancy']=$occupancyCount;
    $data['totalFootFall']=$footFallCount;

    //Current Voilation
    //$occupancyViolation=Camera::getCurrentViolation($where);
  
    
    $data['occupancyViolation']=$occupancyViolationCount;
   
    $occupanyChartData=CameraTransaction::zoneByOccupancyCount($zone_id,$intervalBreak,$interval,'current_occupancy');
    $data['occupanyChartData']=$occupanyChartData;
    
  //  $footFallChartData=CameraTransaction::zoneByOccupancyCount($zone_id,$intervalBreak,$interval,'in');
  $footFallChartData=CameraTransaction::zoneWiseFootFall($zone_id,$intervalBreak,$interval,'in');

    $data['footFallChartData']=$footFallChartData;

    $violationChartData=CameraTransaction::zoneWiseViolation($zone_id,$intervalBreak,$interval,'occupancy_violation');

    $data['violationChartData']=$violationChartData;

   

    // //Yesterday Data
    // $homeData2=Camera::getCurrentOccupancy($zone_id,$previousDate);
    
    // $yesterdayColor="#ff0000";
    // $yesterdayCount=0;
    // $totalFootFallYesterday=$homeData2['totalFootFall'];
    // $totalFootFall=$homeData['totalFootFall'];
    // $totalFallInWeek=Camera::getTotalFallIn($where,$lastWeekDate);
    // $totalFallInMonth=Camera::getTotalFallIn($where,$lastMonthDate);

    // //Yesterday
    // if($totalFootFallYesterday>$totalFootFall){
    //     $yesterdayCount=$totalFootFallYesterday-$totalFootFall;
    // }
    // else {
    //     $yesterdayCount=$totalFootFall-$totalFootFallYesterday;
    //     $yesterdayColor="#228B22";
    // }
    // $data['yesterdayColor']=$yesterdayColor;
    // $data['yesterdayCount']=$yesterdayCount;

    // //Last Week
    // $weekColor="#ff0000";
    // $weekCount=0;
    // if($totalFallInWeek>$totalFootFall){
    //   $weekCount=$totalFallInWeek-$totalFootFall;
    // }
    // else {
    //   $weekCount=$totalFootFall-$totalFallInWeek;
    //   $weekColor="#228B22";
    // }
    // $data['weekColor']=$weekColor;
    // $data['weekCount']=$weekCount;
    
    // //Last Month
    // $monthColor="#ff0000";
    // $monthCount=0;
    // if($totalFallInMonth>$totalFootFall){
    //     $monthCount=$totalFallInMonth-$totalFootFall;
    // }
    // else {
    //     $monthCount=$totalFootFall-$totalFallInMonth;
    //     $monthColor="#228B22";
    // }
    // $data['monthColor']=$monthColor;
    // $data['monthCount']=$monthCount;





//    print_r($data); exit; 

    
    $pusher->trigger('my-channel', 'home', $data);



}

function zoneSanitisationAlert($device){
    $sanitisation_alert=false;
    //Find zone by device
    $camera=Camera::select('zone_id')->where('device_id',$device)->first();
    if($camera){
        $zone_id = $camera->zone_id;
        $branch_id="";
        $visit_limit=0;
        $zone=Zone::select('visit_limit','sanitisation','sanitisation_alert','branch_id')->where('id',$zone_id)->first();
        $zoneAlertNotification=false;
        if($zone){
            if($zone->sanitisation){
                $zoneAlertNotification=true;
                $visit_limit=$zone->visit_limit;
                $sanitisation_alert=$zone->sanitisation_alert;
                $branch_id=$zone->branch_id;
            }
        }
        if($zoneAlertNotification && !$sanitisation_alert){
            
            $cameras  = Camera::select('device_id')->where('zone_id',$zone_id)->get()->toArray();
            $device_lists=[];
            foreach($cameras as $camera){
                $device_lists[]=$camera['device_id'];
            }
            $total_in=0;
            $currentTime=date('Y-m-d');
            foreach($device_lists as $device){
                $cameraTransaction=CameraTransaction::
                whereDate('created_at',$currentTime)
                ->where('device',$device)
                ->take(1)
                ->orderBy('id','desc')
                ->first();
                if($cameraTransaction){
                    $total_in +=$cameraTransaction->visit_count;
                }
            }
            //Check visit limit
            if($total_in>$visit_limit){
                $currentTime = date('Y-m-d H:i:s');
                $branch=Branch::where('id',$branch_id)->first();  
                  
                if($branch){
                $otp="Zone alert notification";
                $mobile=$branch->mobile;
                if($mobile){
                $messageText="Your verification code is $otp . Just enter this code to verify your number -UNYDES";
                Common::sendSMS($mobile,$messageText,'1207160922705121024');

                //Zone notificaton
                $zoneAlertData=[];
                $zoneAlertData['zone_id'] = $zone_id;
                $zoneAlertData['max_allow_person']=$visit_limit;
                $zoneAlertData['total_in_person']= $total_in;
                $zone_notification_id=DB::table('zone_notification')->insertGetId($zoneAlertData);
                if($zone_notification_id){
                    $zoneUpdate=[];
                    $zoneUpdate['last_sanitisation']=null;
                    $zoneUpdate['sanitisation_alert']=1;
                    $zoneUpdate['notification_sent']=$currentTime;
                    $zoneUpdate['zone_notification_id']=$zone_notification_id;
                    Zone::where('id',$zone_id)->update($zoneUpdate);
                }
                }
            }
                

                


            }
        }
    }
}

/**
 * Reset
 */
function resetCamera(Request $request){
    $where=[];
    $where['status']=1;
    $device_id=$request->device_id;
    $zone_id=$request->zone_id;
   
    if($zone_id){
        $where['id']=$zone_id;
    }
    //Pacific
    $where['zones.branch_id']=12;

    
    /**
     * Remove existing file from storage/app/public/data/date-
     */
    $currentDate=date('Y-m-d');
    $zones=Zone::select('zones.id','branch_id','zone_name')
                ->where($where)
                ->get();
                //->toArray();
                // echo "<pre/>";
                // print_r($zones);
                //  exit; 
             
    if($zones){
        foreach($zones as $zone){
            $zone_id=$zone->id; 
            $branch_id=$zone->branch_id;
            $zone->occupancy_last_id=0;
            $zone->occupancy_last_time=null;
            $zone->footfall_last_id=0;
            $zone->footfall_last_time=null;
            $zone->violation_last_id=0;
            $zone->violation_last_time=null;
            $zone->save();
            
            
            $this->deleteFile("zone_$zone_id"."_occupancy.csv",$branch_id);
            $this->deleteFile("zone_$zone_id"."_footfall.csv",$branch_id);
            $this->deleteFile("zone_$zone_id"."_violation.csv",$branch_id);

            //Remove All data for this zone removed
            // CameraTransaction::where('zone_id',$zone_id)
            //                 ->whereDate('created_at','=',$currentDate)
            //                 ->delete();
        }
    }

    $where=[];
    $where['status']=1;
    $zone_id=$request->zone_id;
    if($zone_id){
        $where['zone_id']=$zone_id;
    }
    $where['branch_id']=12;
    //print_r($where);
    $cameras=Camera::where($where)->get();
    // echo "<pre/>";
    // print_r($cameras); exit;  
    if($cameras){
        foreach($cameras as $camera){
            $branch_id=$camera->branch_id;

            $camera->ct_last_id=0;
            $camera->ct_last_time=null;
            $camera->save();

            
            $data=[];
            $data['zone_id']=$camera->zone_id;
            $data['camera_id']=$camera->id;
            $data['device']=$camera->device_id;
            $data['device_mac']=$camera->device_mac;
            $data['in']=0;
            $data['out']=0;
            $data['current_occupancy']=0;
            $data['occupancy_violation']=0;
            //Reset device data in camera transaction table
            CameraTransaction::create($data);
            
            //Update camera
            $camera->reboot=1;
            $camera->reboot_type="interrupt";
            $camera->counter_restart=1;
            $camera->save();

            //File Delete 
            $camera_id=$camera->id; 
            $this->deleteFile("device_$camera_id"."_footfall.csv",$branch_id);


        }
    }
    $status=true;
    $message="Camera reset successfully.";
    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
    return $this->sendResponse($response);
}

/**
 * Delete File
 */
function deleteFile($filename,$branch_id){
    $start_time="";
    $file_extension=strrchr($filename,'_');
     
    if($branch_id){
        $start_time =Branch::branchStartTime($branch_id);
    }
    
    $currentDate=date('d-m-Y');
    $folderName  = "data/$currentDate/";
    $filename    = $folderName.$filename;
    $baseRoot="public/";
    if(\Storage::exists($baseRoot.$filename)){
        \Storage::delete($baseRoot.$filename);
      }else{
       // dd('File does not exists.');
    }

    //File Create
    $fileStart   = "Time, Total";
    if($file_extension=="_occupancy.csv"){
        $fileStart   = "Time, Occupancy";
    }
    else if($file_extension=="_footfall.csv"){
        $fileStart   = "Time, Foot Fall";
    }
    else if($file_extension=="_violation.csv"){
        $fileStart   = "Time, Violation";
    }
   
    if(empty($start_time)){
        $start_time =date('Y-m-d')." 08:00:00";
    }
    $fileStart  .="\n".$start_time.',0';
    $fileFinalContent=$fileStart;
    
    Storage::disk('public')->put($filename, $fileFinalContent);



}

function resetCameraOLD(Request $request){
    $where=[];
    $where['status']=1;
    $device_id=$request->device_id;
    if($device_id){
        $where['device_id']=$device_id;
    }
    $cameras=Camera::where($where)->get(); 
    if($cameras){
        foreach($cameras as $camera){
            $data=[];
            $data['zone_id']=$camera->zone_id;
            $data['camera_id']=$camera->id;
            $data['device']=$camera->device_id;
            $data['device_mac']=$camera->device_mac;
            $data['in']=0;
            $data['out']=0;
            $data['base_in']=0;
            $data['base_out']=0;

            $data['current_occupancy']=0;
            $data['occupancy_violation']=0;
            //Reset device data in camera transaction table
            CameraTransaction::create($data);
            
            //Update camera
            $camera->reboot=1;
            $camera->counter_restart=1;
            $camera->save();

        }
    }
    $status=true;
    $message="Camera reset successfully.";
    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
    return $this->sendResponse($response);
}

function updateCamera(Request $request){
    $where=[];
    $device_id=$request->device_id;
    $lan=$request->lan;
    
    $status=false;
    $message="Please try again!";
    $data=[];
    
    if($device_id){
        $where['device_id']=$device_id;
        $camera=Camera::where($where)->first(); 
        if($camera){
            $camera->lan=$lan;
            $camera->save();
            $status=true;
            $message="Camera lan update successfully.";


        }
    }
    
    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
   
    return $this->sendResponse($response);
   
}

/**
 * File create for chart before one day
 */
// function chartFileCreate(){
//     $branchs =Branch::select('id','start_time','end_time')->get();
//     /*
//       $this->addDataInFile("zone_$zone_id"."_occupancy.csv",$current_occupancy,$branchTiming);
//     $this->addDataInFile("zone_$zone_id"."_footfall.csv",$zone_total_enter,$branchTiming);
   
//     if($occupancy_violation==1 || $occupancy_violation==0){
//         $this->addDataInFile("zone_$zone_id"."_violation.csv",$occupancy_violation,$branchTiming);
//     }
//     $this->addDataInFile("device_$camera_id"."_footfall.csv",$entry,$branchTiming);

//      */
//     $selectedDate=date('d-m-Y');
//     if($branchs){
//         foreach($branchs as $branch){
//             $branch_id=$branch->id;
//             $start_time=$branch->start_time;
//             $end_time=$branch->end_time;
//             if(!$start_time || !$end_time){
//                 continue;
//             }
//             $start_time=date('H:i:s',strtotime($start_time));
//             $start_time= $selectedDate." ".$start_time;
//             $start_time=date('Y-m-d H:i:s',strtotime($start_time));
            
//             $end_time=date('H:i:s',strtotime($end_time));
//             $end_time= $selectedDate." ".$end_time;
//             $end_time=date('Y-m-d H:i:s',strtotime($end_time));

//             $branchTiming['start_time']=$start_time;
//             $branchTiming['end_time']=$end_time;
//             $selectedDate=$start_time; 
//             // echo "$branch_id  = ".$end_time."=".$selectedDate;
//             // echo "<br/>";
            
//             $zones=Zone::where('branch_id',$branch_id)->get();
//             if($zones){
//                 foreach($zones as $zone){
//                     $zone_id=$zone->id;
//                     $this->addDataInFile("zone_$zone_id"."_occupancy.csv",0,$branchTiming,$selectedDate,false);
//                     $this->addDataInFile("zone_$zone_id"."_footfall.csv",0,$branchTiming,$selectedDate,false);
//                     $this->addDataInFile("zone_$zone_id"."_footfall.csv",0,$branchTiming,$selectedDate,false);
                   
//                 }
//             }
            

//         }
//     }
// }

/**
 *Zone File write in csv 
 */
function zoneFileWrite(Request $request,$file_end){
    $status=true;
    $message=false;
    $allow_extension=['occupancy','footfall','violation'];
    if(!in_array($file_end,$allow_extension)){
        return "Not allowed";
    }
    $zone_id = $request->zone_id;
    // $branch_id =12;// $request->branch_id;
    //$file_end =$request->file_end;
    $where['zones.status']=1;
    if($zone_id){
        $where['zones.id']=$zone_id;
    }
    if($branch_id){
        $where['zones.branch_id']=$branch_id;
    }

   

    $zones=Zone::fetchZoneList($where);
    $currentDate = Common::currentDate();
    $last_id=0;
    $fetchField="";
    $file_name_end="";
    $where=[];
    if($zones){
        foreach($zones as $zone){
            $start_time=$zone->start_time;
            if($start_time){
                $start_time=Common::getDateFromTime($start_time);
                $where['start_time']=$start_time;
            }  
            
            $end_time=$zone->end_time;
            if($end_time){
                $end_time=Common::getDateFromTime($end_time);
                $where['end_time']=$end_time;
            }  

            $zone_id=$zone->id;
            $where['zone_id']=$zone_id;
            $max_occupancy=$zone->max_occupancy;
            //Set data for file write
            $dataArray=[];
            $dataArray['id']=$zone_id;
            $dataArray['file_name_start']="zone";

            if($file_end=="occupancy"){
                $where['id']=$zone->occupancy_last_id;
                $fetchField="current_occupancy";
                $file_name_end="occupancy";
           }
           else if($file_end=="footfall"){
                $where['id']=$zone->footfall_last_id;
                $fetchField="zone_total_enter";
                $file_name_end="footfall";
            }
            else if($file_end=="violation" && $max_occupancy){
                $where['id']=$zone->violation_last_id;
                $fetchField="occupancy_violation";
                $file_name_end="violation";

                $dataArray['data']=CameraTransaction::cameraTransaction($where,$fetchField,$zone_id);
                $dataArray['file_name_end']=$file_name_end;
                $dataArray['start_time']=$start_time;
               
                Common::fileCreateOrUpdate($dataArray);


            }
            //print_r($where);
            if($file_end!='violation'){
            $dataArray['data']=CameraTransaction::cameraTransaction($where,$fetchField,$zone_id);
            $dataArray['file_name_end']=$file_name_end;
            $dataArray['start_time']=$start_time;
                
            Common::fileCreateOrUpdate($dataArray);
            
             }


            
        }
    }
   

    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
   
    return $this->sendResponse($response);
}
/**
 * Device file write in csv
 */
function deviceFileWrite(Request $request){
    $status=true;
    $message=false;
   
    $device = $request->device;
    $zone_id=$request->zone_id;
    $branch_id=$request->branch_id;
    //$file_end =$request->file_end;
    $where['cameras.status']=1;
    if($device){
        $where['cameras.device_id']=$device;
    }
    if($zone_id){
        $where['zone_id']=$zone_id;
    }
    if($branch_id){
        $where['branch_id']=$branch_id;
    }
    $rows=Camera::fetchCameraList($where);
  

    $currentDate = Common::currentDate();
    $last_id=0;
    $fetchField="";
    $file_name_end="";
    $where=[];
    if($rows){
        foreach($rows as $row){
            $start_time=$row->start_time;
            if($start_time){
                $start_time=Common::getDateFromTime($start_time);
                $where['start_time']=$start_time;
            }  
            
            $end_time=$row->end_time;
            if($end_time){
                $end_time=Common::getDateFromTime($end_time);
                $where['end_time']=$end_time;
            }  

            $camera_id=$row->id;
            $where['camera_id']=$camera_id;
           
            //Set data for file write
            $dataArray=[];
            $dataArray['id']=$camera_id;
            $dataArray['file_name_start']="device";
            $where['id']=$row->ct_last_id;
            $fetchField="enter";
            $file_name_end="footfall";
           
            $dataArray['data']=CameraTransaction::cameraTransaction($where,$fetchField,$camera_id,'device');
            $dataArray['file_name_end']=$file_name_end;
            $dataArray['start_time']=$start_time;
          
            Common::fileCreateOrUpdate($dataArray);

            
        }
    }
   

    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
   
    return $this->sendResponse($response);
}

 /**
     * Zone wise foot fall
     */
    public function zone_foot_fall(Request $request){
        $zones=$request->zone;
        $selected_date=$request->selected_date;
        if(empty($zones)) return;
        $zones=explode(',',$zones);
        $zones=array_flip($zones);
        $zone_foot_fall=CameraTransaction::fetchFootFallDate($zones,$selected_date);
        $zone_foot_fall=$zone_foot_fall;
       
        $response['status']  =true;
        $response['message'] = "";
        $response['data']    = ['foot_fall'=>$zone_foot_fall];
       
        return $this->sendResponse($response);
    }

    public function device_foot_fall(Request $request){
        $device=$request->device;
        $selected_date=$request->selected_date;
        if(empty($device)) return;
        $device=explode(',',$device);
        $device=array_flip($device);
  
        $device_foot_fall=CameraTransaction::fetchDeviceFootFallDate($device,$selected_date);
        $device_foot_fall=$device_foot_fall;
       
        $response['status']  =true;
        $response['message'] = "";
        $response['data']    = ['foot_fall'=>$device_foot_fall];
       
        return $this->sendResponse($response);
    }



}