<?php

namespace App\Http\Controllers\Api;
use Carbon\Carbon;
use Pusher\Pusher;
use App\User;
use App\Helpers\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;
use App\CameraTransaction;
use App\Camera;
use App\Zone;
use App\Branch;
use Illuminate\Support\Facades\DB;
use Config;
class CameraTransactionController2 extends ApiController
{
    private $defaultInterval;
    function __construct(){
        $this->defaultInterval=Config::get('app.chart_interval');
    }
    /**
 * @ Add camera request
 */
function addCameraTransaction(Request $request){
    $in       = $request->in;
    $out      = $request->out;
    $device   = $request->device;
    $device_mac=$request->device_mac;
    $current_occupancy =0;// $in-$out;
    $base_in  =0;
    $zone_id  ="";
    $camera_id ="";
    $base_out=0;

    $prevoiusIn=0;
    $previousOut=0;
    $base_in=0;
    $base_out=0;
    $previousOccupancy=0;
    $zone_id=0;
    $branch_id=0; 
    $branchStartTime="";
    if(!$device){
        return "Device id missing";
    }

    //Fetch device from camera table
    $camera = Camera::where('device_id',$device)->first();
    if($camera){
        $zone_id=$camera->zone_id;
        $reboot_type = $camera->reboot_type;
        $camera_id   = $camera->id;

        //If device reboot type is interrupt then data is not insert. Return false
        if($reboot_type=='interrupt'){
            $data['status']  = false;
            $data['message'] = "Rebot type - interrupt. Transaction is not added.";
            $data['data']    = [];
            return $this->sendResponse($data);
        }
    }
    else{
        return "Device not found"; 
    }
   //End Fetch device from camera table 

    //Fetch zone max_occupancy
    $zoneRow =Zone::where('id',$zone_id)->first();                    
    if(!$zoneRow){
        $this->notFound('Invalid zone id. Please try again!');
    }                   
    $max_occupancy=$zoneRow->max_occupancy;
    $branch_id=$zoneRow->branch_id;
    if(empty($branch_id)){
        $this->notFound('Invalid branch id. Please try again!');
    }
    //End Fetch zone max_occupancy
    
    //Fetch branch start time
    if($branch_id){
        $branchStartTime=Branch::branchStartTime($branch_id);
    }
    if(empty($branchStartTime)){
        $this->notFound('Branch start time not set!');
    }
    
    //Fetch last entry from this device in camera transaction table
    $lastRecord=CameraTransaction::where('device',$device)
                                ->where('created_at','>=',$branchStartTime)
                                ->orderBy('id','desc')
                                ->first();
    if($lastRecord){
        $prevoiusIn=$lastRecord->in;
        $previousOut=$lastRecord->current_occupancy;
        $base_in=$lastRecord->base_in;
        $base_out=$lastRecord->base_out;
        $previousOccupancy=$lastRecord->current_occupancy;
    }
    //End Fetch last entry from this device in camera transaction table

    //If out greater than in 
    if($out>$in){
        if($lastRecord){
            $in_delta=($in-$base_in); 
            $out_delta=($out-$base_out); 
            if($in_delta>0){
                if($in_delta<$out_delta){
                    $current_occupancy=0;
                    $base_in=$in;
                    $base_out=$out;
                }
                else{
                    $current_occupancy=$previousOccupancy+($in_delta-$out_delta);
                    $base_in=$in;
                    $base_out=$out;
                  }
            }
            else if($in_delta==0){
                if($out_delta>0 && $previousOccupancy>0){
                    $current_occupancy=$previousOccupancy-$out_delta;
                }
                else{
                    $current_occupancy=0;
                }
                $base_out=$out;
            }
        } 
        else{
            $base_in = $in; 
            $base_out=$out; 
            $current_occupancy=0;
        } 
    }          
    else{
        $in_delta=($in-$base_in);
        $out_delta=($out-$base_out);
        $current_occupancy=($in_delta-$out_delta)+$previousOccupancy;
        $base_in=$in;
        $base_out=$out;
    }
    //End If-else out greater than in


    
   
   

    // If current occupancy less than 0 then set 0
    if($current_occupancy<0){
        $current_occupancy=0;
    }

    $occupancy_violation=0;
    $data=[];
    // $zone  = Zone::select('max_occupancy','zones.id')
    //                     ->join('cameras','zone_id','=','zones.id')
    //                     ->where(['device_id'=>$device])
    //                     ->whereNull('zones.deleted_at')
    //                     ->whereNull('cameras.deleted_at')
    //                     ->first();

    
    
    //If zone has has set max occupancy then go to if block
    if($max_occupancy){

         //If current_occupancy greater than zone max occupancy
         if($current_occupancy>$max_occupancy){
             $violationCheck = CameraTransaction::where(['device'=>$device,'occupancy_violation'=>1])
                               ->whereNull('violation_end')    
                               ->orderBy('id','desc')
                               ->take(1)
                               ->get()
                               ->count(); 
                      
             if($violationCheck){
                $occupancy_violation=2;
             }                   
             else{
                $occupancy_violation=1;
                $data['violation_start']=date('Y-m-d H:i:s');
             }
         }
         else{
             //If current_occupancy less than zone max occupancy
            $violationCheck = CameraTransaction::where(['device'=>$device,'occupancy_violation'=>1])
                                ->whereNull('violation_end')    
                                ->orderBy('id','desc')
                                ->first();

            if($violationCheck){
                $violationCheck->id;
                $currentTime=date('Y-m-d H:i:s');
                $violationCheck->violation_end=$currentTime;
               
                $from_time=strtotime($violationCheck->violation_start); 
                $to_time=strtotime($currentTime);
                $duration = round(abs($to_time - $from_time) / 60,2);
                $violationCheck->violation_duration=$duration;
                $violationCheck->save();    
               
            }                   
             $occupancy_violation=0;
         }  
    }
    
    //Find last total entry
    //Total entry last entry - current in 
    $lastEntry =CameraTransaction::findLastRecord($device,'in');
    $entry = $in-$lastEntry; 
    if($entry<0){
        $entry=0;
    }
    
    $data['enter']=$entry;
    $data['in']=$in;
    $data['out']=$out;
    $data['current_occupancy']=$current_occupancy;
    $data['device']=$device;
    $data['device_mac']=$device_mac;
    $data['occupancy_violation']=$occupancy_violation;
    $data['zone_id']=$zone_id;
    $data['camera_id']=$camera_id;
    $data['base_in']=$base_in;
    $data['base_out']=$base_out;

    //rebot type innert
    
    //Find last record by device id
    $cameraTransaction = CameraTransaction::select('created_at','id','current_occupancy')
                            ->where(['device'=>$device])
                                ->orderBy('id','desc')
                                ->first();
    if($cameraTransaction){
        $created_at = $cameraTransaction->created_at;
        $currentTime = date('Y-m-d H:i:s');
        $startTime = Carbon::parse($created_at);
        $finishTime = Carbon::parse($currentTime);
        $duration = $finishTime->diffInSeconds($startTime);
        $total_duration = $duration*$cameraTransaction->current_occupancy;
        if($total_duration<0){$total_duration=0;}
        //Update duration in last record
        $updateData=[];
        $cameraTransaction->duration=$duration;
        $cameraTransaction->total_duration=$total_duration;
        $cameraTransaction->save();
     }
    //End find last record by device id                            
    
    //Find base value
    $cT=CameraTransaction::where('device',$device)->where('base_value','>',0)->orderBy('id','desc')->first();
    if($cT){
        $baseValue =  $cT->base_value;
        $visit_count = $in-$baseValue;
        if($visit_count<0){
            $visit_count=0;
        }
        $data['visit_count']=$visit_count;
    }  
    else{
        $data['visit_count']=$in;
    }                          
    
    $cameratransaction=CameraTransaction::create($data);

    //Zone sanitisation alert
    //$this->zoneSanitisationAlert($device);     

    $response['status']  =true;
    $response['message'] = "Added";
    $response['data']    = ['id'=>$cameratransaction->id];
   
    $this->addSocket($zone_id,$camera_id,$device);
    return $this->sendResponse($response);
}



    function getDeviceRebootStatus(Request $request){
    //     $validator = Validator::make($request->all(), [
    //     'device_id' => 'required',
    //   ]);
    // if ($validator->fails()) {
    //     $response['status']  =false;
    //     $response['message'] = trans('message.validation');;
    //     $response['data']    = $validator->errors();
    //     return $this->sendResponse($response);
    // }
    $device_id=$request->device_id;
    $device =Camera::select('id','reboot','counter_restart','height')->where('device_id',$device_id)->first();
    $response['status']  =true;
    $response['message'] = "Added";
    $cameraObj=new Camera();
    $response['data']    = ['device'=>$device,'deviceCount'=>$cameraObj->getDeviceLastRecord($device_id,true)];
    return $this->sendResponse($response);

    }

    function updateDeviceRebootStatus(Request $request){
    //     $validator = Validator::make($request->all(), [
    //     'id' => 'required',
    //   ]);
    // if ($validator->fails()) {
    //     $response['status']  =false;
    //     $response['message'] = trans('message.validation');;
    //     $response['data']    = $validator->errors();
    //     return $this->sendResponse($response);
    // }
    $device_id=$request->device_id;
    $last_reboot=date('Y-m-d H:i:s');
    $reboot_type=$request->reboot_type;
    $device =Camera::where('device_id',$device_id)->first();
    if(!$device){
        $response['status']  =false;
        $response['message'] = "Device id not found".$device_id;
        $response['data']    = [];
        return $this->sendResponse($response);     
    }

    $reboot=$device->reboot;
    $counter_restart=$device->counter_restart;

    if($reboot_type=='interrupt'){
        $reboot= 1;
        $counter_restart= 0;
    }
    else{
        $reboot= 0;
    }
    
    //In
    //reboot
    //counter resta=0

    $device =Camera::where('device_id',$device_id)
    ->update(['reboot'=>$reboot,'counter_restart'=>$counter_restart,'last_reboot'=>$last_reboot,'reboot_type'=>$reboot_type]);
    $response['status']  =true;
    $response['message'] = "Updated successfully";
    $response['data']    = [];
    return $this->sendResponse($response);

    }


function addSocket($zone_id,$camera_id,$device_id){
    $currentDate=date('Y-m-d');
    // $previousDate=date('Y-m-d',strtotime('-1 day',strtotime($currentDate)));
    // $lastWeekDate = date('Y-m-d',strtotime('-7 day',strtotime($currentDate)));
    // $lastMonthDate = date('Y-m-d',strtotime('-30 day',strtotime($currentDate)));
      

    //Socket code for home page
    $options = array(
        'cluster' => 'ap2',
        'useTLS' => true
    );
    
    $pusher =  new Pusher(
        Config::get('app.pusher_app_key'),
        Config::get('app.pusher_app_secret') ,
        Config::get('app.pusher_app_id') ,
        $options
    );
    $data=[];
    $where=[];
    $data['zone_id']=$zone_id;
    $data['new_zone_id']=$zone_id;
    $data['camera_id']=$camera_id;
    $camera=Camera::where('device_id',$device_id)->first();
    if($camera){
       $company_id=$camera->company_id;
       $branch_id=$camera->branch_id;
       $where['cameras.company_id']=$company_id;
       $where['cameras.branch_id']=$branch_id;
       $where['camera_transactions.zone_id']=$zone_id;
       #$where['camera_id']=$camera_id;
       $data['company_id']=$company_id;
       $data['branch_id']=$branch_id;
    }
    else{
        echo "Not found";
        exit;
    }   
   
    $interval=$this->defaultInterval;//Minute
    $intervalBreakData=CameraTransaction::getTimeSlot($branch_id,$currentDate,$interval);
    $intervalBreak=@$intervalBreakData['timeBreak'];
    
    //$homeData=Camera::getCurrentOccupancy($where);
    #$homeData=Camera::getCurrentOccupancy($zone_id,$currentDate);
    $occupancyCount=CameraTransaction::ZoneWiseCount($zone_id,$currentDate,'current_occupancy');
    $footFallCount=CameraTransaction::ZoneWiseCount($zone_id,$currentDate,'in');
   // $occupancyViolationCount=CameraTransaction::ZoneWiseCount($zone_id,$currentDate,'occupancy_violation');
    
   $occupancyViolationCount=CameraTransaction::zoneWiseViolationCount($zone_id,$intervalBreak,$interval,'occupancy_violation');


    $data['current_occupancy']=$occupancyCount;
    $data['totalFootFall']=$footFallCount;

    //Current Voilation
    //$occupancyViolation=Camera::getCurrentViolation($where);
  
    
    $data['occupancyViolation']=$occupancyViolationCount;
   
    $occupanyChartData=CameraTransaction::zoneByOccupancyCount($zone_id,$intervalBreak,$interval,'current_occupancy');
    $data['occupanyChartData']=$occupanyChartData;
    
  //  $footFallChartData=CameraTransaction::zoneByOccupancyCount($zone_id,$intervalBreak,$interval,'in');
  $footFallChartData=CameraTransaction::zoneWiseFootFall($zone_id,$intervalBreak,$interval,'in');

    $data['footFallChartData']=$footFallChartData;

    $violationChartData=CameraTransaction::zoneWiseViolation($zone_id,$intervalBreak,$interval,'occupancy_violation');

    $data['violationChartData']=$violationChartData;

   

    // //Yesterday Data
    // $homeData2=Camera::getCurrentOccupancy($zone_id,$previousDate);
    
    // $yesterdayColor="#ff0000";
    // $yesterdayCount=0;
    // $totalFootFallYesterday=$homeData2['totalFootFall'];
    // $totalFootFall=$homeData['totalFootFall'];
    // $totalFallInWeek=Camera::getTotalFallIn($where,$lastWeekDate);
    // $totalFallInMonth=Camera::getTotalFallIn($where,$lastMonthDate);

    // //Yesterday
    // if($totalFootFallYesterday>$totalFootFall){
    //     $yesterdayCount=$totalFootFallYesterday-$totalFootFall;
    // }
    // else {
    //     $yesterdayCount=$totalFootFall-$totalFootFallYesterday;
    //     $yesterdayColor="#228B22";
    // }
    // $data['yesterdayColor']=$yesterdayColor;
    // $data['yesterdayCount']=$yesterdayCount;

    // //Last Week
    // $weekColor="#ff0000";
    // $weekCount=0;
    // if($totalFallInWeek>$totalFootFall){
    //   $weekCount=$totalFallInWeek-$totalFootFall;
    // }
    // else {
    //   $weekCount=$totalFootFall-$totalFallInWeek;
    //   $weekColor="#228B22";
    // }
    // $data['weekColor']=$weekColor;
    // $data['weekCount']=$weekCount;
    
    // //Last Month
    // $monthColor="#ff0000";
    // $monthCount=0;
    // if($totalFallInMonth>$totalFootFall){
    //     $monthCount=$totalFallInMonth-$totalFootFall;
    // }
    // else {
    //     $monthCount=$totalFootFall-$totalFallInMonth;
    //     $monthColor="#228B22";
    // }
    // $data['monthColor']=$monthColor;
    // $data['monthCount']=$monthCount;





//    print_r($data); exit; 

    
    $pusher->trigger('my-channel', 'home', $data);



}

function zoneSanitisationAlert($device){
    $sanitisation_alert=false;
    //Find zone by device
    $camera=Camera::select('zone_id')->where('device_id',$device)->first();
    if($camera){
        $zone_id = $camera->zone_id;
        $branch_id="";
        $visit_limit=0;
        $zone=Zone::select('visit_limit','sanitisation','sanitisation_alert','branch_id')->where('id',$zone_id)->first();
        $zoneAlertNotification=false;
        if($zone){
            if($zone->sanitisation){
                $zoneAlertNotification=true;
                $visit_limit=$zone->visit_limit;
                $sanitisation_alert=$zone->sanitisation_alert;
                $branch_id=$zone->branch_id;
            }
        }
        if($zoneAlertNotification && !$sanitisation_alert){
            
            $cameras  = Camera::select('device_id')->where('zone_id',$zone_id)->get()->toArray();
            $device_lists=[];
            foreach($cameras as $camera){
                $device_lists[]=$camera['device_id'];
            }
            $total_in=0;
            $currentTime=date('Y-m-d');
            foreach($device_lists as $device){
                $cameraTransaction=CameraTransaction::
                whereDate('created_at',$currentTime)
                ->where('device',$device)
                ->take(1)
                ->orderBy('id','desc')
                ->first();
                if($cameraTransaction){
                    $total_in +=$cameraTransaction->visit_count;
                }
            }
            //Check visit limit
            if($total_in>$visit_limit){
                $currentTime = date('Y-m-d H:i:s');
                $branch=Branch::where('id',$branch_id)->first();  
                  
                if($branch){
                $otp="Zone alert notification";
                $mobile=$branch->mobile;
                if($mobile){
                $messageText="Your verification code is $otp . Just enter this code to verify your number -UNYDES";
                Common::sendSMS($mobile,$messageText,'1207160922705121024');

                //Zone notificaton
                $zoneAlertData=[];
                $zoneAlertData['zone_id'] = $zone_id;
                $zoneAlertData['max_allow_person']=$visit_limit;
                $zoneAlertData['total_in_person']= $total_in;
                $zone_notification_id=DB::table('zone_notification')->insertGetId($zoneAlertData);
                if($zone_notification_id){
                    $zoneUpdate=[];
                    $zoneUpdate['last_sanitisation']=null;
                    $zoneUpdate['sanitisation_alert']=1;
                    $zoneUpdate['notification_sent']=$currentTime;
                    $zoneUpdate['zone_notification_id']=$zone_notification_id;
                    Zone::where('id',$zone_id)->update($zoneUpdate);
                }
                }
            }
                

                


            }
        }
    }
}

/**
 * Reset
 */
function resetCamera(Request $request){
    $where=[];
    $where['status']=1;
    $device_id=$request->device_id;
    if($device_id){
        $where['device_id']=$device_id;
    }
    $cameras=Camera::where($where)->get(); 
    if($cameras){
        foreach($cameras as $camera){
            $data=[];
            $data['zone_id']=$camera->zone_id;
            $data['camera_id']=$camera->id;
            $data['device']=$camera->device_id;
            $data['device_mac']=$camera->device_mac;
            $data['in']=0;
            $data['out']=0;
            $data['base_in']=0;
            $data['base_out']=0;

            $data['current_occupancy']=0;
            $data['occupancy_violation']=0;
            //Reset device data in camera transaction table
            CameraTransaction::create($data);
            
            //Update camera
            $camera->reboot=1;
            $camera->counter_restart=1;
            $camera->save();

        }
    }
    $status=true;
    $message="Camera reset successfully.";
    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
    return $this->sendResponse($response);
}

function updateCamera(Request $request){
    $where=[];
    $device_id=$request->device_id;
    $lan=$request->lan;
    
    $status=false;
    $message="Please try again!";
    $data=[];
    
    if($device_id){
        $where['device_id']=$device_id;
        $camera=Camera::where($where)->first(); 
        if($camera){
            $camera->lan=$lan;
            $camera->save();
            $status=true;
            $message="Camera lan update successfully.";


        }
    }
    
    $response['status']  =$status;
    $response['message'] = $message;
    $response['data']    = [];
   
    return $this->sendResponse($response);
   
}


}