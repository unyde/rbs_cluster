<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;
use App\TagUser;
use App\Camera;
use Illuminate\Support\Facades\DB;
class TagUserController extends ApiController
{
    function login(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $response['status']  =false;
            $response['message'] = trans('message.validation');;
            $response['data']    = $validator->errors();
            return $this->sendResponse($response);
        }

        $username = $request->username;
        $password = $request->password;
        $tagUser = TagUser::select('company_name','branch_name','tag_users.id','tag_users.company_id','tag_users.branch_id')
                            ->join('companies','company_id','=','companies.id')
                            ->join('branches','branch_id','=','branches.id')
                            ->where(['username'=>$username,'password'=>$password])
                            ->first();
        $status=false; 
        $message="Please try again.";
        $data=[];
        if($tagUser){
            $status=true;
            $message="Tag user found.";
            $data['tag_user_id'] = $tagUser->id;
            $data['company_id'] = $tagUser->company_id;
            $data['branch_id']  = $tagUser->branch_id;
            $data['company_name'] = $tagUser->company_name;
            $data['branch_name']  = $tagUser->branch_name;
            $branch_id=$tagUser->branch_id;
        }
        $response = array();
        $response['status']     = $status;
        $response['message']    = $message;
        $response['data']       = $data;
        
        return $this->sendResponse($response);
}

function camera(Request $request){
    $validator = Validator::make($request->all(), [
        'branch_id' => 'required',
    ]);
    if ($validator->fails()) {
        $response['status']  =false;
        $response['message'] = trans('message.validation');;
        $response['data']    = $validator->errors();
        return $this->sendResponse($response);
    }

    $branch_id = $request->branch_id;
    $cameras = Camera::where(['branch_id'=>$branch_id,'status'=>1])
                        ->get();
    $status=false; 
    $message="Please try again.";
    $data=[];
    if($cameras){
        $status=true;
        $message="Camera found.";
        $data['cameras']  = $cameras;
       
    }
    $response = array();
    $response['status']     = $status;
    $response['message']    = $message;
    $response['data']       = $data;
    
    return $this->sendResponse($response);
}

}