<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\User;
use DB;
use App\Camera;
use App\Branch;
use App\TagUser;
use App\Floor;
use App\Zone;
use App\CameraTransaction;
class HomeController2 extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  
    public function index(Request $request)
    {   
        $currentDate=date('Y-m-d');
        if($request->selected_date){
            $currentDate=date('Y-m-d',strtotime($request->selected_date));
        }
        $where=[];
        $userInfo = User::getUserInfo();
        $company_id = $userInfo['company_id'];
        $branch_id = $userInfo['branch_id'];
        $zoneOccupancyWhere=[];
        if($company_id){
            $zoneOccupancyWhere['zones.company_id']=$company_id;
        }
        if($branch_id){
            $zoneOccupancyWhere['zones.branch_id']=$branch_id;
        }
        $companyName="Super Admin";
        $currentOccupancyTotal=0;
        $totalFootFall=0;
        $totalFootFallYesterday=0;
        $start_time = "";
        $end_time   = "";
        $time       = 0;
        $interval=0;
        $timeSlots=[];
        $todayAllTimes=[];
        $yesterdayAllTimes=[];
        $zones=[];
        $zone_id=$request->zone_id;
        
        $zone=['max_occupancy'=>0];

        $sanitisationZones=[];
              
        if($branch_id){
            //Find sanitisation zone
            $sanitisationZones=Zone::where(['company_id'=>$company_id,
            'branch_id'=>$branch_id,'sanitisation'=>1])
            ->get()->toArray();
            

            $branchInfo = Branch::where('id',$branch_id)->first();
            $zones=Zone::where(['company_id'=>$company_id,'branch_id'=>$branch_id])
            ->pluck('zone_name','id');

            if($zone_id){
                $zonerow=Zone::where(['company_id'=>$company_id,
                'branch_id'=>$branch_id,
                'id'=>$zone_id])
                ->first();
                $zone['max_occupancy']=$zonerow->max_occupancy;

            }
            else{
                $zonerow=Zone::where(['company_id'=>$company_id,'branch_id'=>$branch_id])
                ->first();
                $zone_id=$zonerow->id;
                $zone['max_occupancy']=$zonerow->max_occupancy;
                
            }
            $start_time = $branchInfo->start_time;
            $end_time   = $branchInfo->end_time;
            if($start_time){
                $start_time=date('H.i',strtotime($start_time));
                $time=strtotime($start_time);
            }
            if($end_time){
                $end_time=date('H.i',strtotime($end_time));
            }
            if($start_time && $end_time){
                //Start Time
                $slot=['label'=>$start_time];
                $timeSlots[]=$slot;
                

                $interval = round(abs(strtotime($end_time) - strtotime($start_time)) / 60,2);
                $interval = ceil($interval/60);
               
               // echo $interval; exit; 
                for($i=1;$i<$interval;$i++){
                    $addTime=$i*60;// Add in minute
                    $newTime=date("H:i", strtotime("+$addTime minutes", $time));
                    $slot=[];
                    $slot['label']=$newTime;
                    $timeSlots[]=$slot;
                }

                //End Time
                $slot=['label'=>$end_time];
                $timeSlots[]=$slot;
            }
        }

        if(is_array($timeSlots) && count($timeSlots)){
            foreach($timeSlots as $key=>$slot){
                $from_time ="";
                $to_time="";
                $to_time=$slot['label'];
                $timeSlots[$key]['label_text']=str_replace(':00','',date('g:i a',strtotime($to_time)));
                if($key){
                    $previousTime=$key-1;
                    $from_time = $timeSlots[$previousTime]['label'];
                }
                
                //Today
                $todayFromTime ="";
                $todayToTime="";
                $yesterdayFromTime ="";
                $yesterdayToTime="";
                $lastMonthFromTime ="";
                $lastMonthToTime="";
                $todayDate=date('Y-m-d');
                $yesterdayDate=$previousDate;//date('Y-m-d',strtotime('-1 day',strtotime('now')));
                $lastMonthDate=date('Y-m-d',strtotime('-1 months',strtotime($currentDate)));
                if($from_time){
                    $todayFromTime=$todayDate." ".$from_time;
                    $yesterdayFromTime=$yesterdayDate." ".$from_time;
                    $lastMonthFromTime=$lastMonthDate." ".$from_time;
                }
                if($to_time){
                    $todayToTime=$todayDate." ".$to_time;
                    $yesterdayToTime=$yesterdayDate." ".$to_time;
                    $lastMonthToTime=$lastMonthDate." ".$to_time;
                }
                $todayAllTimes[]=['from_time'=>$todayFromTime,'to_time'=>$todayToTime];
                $yesterdayAllTimes[]=['from_time'=>$yesterdayFromTime,'to_time'=>$yesterdayToTime];
                $timeSlots[$key]['today'] = ['from_time'=>$todayFromTime,'to_time'=>$todayToTime];
                $timeSlots[$key]['yesterday'] = ['from_time'=>$yesterdayFromTime,'to_time'=>$yesterdayToTime];
                $timeSlots[$key]['last_month'] = ['from_time'=>$lastMonthFromTime,'to_time'=>$lastMonthToTime];
            }
        }
      
        $companyInfo = Company::select('*')->where('id',$company_id)->first();
        if($company_id){
            $company = Company::select('company_name')->where('id',$company_id)->first();
            $companyName = $company->company_name;
            $where['cameras.company_id'] = $company_id;
        }

        if($branch_id){
            $where['cameras.branch_id'] = $branch_id;
        }

        /**
         * Find current occupancy
         */
        
        // $currentOccupancies = Camera::select('current_occupancy','in')
        // ->join('camera_transactions','device_id','=','device')
        //                           ->where($where)
        //                           ->where('camera_transactions.zone_id',$zone_id)
        //                           ->whereDate('camera_transactions.created_at','=',$currentDate)
        //                           ->whereIn('camera_transactions.id', function($q) use ($currentDate){ 
        //                             $q->select(DB::raw("MAX(camera_transactions.id) FROM camera_transactions where date(camera_transactions.created_at)='$currentDate' GROUP BY device"));
        //                            })
        //                           ->groupBy('device')
        //                           ->orderBy('camera_transactions.id','desc')
        //                           ->get();
                                  
                                  
       
        // foreach($currentOccupancies as $row){
        //   $currentOccupancyTotal+=$row->current_occupancy; 
        //   $totalFootFall+=$row->in; 
        // }
        // if($currentOccupancyTotal<0){
        //     $currentOccupancyTotal=0;
        // }
        $zoneData=CameraTransaction::ZoneCount($zone_id,$currentDate);
        $currentOccupancyTotal=$zoneData['occupancy'];
        $totalFootFall=$zoneData['footFall'];
        /**
         * FootFall
         */
        $totalFootFallYesterday=Camera::getTotalFallIn($where,$previousDate);
        $totalFallInWeek=Camera::getTotalFallIn($where,$lastWeekDate);
        $totalFallInMonth=Camera::getTotalFallIn($where,$lastMonthDate);
       
        /**
         * Occupancy Violation
         */
        $occupancyViolation=Camera::select('camera_transactions.id')->join('camera_transactions','device_id','=','device')
                                    ->where($where)
                                    ->where('occupancy_violation','=',1)
                                    ->whereDate('camera_transactions.created_at','=',$currentDate)
                                    ->get()
                                    ->count();
                        
                    
        /**
         * FOOTFALL TREAND
         * */ 
        $fallTrends=[];
        $todayFallTrends=[];
        $yesterdayFallTrends=[];
        $lastMonthFallTrends=[];
     
        if(is_array($timeSlots) && count($timeSlots)){
            foreach($timeSlots as $timeSlot){
               $from_time=$timeSlot['today']['from_time'];
               $to_time=$timeSlot['today']['to_time'];
               if($from_time && $to_time){
                // $yesterdayDate=date('Y-m-d',strtotime('-1 day',strtotime('now')));
                // $lastMonthDate=date('Y-m-d',strtotime('-1 months',strtotime('now')));
              
                    for($i=1;$i<=3;$i++){
                    if($i==1){}
                    else if($i==2){
                        $from_time=date('Y-m-d H:i:s',strtotime('-1 day',strtotime($from_time)));
                        $to_time=date('Y-m-d H:i:s',strtotime('-1 day',strtotime($to_time)));
                    }
                    else{
                        $from_time=date('Y-m-d H:i:s',strtotime('-1 months',strtotime($from_time)));
                        $to_time=date('Y-m-d H:i:s',strtotime('-1 months',strtotime($to_time)));
                    }  

                    $cameraTodayFallTrends=Camera::select('in')->join('camera_transactions','device_id','=','device')
                            ->where($where)
                            ->where('camera_transactions.created_at','>=',$from_time)
                            ->where('camera_transactions.created_at','<=',$to_time)
                            ->whereIn('camera_transactions.id', function($q) use ($from_time,$to_time){
                                 $q->select(DB::raw("MAX(camera_transactions.id) FROM camera_transactions where camera_transactions.created_at>='$from_time' && camera_transactions.created_at<='$to_time' GROUP BY device"));
                            })
                            ->groupBy('device')
                            ->orderBy('camera_transactions.id','desc')
                            ->get();

                    $totalIn=0;
                    foreach($cameraTodayFallTrends as $cameraTodayFallTrend){
                        $totalIn+=$cameraTodayFallTrend->in;
                    }

                    if($i==1){
                        array_push($todayFallTrends,$totalIn); 
                    }
                    else if($i==2){
                        array_push($yesterdayFallTrends,$totalIn); 
                    }
                    else if($i==3){
                        array_push($lastMonthFallTrends,$totalIn); 
                    }
                   
                }

               }
               else{
                array_push($todayFallTrends,0);
                array_push($yesterdayFallTrends,0);
                array_push($lastMonthFallTrends,0);
               }    
            }
        }  
        $fallTrends['today']=['today'=>$todayFallTrends,'yesterday'=>$yesterdayFallTrends,'lastMonth'=>$lastMonthFallTrends];
        
        // echo "<pre/>";
        // print_r($fallTrends); exit;
        /**
         * Fall Trends in week level
         */
        $weekData=[];
        $weekDays=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        $currenWeekNo = $this->weekOfMonth(strtotime($currentDate)) . " "; // 2
       
        $firstday = date('w l - d/m/Y', strtotime("this week"));
        $currentWeekNumber = date('w',strtotime($currentDate));
        $subtractDay=6;
        if($currentWeekNumber){
            $subtractDay=$currentWeekNumber-1;
        }
        $currentWeekStart=date('Y-m-d',strtotime("-$subtractDay day",strtotime($currentDate)));
        # echo $this->getWeeks('2021-04-12','Monday');
        # exit;
        // echo $lastday = date('l - d/m/Y', strtotime("this week +6 days"));
        //$currentWeekStart = date('Y-m-d', strtotime("this week"));
      
        $currentWeekEnd   = date('Y-m-d', strtotime("+6 day",strtotime($currentWeekStart)));
        $allDateOfCurrentWeek=Camera::getDateBetweenTwoDate($currentWeekStart,$currentWeekEnd);
        //$totalFallInWeek=Camera::footFall($where,$allDateOfCurrentWeek);
       
        // dd($allDateOfCurrentWeek);
        //  exit;
        $weekDay=[];
  

        //Set current week data 0
        for($i=0;$i<=6;$i++){$weekDay[$i]=1;}
        $weekData['weekDay']=$weekDay;
        $weekData['currentWeek']=Camera::getWeekData($where,$currentWeekStart,$currentWeekEnd);
        
        //Last Week
        #$lastWeekStart = date('Y-m-d', strtotime('last week'));
        $lastWeekStart  = date('Y-m-d',strtotime("-7 day",strtotime($currentWeekStart)));
        //$lastWeekEnd   = date('Y-m-d', strtotime("last week +6 days"));
        $lastWeekEnd   = date('Y-m-d', strtotime("+6 day",strtotime($lastWeekStart)));
        $weekData['lastWeek']=Camera::getWeekData($where,$lastWeekStart,$lastWeekEnd);

        //Last Month Week
        $lastMonthFirst =  date('Y-m-d', strtotime('first day of last month'));
        $lastMonthLast =  date('Y-m-d', strtotime('last day of last month'));
        $month=date('n',strtotime($lastMonthFirst));
        $year=date('Y',strtotime($lastMonthFirst));
        $totalWeek = $this->weeks_in_month($month,$year);
      
       
        /**
         * FallTrend Month
         */
         $totalDay=date('t');
         $fallTrendMonth=['currentMonth'=>[],'lastMonth'=>[]];
         //Current Month
         $month = date('m');
         $year  = date('Y');
         $currentMonthFallTrend=Camera::getFallTrendMonthData($where,$month,$year,$totalDay);
         $fallTrendMonth['currentMonth']=$currentMonthFallTrend;
         
         //Previous Month
         $newdate = date("Y-m-d", strtotime ( '-1 month' , strtotime ( $currentDate ) )) ;
         $lastMonth=date("m", strtotime($newdate));
         $lastYear=date("Y", strtotime($newdate));
         $totalDay= date("t", strtotime($newdate));
         $lastMonthData=Camera::getFallTrendMonthData($where,$lastMonth,$lastYear,$totalDay);
         $fallTrendMonth['lastMonth']=$lastMonthData;
        
         /**
          * AVG. TIME SPEND
          */
          $timeSpend=['today'=>[],'yesterday'=>[],'currentWeek'=>[],'lastWeek'=>[],'currentMonth'=>0,'lastMonth'=>0];
          
          //Today
          $times=[];
          foreach($timeSlots as $item){
              $conditionTime =$item['today'];
              $times[]=array('from_time'=>$conditionTime['from_time'],'to_time'=>$conditionTime['to_time']);
          }
          $today=Camera::getSpendTime($where,$times,'time');
          $timeSpend['today']=$today;
          
          //Yesterday
          $times=[];
          foreach($timeSlots as $item){
              $conditionTime =$item['yesterday'];
              $times[]=array('from_time'=>$conditionTime['from_time'],'to_time'=>$conditionTime['to_time']);
          }
          $yesterday=Camera::getSpendTime($where,$times,'time');
          $timeSpend['yesterday']=$yesterday;
          
          //Current Week
          $allDates=Camera::getDateBetweenTwoDate($currentWeekStart,$currentWeekEnd);
          $allDates=Camera::getDateArray($allDates); 
          $currentWeekAllDates=$allDates;
          $currentWeek=Camera::getSpendTime($where,$allDates,'day');
          $timeSpend['currentWeek']=$currentWeek;
          
          //Last Week Spend Time
          $allDates=Camera::getDateBetweenTwoDate($lastWeekStart,$lastWeekEnd);
          $allDates=Camera::getDateArray($allDates); 
          $lastWeekAllDates=$allDates;
          $lastWeek=Camera::getSpendTime($where,$allDates,'day');
          $timeSpend['lastWeek']=$lastWeek;

          //Current Month
          $month = date('m',strtotime($currentDate));
          $year  = date('Y',strtotime($currentDate));
          $totalDay=date('t',strtotime($currentDate));
          
          $start_date=$year."-".$month."-01";
          $end_date=$year."-".$month."-".$totalDay;
         
          $allDates=Camera::getDateBetweenTwoDate($start_date,$end_date);
          $currentMonthAllDates=$allDates;
          //$totalFallInMonth=Camera::footFall($where,$allDates);
         
          $allDates=Camera::getDateArray($allDates);
          $timeSpend['currentMonth']=Camera::getSpendTime($where,$allDates,'day');
          
          //Last Month
          $newdate = date("Y-m-d", strtotime ( '-1 month' , strtotime ( $currentDate ) )) ;
          $lastMonth=date("m", strtotime($newdate));
          $lastYear=date("Y", strtotime($newdate));
          $totalDay= date("t", strtotime($newdate));
          $start_date=$year."-".$lastMonth."-01";
          $end_date=$year."-".$lastMonth."-".$totalDay;
          
          $allDates=Camera::getDateBetweenTwoDate($start_date,$end_date);
          $lastMonthAllDates=$allDates;

          $allDates=Camera::getDateArray($allDates);
          $timeSpend['lastMonth']=Camera::getSpendTime($where,$allDates,'day');
          
          /* Occupancy Violation */
          $occupancyGraphData=['today'=>[],'yesterday'=>[],'currentWeek'=>[],'lastWeek'=>[],'currentMonth'=>[],'lastMonth'=>[]];
          //Today
          $occupancyGraphData['today'] =Camera::getOccupancyViolation($where,$todayAllTimes,'time');
          $occupancyGraphData['yesterday'] =Camera::getOccupancyViolation($where,$yesterdayAllTimes,'time');
          //Week
          $occupancyGraphData['currentWeek'] =Camera::getOccupancyViolation($where,$currentWeekAllDates,'day');
          $occupancyGraphData['lastWeek'] =Camera::getOccupancyViolation($where,$lastWeekAllDates,'day');
          
          //Month
          $occupancyGraphData['currentMonth'] =Camera::getOccupancyViolation($where,$currentMonthAllDates,'day');
          $occupancyGraphData['lastMonth'] =Camera::getOccupancyViolation($where,$lastMonthAllDates,'day');
          
         

          /**
         * Device Status
         */
           $activeCameras=Camera::select('cameras.*','branch_name','zone_name')
                ->join('branches','branch_id','=','branches.id')  
                ->join('zones','zone_id','=','zones.id')       
                ->where($where)
                ->get();
         //Repurpose zone
         $zoneOccupancies = Zone::Occupancy($zoneOccupancyWhere,'','');
            
                                                        
        
        return view('home',compact('zoneOccupancies','sanitisationZones','zones','zone','zone_id','company_id','branch_id','occupancyGraphData','currentDate','totalFallInWeek','totalFallInMonth','timeSpend','fallTrendMonth','currentDate','fallTrends','weekData','companyName','currentOccupancyTotal','totalFootFall','totalFootFallYesterday','occupancyViolation','timeSlots','activeCameras'));
    }
    // function weekOfMonth($date) {
    //     //Get the first day of the month.
    //     $firstOfMonth = strtotime(date("Y-m-01", $date));
    //     //Apply above formula.
    //     return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
    // }
    
    // function weekOfYear($date) {
    //     $weekOfYear = intval(date("W", $date));
    //     if (date('n', $date) == "1" && $weekOfYear > 51) {
    //         // It's the last week of the previos year.
    //         $weekOfYear = 0;    
    //     }
    //     return $weekOfYear;
    // }
    

    function getWeeks($date, $rollover)
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover))  $weeks ++;
        }

        return $weeks;
    }


    function weeks_in_month($month, $year) {
        // Start of month
        $start = mktime(0, 0, 0, $month, 1, $year);
        
        // End of month
        $end = mktime(0, 0, 0, $month, date('t', $start), $year);
        // Start week
        $start_week = date('W', $start);
        // End week
        $end_week = date('W', $end);
       
        if ($end_week < $start_week) { // Month wraps
          return ((52 + $end_week) - $start_week) + 1;
        }
       
        return ($end_week - $start_week) + 1;
       }


    function cameraCount(){
         $camera=DB::table('options')->where('id',1)->first();
         $cameraData=$camera->option_value;
         $cameraData=json_decode($cameraData,true);
         $current_occupancy=$cameraData['in']-$cameraData['out'];
         if($current_occupancy<1){
             $current_occupancy=0;
         }
         $data['in']=$cameraData['in'];
         $data['out']=$cameraData['out'];
         $data['current_occupancy']=$current_occupancy;
         return json_encode($data);
    }
    function weekOfMonth($date) {
        //Get the first day of the month.
        $firstOfMonth = strtotime(date("Y-m-01", $date));
        //Apply above formula.
        return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
    }
    
    function weekOfYear($date) {
        $weekOfYear = intval(date("W", $date));
        if (date('n', $date) == "1" && $weekOfYear > 51) {
            // It's the last week of the previos year.
            $weekOfYear = 0;    
        }
        return $weekOfYear;
    }

    function postZoneSanitisation(Request $request){
            $zone_id=$request->zone_id;
            $zone_notification_id=$request->zone_notification_id;
            $remark=$request->remark;
            $status=$request->status;

            //Find device
            $cameras = Camera::select('device_id')->where('zone_id',$zone_id)->get();
            if($cameras){
                foreach($cameras as $camera){
                    $device_id = $camera->device_id;
                    $cameraTransaction = CameraTransaction::select('id','in')->where('device',$device_id)->orderBy('id','desc')->first();
                    if($cameraTransaction){
                        $cameraTransaction->base_value =$cameraTransaction->in;
                        $cameraTransaction->save();
                    }
                }
            }

            $zoneUpdate=[];
            $currentTime=date('Y-m-d H:i:s');
            $zoneUpdate['last_sanitisation'] =  $currentTime;
            $zoneUpdate['zone_notification_id']="";
            $zoneUpdate['sanitisation_alert']=0;
            //$zoneUpdate['notification_sent']="";
            Zone::where('id',$zone_id)->update($zoneUpdate);

            //Zone notification update
            $zoneNotificationUpdate=[];
            $zoneNotificationUpdate['remark']=$remark;
            $zoneNotificationUpdate['status']=$status;
            $zoneNotificationUpdate['resolve_time']=$currentTime;
            if($zone_notification_id){
                DB::table('zone_notification')->where('id',$zone_notification_id)->update($zoneNotificationUpdate);
            }    
            return redirect()->back();
    }
}
