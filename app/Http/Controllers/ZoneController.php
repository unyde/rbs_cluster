<?php

namespace App\Http\Controllers;

use App\Zone;
use Illuminate\Http\Request;
use App\Company;
use App\Branch;
use App\User;
use App\Floor;

class ZoneController extends Controller
{
    private $companyList;
   
    public function __construct()
    {
   
        
        $this->companyList=Company::companyList();
        $this->middleware('auth');
        $this->middleware('permission:zone-list',['only' => ['index']]);
        $this->middleware('permission:zone-create', ['only' => ['create','store']]);
        $this->middleware('permission:zone-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:zone-delete', ['only' => ['destroy']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   $company_id = $request->company_id;
        $branch_id  = $request->branch_id;
        $q  = $request->q;
        $branches=[];
        if($company_id){
            $branches=Branch::branchByCompanyId($company_id);
        }

        $companyList= $this->companyList;
        $userInfo= User::getUserInfo();
        if($userInfo['branch_id']){
            $branch_id=$userInfo['branch_id'];
        }
        $data = Zone::select('zones.*','company_name','branch_name','floor_name')
        ->join('companies','company_id','=','companies.id')
        ->join('branches','branch_id','=','branches.id')
        ->leftJoin('floors','floor_id','=','floors.id')
        ->where(function($query) use ($userInfo,$company_id,$branch_id,$q){
            if($company_id){
                //
            }
            else{
                $company_id=$userInfo['company_id'];
            }
            if($branch_id){
                //
            }
            else{
                $branch_id=$userInfo['branch_id'];
            }
           
            if($company_id){
                $query->where('zones.company_id',$company_id);
            }
            if($branch_id){
                $query->where('zones.branch_id',$branch_id);
            }
            if($q){
                $query->where('zone_name','Like',"%$q%"); 
            }
            
        })
        ->orderBy('zones.id','desc')->get();
        $i=0;
        return view('zone.index',compact('data','i','companyList','branches'));
    }

    public function currentOccupany()
    {
        $userInfo= User::getUserInfo();
        $data = Zone::select('zones.*','company_name','branch_name','floor_name')
        ->join('companies','company_id','=','companies.id')
        ->join('branches','branch_id','=','branches.id')
        ->join('floors','floor_id','=','floors.id')
        ->where(function($query) use ($userInfo){
            $company_id=$userInfo['company_id'];
            $branch_id=$userInfo['branch_id'];
            if($company_id){
                $query->where('zones.company_id',$company_id);
            }
            if($branch_id){
                $query->where('zones.branch_id',$branch_id);
            }
            
        })
        ->orderBy('zones.id','desc')->get();
        $i=0;
        print_r($data); exit; 
        return view('zone.current-occupany',compact('data','i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userInfo= User::getUserInfo();
        $branches = Branch::branchByCompanyId($userInfo['company_id']);
        $floors=Floor::getFloors();
        return view('zone.create',compact('userInfo','branches','floors'))->with(['companyList'=>$this->companyList]);
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'branch_id'  => 'required',
            'zone_name'  => 'required',
        ]);

        $input = $request->all();
        #echo "<pre/>";
        #print_r($input); exit; 
        $zone = Zone::create($input);
        if($zone){
            return redirect()->route('zone.index')

            ->with('success','Zone created successfully');
        }
        else{

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(Zone $zone)
    {
        $floors   = Floor::getFloors();
        $userInfo = User::getUserInfo();
        $branches = Branch::branchByCompanyId($zone->company_id);
      
        return view('zone.edit',compact('zone','userInfo','floors'))
               ->with(['companyList'=>$this->companyList,'branches'=>$branches]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'branch_id'  => 'required',
            'zone_name'  => 'required',
        ]);

        $zone->company_id     = $request->company_id;
        $zone->branch_id      = $request->branch_id;
        $zone->building_name  = $request->building_name;
        $zone->floor_id       = $request->floor_id;
        $zone->zone_name      = $request->zone_name;
        $zone->sub_title      = $request->sub_title;
        $zone->max_occupancy  = $request->max_occupancy;

        $zone->sanitisation = $request->sanitisation;
        $zone->visit_limit  = $request->visit_limit;

        if($zone->save()){
            return redirect()->route('zone.index')

            ->with('success','Zone updated successfully');
        }
        else{
            return redirect()->route('zone.index')

            ->with('warning','Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone)
    {
        $zone->delete();
        return redirect()->route('zone.index')
    
                            ->with('success','Zone deleted successfully');
    }

     /**
     * Zone list by branch id 
     */
    public function zoneList(Request $request,$branchId)
    {
      $data = Zone::zoneListByBranch($branchId);
      return response()->json(['data'=>$data]);
    }

    function zoneNotification(Request $request){

        $userInfo= User::getUserInfo();
        $data = Zone::join('zone_notification','zone_id','=','zones.id')
        ->join('companies','company_id','=','companies.id')
        ->join('branches','branch_id','=','branches.id')
        ->join('floors','floor_id','=','floors.id')
        ->where(function($query) use ($userInfo){
            $company_id=$userInfo['company_id'];
            $branch_id=$userInfo['branch_id'];
            if($company_id){
                $query->where('zones.company_id',$company_id);
            }
            if($branch_id){
                $query->where('zones.branch_id',$branch_id);
            }
            
        })
        ->orderBy('zone_notification.id','desc')
        ->paginate(config('app.per_page'));
        $i= ($request->input('page', 1) - 1) * config('app.per_page');
        return view('zone.zone_notification',compact('data','i'));

    }

}
