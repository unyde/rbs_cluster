<?php

namespace App\Http\Controllers;

use App\Camera;
use Illuminate\Http\Request;
use App\Company;
use App\Branch;
use App\User;
use App\Floor;
use App\Zone;
class CameraController extends Controller
{

    private $companyList;
    public function __construct()
    {
        $this->companyList=Company::companyList();
        $this->middleware('auth');
        // $this->middleware('permission:camera-list',['only' => ['index']]);
        // $this->middleware('permission:camera-create', ['only' => ['create','store']]);
        // $this->middleware('permission:camera-edit', ['only' => ['edit','update']]);
        // $this->middleware('permission:camera-delete', ['only' => ['destroy']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   $companyList=$this->companyList;
        $company_id=$request->company_id;
        $branch_id=$request->branch_id;
        $zone_id=$request->zone_id;
        $q=$request->q;
        $branches=[];
        $zones=[];
        if($company_id){
            $branches=Branch::branchByCompanyId($company_id);
        }
        if($branch_id){
           $zones= Zone::zoneListByBranch($branch_id);
           #print_r($zones); exit;
        }

        $userInfo= User::getUserInfo();
        $data = Camera::select('cameras.*','company_name','branch_name','zone_name')
                    ->join('companies','company_id','=','companies.id')
                    ->leftJoin('branches','branch_id','=','branches.id')
                    ->leftJoin('zones','zone_id','=','zones.id')
                    ->where(function($query) use ($userInfo,$company_id,$branch_id,$zone_id,$q){
                        if(!$company_id){
                            $company_id=$userInfo['company_id'];
                        }
                        if(!$branch_id){
                            $branch_id=$userInfo['branch_id'];
                        }
                        if($zone_id){
                            $query->where('zone_id',$zone_id);
                        }
                        if($q){
                            $query->where('device_name','LIKE',"%$q%");
                        }
                        
                        if($company_id){
                            $query->where('cameras.company_id',$company_id);
                        }
                        
                        if($branch_id){
                            $query->where('cameras.branch_id',$branch_id);
                        }
                        
                    })
                    ->orderBy('cameras.id','desc')
                    ->get();
       
        $i=0;
        return view('camera.index',compact('data','i','companyList','branches','zones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userInfo = User::getUserInfo();
        $branches = Branch::branchByCompanyId($userInfo['company_id']);
        $branch_id =$userInfo['branch_id'];
        $floors=Floor::getFloors();
        return view('camera.create',compact('userInfo','branches','floors'))->with(['companyList'=>$this->companyList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'branch_id'  => 'required',
            'zone_id'=>'required',
            'device_id'=>'required|unique:cameras'
        ]);

        $input = $request->all();
        
        $camera = Camera::create($input);
        if($camera){
            return redirect()->route('camera.index')

            ->with('success','Camera created successfully');
        }
        else{

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function show(camera $camera)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function edit(camera $camera)
    {
        $userInfo = User::getUserInfo();
        $branches = Branch::branchByCompanyId($camera->company_id);

        $zones = Zone::zoneListByBranch($camera->branch_id);
        return view('camera.edit',compact('camera','userInfo','zones'))
               ->with(['companyList'=>$this->companyList,'branches'=>$branches]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, camera $camera)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'branch_id'  => 'required',
            'zone_id'=>'required'
        ]);

        $camera->company_id     = $request->company_id;
        $camera->branch_id   = $request->branch_id;
        $camera->zone_id   = $request->zone_id;
        $camera->device_name = $request->device_name;
        $camera->device_id = $request->device_id;
        $camera->device_mac = $request->device_mac;
        $camera->reboot = $request->reboot;
        $camera->counter_restart = $request->counter_restart;
        $camera->height = $request->height;
        $camera->reboot_type = $request->reboot_type;
        //rebot type - in yes
        //counter - 
        

        if($camera->save()){
            return redirect()->route('camera.index')

            ->with('success','Camera updated successfully');
        }
        else{
            return redirect()->route('camera.index')

            ->with('warning','Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function destroy(camera $camera)
    {
        $camera->delete();
        return redirect()->route('camera.index')
    
                            ->with('success','Camera deleted successfully');
    }
    /**
     * Device list show for branch
     */
    public function device(Request $request)
    {   $userInfo= User::getUserInfo();
        $data = Camera::select('cameras.*','company_name','branch_name','zone_name')
                    ->join('companies','company_id','=','companies.id')
                    ->leftJoin('branches','branch_id','=','branches.id')
                    ->leftJoin('zones','zone_id','=','zones.id')
                    ->where(function($query) use ($userInfo){
                        $branch_id=$userInfo['branch_id'];
                        if($branch_id){
                            $query->where('cameras.branch_id',$branch_id);
                        }
                        
                    })
                    ->orderBy('cameras.id','desc')
                    ->get();
       
        $i=0;
        return view('camera.device',compact('data','i','companyList','branches','zones'));
    }
}
