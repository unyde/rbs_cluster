<?php

namespace App\Http\Controllers;

use App\CameraTransaction;

use App\User;

use Illuminate\Http\Request;
use App\Camera;
use App\Company;
use DB;
use App\Zone;
use App\Branch;
use Config;
class CameraTransactionController extends Controller
{   private $defaultInterval;
    function __construct(){
        $this->defaultInterval=Config::get('app.chart_interval');
    }

    /**
     * Current Occupancy - view more 
     */
    function currentOccupancy(Request $request){
        $data=[];
        $zone_id=$request->zone_id;
        $selectedDate=$request->filter_date;
        $data=Zone::zoneListByUser('zone','occupancy.csv',$selectedDate,$zone_id);
        return view('camera-transaction.current-occupancy',compact('data'));
    }
    
    /**
    * Foot Fall - view more 
    */
    function footFall(Request $request){
        $data=[];
        $zone_id=$request->zone_id;
        $selectedDate=$request->filter_date;
        $data=Zone::zoneListByUser('zone','footfall.csv',$selectedDate,$zone_id);
        $zones=$data['zone_names'];
        if($selectedDate){
            $selectedDate=date('Y-m-d',strtotime($selectedDate));
        }
        $selectedZones=$zones->toArray();
       

        $zone_foot_fall=CameraTransaction::fetchFootFallDate($selectedZones,$selectedDate);
        $zone_foot_fall=$zone_foot_fall;
        $selectedZones=implode(',',array_flip($selectedZones));
       
 
        $url="?zone=$selectedZones";
        $url.="&selected_date=$selectedDate";

       
   
        return view('camera-transaction.foot-fall',compact('data','zone_foot_fall','url'));
    }

    /**
    * Occupancy Violation - view more 
    */
    function violation(Request $request){
        $data=[];
        $zone_id=$request->zone_id;
        $selectedDate=$request->filter_date;
        $data=Zone::zoneListByUser('zone','violation.csv',$selectedDate,$zone_id);
        return view('camera-transaction.occupancy-violation',compact('data'));
    }
     /**
    * Foot Fall by device - view more 
    */
    function footFallDevice(Request $request,$zone_id){
        $data=[];
        $selectedDate=$request->filter_date;
        $zone_name="";

        $zone=Zone::select('zone_name')->where('id',$zone_id)->first();
        if($zone){
            $zone_name=$zone->zone_name;
        }
       
        $data=Zone::deviceListByZone('device','footfall.csv',$selectedDate,$zone_id);
        $deviceList=$data['camera_names'];
        
        if($selectedDate){
            $selectedDate=date('Y-m-d',strtotime($selectedDate));
        }

        $device_foot_fall=CameraTransaction::fetchDeviceFootFallDate($deviceList,$selectedDate);
      
        $deviceList=$deviceList;
        $deviceList=implode(',',array_flip($deviceList));
   
       
        $url="?device=$deviceList";
        $url.="&selected_date=$selectedDate";

       

        
        return view('camera-transaction.foot-fall-device',compact('zone_name','data','device_foot_fall','url'));
    }

    /**
     * Get chart file content from file, because some direct file cache issue
     */
    function chartFileContent(Request $request){
        $file_name=$request->chart_file_path;
        $file_name=urldecode($file_name);
       
        $filename = 'app/public/'.$file_name;
       $path = storage_path($filename);
       return response()->file($path);
    }


    // function currentOccupancyOld(Request $request){
    //     $where=[];
    //     $zones=[];
    //     $zonesList=['zones'=>[]];
    //     $timeSlots=[];
    //     $zone_id=$request->zone_id;
    //     $filterDate=$request->filter_date;

    //     $selectedDate=$request->filter_date?$request->filter_date:date('Y-m-d');
    //     if($selectedDate){
    //         $selectedDate=date('Y-m-d',strtotime($selectedDate));
    //     }
    //     $userInfo = User::getUserInfo();
    //     $company_id = $userInfo['company_id'];
    //     $branch_id = $userInfo['branch_id'];
    //     $companyInfo = Company::select('*')->where('id',$company_id)->first();
    //     $selectedInterval=$request->interval?$request->interval:$this->defaultInterval;
    //     $interval=$selectedInterval; //minute
    //     $timeSlots=CameraTransaction::getTimeSlot($branch_id,$selectedDate,$interval);
    //     $timeBreak=@$timeSlots['timeBreak'];

    //     // echo "<pre/>";
    //     // print_r($timeSlots);
    //     // print_r($timeBreak); exit;



    //     if($company_id){
    //         $company = Company::select('company_name')->where('id',$company_id)->first();
    //         $companyName = $company->company_name;
    //         $where['zones.company_id'] = $company_id;
    //     }
       
    //     if($branch_id){
    //         $where['zones.branch_id'] = $branch_id;
    //         $zones=Zone::where('branch_id',$branch_id)->pluck('zone_name','id');
    //         $zonesList['zones']=Zone::getZonesList($branch_id,$zone_id);
            
    //     }
    //     else{

    //     }

    //     // if($branch_id){
    //     //     $where['zones.branch_id'] = $branch_id;
    //     //     $intervalBreakData=CameraTransaction::getTimeSlot($branch_id,$selectedDate,$interval);
    //     // $intervalBreak=@$intervalBreakData['timeBreak'];
    //     // }
    //     // else{
    //     //     $adminBranchId =Zone::getBranchIdByZone($selectedZone);
    //     //     $intervalBreakData=CameraTransaction::getTimeSlot($adminBranchId,$selectedDate,$interval);
    //     //     $intervalBreak=@$intervalBreakData['timeBreak'];
    //     // }

      
      
    //     if($filterDate){
    //         $filterDate=date('Y-m-d',strtotime($filterDate));
    //     }
    //    // $data=Zone::Occupancy($where,$filterDate,$zone_id);
       
    //     $zoneData=[];
    //     if($zones){
    //         foreach($zones as $zone_id=>$zone){
    //             $currentOccupancy = CameraTransaction::ZoneWiseCount($zone_id,$selectedDate,'current_occupancy');
    //             $chart=CameraTransaction::zoneByOccupancyCount($zone_id,$timeBreak,$interval,'current_occupancy');
    //             $zoneData[$zone_id]=['currentOccupancy'=>$currentOccupancy,'chart'=>$chart];

                
    //         }
    //     }
       
       
    //     return view('camera-transaction.current-occupancy',compact('selectedInterval','zoneData','zones','zonesList','timeSlots'));
    // }

    /**
     * Foot Fall 
     */
    function footFall_Old_10_07_21(Request $request){
        $where=[];
        $zones=[];
        $zonesList=['zones'=>[]];
        $timeSlots=[];
        $zone_id=$request->zone_id;
        $filterDate=$request->filter_date;
        $selectedDate=$request->filter_date?$request->filter_date:date('Y-m-d');
        if($selectedDate){
            $selectedDate=date('Y-m-d',strtotime($selectedDate));
        }

        $userInfo = User::getUserInfo();
        $company_id = $userInfo['company_id'];
        $branch_id = $userInfo['branch_id'];
        $companyInfo = Company::select('*')->where('id',$company_id)->first();
        $selectedInterval=$request->interval?$request->interval:$this->defaultInterval;
        $interval=$selectedInterval; //minute
        $timeSlots=CameraTransaction::getTimeSlot($branch_id,$selectedDate,$interval);
        $timeBreak=@$timeSlots['timeBreak'];
        // echo "<pre/>";
        // print_r($timeBreak); exit;



        if($company_id){
            $company = Company::select('company_name')->where('id',$company_id)->first();
            $companyName = $company->company_name;
            $where['zones.company_id'] = $company_id;
        }
       
        if($branch_id){
            $where['zones.branch_id'] = $branch_id;
            $zones=Zone::where('branch_id',$branch_id)->pluck('zone_name','id');
            $zonesList['zones']=Zone::getZonesList($branch_id,$zone_id);
        }
        
        if($filterDate){
            $filterDate=date('Y-m-d',strtotime($filterDate));
        }
       // $data=Zone::Occupancy($where,$filterDate,$zone_id);
       
        $zoneData=[];
        if($zones){
            foreach($zones as $zone_id=>$zone){
                $zoneData[$zone_id]=CameraTransaction::zoneWiseFootFall($zone_id,$timeBreak,$interval,'in');
                
            }
        }
        
       
       
       
        return view('camera-transaction.foot-fall',compact('selectedInterval','zoneData','zones','zonesList','timeSlots'));
    }

    /**
     * Violation
     */
    function violationOld_10_07_21(Request $request){
        $where=[];
        $zones=[];
        $zonesList=['zones'=>[]];
        $timeSlots=[];
        $zone_id=$request->zone_id;
        $filterDate=$request->filter_date;
        $selectedDate=$request->filter_date?$request->filter_date:date('Y-m-d');
        if($selectedDate){
            $selectedDate=date('Y-m-d',strtotime($selectedDate));
        }

        $userInfo = User::getUserInfo();
        $company_id = $userInfo['company_id'];
        $branch_id = $userInfo['branch_id'];
        $companyInfo = Company::select('*')->where('id',$company_id)->first();
        $selectedInterval=$request->interval?$request->interval:$this->defaultInterval;
        $interval=$selectedInterval; //minute
        $timeSlots=CameraTransaction::getTimeSlot($branch_id,$selectedDate,$interval);
        $timeBreak=@$timeSlots['timeBreak'];
        // echo "<pre/>";
        // print_r($timeBreak); exit;



        if($company_id){
            $company = Company::select('company_name')->where('id',$company_id)->first();
            $companyName = $company->company_name;
            $where['zones.company_id'] = $company_id;
        }
       
        if($branch_id){
            $where['zones.branch_id'] = $branch_id;
            $zones=Zone::where('branch_id',$branch_id)->pluck('zone_name','id');
            $zonesList['zones']=Zone::getZonesList($branch_id,$zone_id);
        }
       
        if($filterDate){
            $filterDate=date('Y-m-d',strtotime($filterDate));
        }
       // $data=Zone::Occupancy($where,$filterDate,$zone_id);
       
        $zoneData=[];
        if($zones){
            foreach($zones as $zone_id=>$zone){
                $zoneData[$zone_id]=CameraTransaction::zoneWiseViolation($zone_id,$timeBreak,$interval,'occupancy_violation');

                //$zoneData[$zone_id]=CameraTransaction::zoneByOccupancyCount($zone_id,$timeBreak,$interval,'occupancy_violation');
                
            }
        }
        return view('camera-transaction.occupancy-violation',compact('selectedInterval','zoneData','zones','zonesList','timeSlots'));
    }


    function getZoneOccupancy(Request $request){
        $zone_id=$request->zone_id;
        $selectedDate=$request->selectedDate;
        if($selectedDate){
            $selectedDate=date('Y-m-d',strtotime($selectedDate));
        }

        $interval=$request->interval;
        $dataType=$request->dataType;

        $userInfo = User::getUserInfo();
        $company_id = $userInfo['company_id'];
        $branch_id = $userInfo['branch_id'];
        $timeSlots=CameraTransaction::getTimeSlot($branch_id,$selectedDate,$interval);
        $timeBreak=@$timeSlots['timeBreak'];
        //$zoneData=CameraTransaction::zoneByOccupancyCount($zone_id,$timeBreak,$interval,$dataType);
        if($dataType=='current_occupancy'){
        $currentOccupancy = CameraTransaction::ZoneWiseCount($zone_id,$selectedDate,'current_occupancy');
        $chart=CameraTransaction::zoneByOccupancyCount($zone_id,$timeBreak,$interval,'current_occupancy');
        $zoneData=['currentOccupancy'=>$currentOccupancy,'chart'=>$chart];
        }
        else if($dataType=="occupancy_violation"){
            $zoneData['chart']=CameraTransaction::zoneWiseViolation($zone_id,$timeBreak,$interval,'occupancy_violation');

        }
        else{
            //Total Enter 
            //zoneWiseFootFall
            $zoneData['chart']=CameraTransaction::zoneWiseFootFall($zone_id,$timeBreak,$interval,$dataType);
           // $zoneData['chart']=CameraTransaction::zoneByOccupancyCount($zone_id,$timeBreak,$interval,$dataType);
        }


        return response()->json($zoneData);
        //return view('welcome');
        
    }

   




  
    
    // function footFallOld(Request $request){
    //     $where=[];
    //     $zones=[];
    //     $userInfo = User::getUserInfo();
    //     $company_id = $userInfo['company_id'];
    //     $branch_id = $userInfo['branch_id'];
    //     $companyInfo = Company::select('*')->where('id',$company_id)->first();
    //     if($company_id){
    //         $company = Company::select('company_name')->where('id',$company_id)->first();
    //         $companyName = $company->company_name;
    //         $where['cameras.company_id'] = $company_id;
    //     }

    //     if($branch_id){
    //         $where['cameras.branch_id'] = $branch_id;
    //         $zones=Zone::pluck('zone_name','id');
    //     }
    //     $zone_id=$request->zone_id;
    //     $filterDate=$request->filter_date;
    //     if($filterDate){
    //         $filterDate=date('Y-m-d',strtotime($filterDate));
    //     }
    //     $data=Camera::select('camera_transactions.*','company_name','branch_name','zone_name','device_name')
    //     ->join('camera_transactions','device_id','=','device')
    //     ->join('companies','company_id','=','companies.id')
    //     ->join('branches','branch_id','=','branches.id')
    //     ->join('zones','cameras.zone_id','=','zones.id')
    //     ->where($where)
    //     ->where(function($query) use ($filterDate,$zone_id){
    //         if($filterDate){
    //         $query->whereDate('camera_transactions.created_at','=',$filterDate);
    //         $query->whereIn('camera_transactions.id', function($q){
    //             $q->select(DB::raw('MAX(camera_transactions.id) FROM camera_transactions GROUP BY device'));
    //            });
    //         }
    //         if($zone_id){
    //             $query->where('zone_id',$zone_id);
    //         }
    //     })
    //     ->groupBy('device')
    //     ->orderBy('camera_transactions.id','desc')
    //     ->get();
        
    //     return view('camera-transaction.foot-fall',compact('data','zones'));
    // }

   
    
    // function violationOld(Request $request){
    //     $where=[];
    //     $zones=[];
    //     $userInfo = User::getUserInfo();
    //     $company_id = $userInfo['company_id'];
    //     $branch_id = $userInfo['branch_id'];
    //     $companyInfo = Company::select('*')->where('id',$company_id)->first();
    //     if($company_id){
    //         $company = Company::select('company_name')->where('id',$company_id)->first();
    //         $companyName = $company->company_name;
    //         $where['cameras.company_id'] = $company_id;
    //     }

    //     if($branch_id){
    //         $where['cameras.branch_id'] = $branch_id;
    //         $zones=Zone::where('branch_id',$branch_id)->pluck('zone_name','id');
    //     }
    //     $zone_id=$request->zone_id;
    //     $filterDate=$request->filter_date;
    //     if($filterDate){
    //         $filterDate=date('Y-m-d',strtotime($filterDate));
    //     }
    //     $data=Camera::select('camera_transactions.*','company_name','branch_name','zone_name','device_name')
    //     ->join('camera_transactions','device_id','=','device')
    //     ->join('companies','company_id','=','companies.id')
    //     ->join('branches','branch_id','=','branches.id')
    //     ->join('zones','cameras.zone_id','=','zones.id')
    //     ->where($where)
    //     ->where('occupancy_violation',1)
    //     ->where(function($query) use ($filterDate,$zone_id){
    //         if($filterDate){
    //         $query->whereDate('camera_transactions.created_at','=',$filterDate);
    //         // $query->whereIn('camera_transactions.id', function($q){
    //         //     $q->select(DB::raw('MAX(camera_transactions.id) FROM camera_transactions GROUP BY device'));
    //         //    });

    //         }
    //         if($zone_id){
    //             $query->where('zone_id',$zone_id);
    //         }
    //     })
    //     //->groupBy('device')
    //     ->orderBy('camera_transactions.id','desc')
    //     ->get();
    //     return view('camera-transaction.violation',compact('data','zones'));
    // }
    

    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CameraTransaction  $cameraTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(CameraTransaction $cameraTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CameraTransaction  $cameraTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(CameraTransaction $cameraTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CameraTransaction  $cameraTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CameraTransaction $cameraTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CameraTransaction  $cameraTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(CameraTransaction $cameraTransaction)
    {
        //
    }

    // function currentOccupancyOld(Request $request){
    //     $where=[];
    //     $zones=[];
    //     $userInfo = User::getUserInfo();
    //     $company_id = $userInfo['company_id'];
    //     $branch_id = $userInfo['branch_id'];
    //     $companyInfo = Company::select('*')->where('id',$company_id)->first();
    //     if($company_id){
    //         $company = Company::select('company_name')->where('id',$company_id)->first();
    //         $companyName = $company->company_name;
    //         $where['cameras.company_id'] = $company_id;
    //     }

    //     if($branch_id){
    //         $where['cameras.branch_id'] = $branch_id;
    //         $zones=Zone::where('branch_id',$branch_id)->pluck('zone_name','id');
    //     }
    //     $zone_id=$request->zone_id;
    //     $filterDate=$request->filter_date;
    //     if($filterDate){
    //         $filterDate=date('Y-m-d',strtotime($filterDate));
    //     }
    //     $data=Camera::select('camera_transactions.*','company_name','branch_name','zone_name','device_name','max_occupancy')
    //     ->join('camera_transactions','device_id','=','device')
    //     ->join('companies','company_id','=','companies.id')
    //     ->join('branches','branch_id','=','branches.id')
    //     ->join('zones','zone_id','=','zones.id')
    //     ->where($where)
    //     ->where(function($query) use ($filterDate,$zone_id){
    //         if($filterDate){
    //         $query->whereDate('camera_transactions.created_at','=',$filterDate);
    //         $query->whereIn('camera_transactions.id', function($q){
    //             $q->select(DB::raw('MAX(camera_transactions.id) FROM camera_transactions GROUP BY device'));
    //            });
    //         }
    //         if($zone_id){
    //             $query->where('zone_id',$zone_id);
    //         }
    //     })
    //     ->groupBy('device')
    //     ->orderBy('camera_transactions.id','desc')
    //     ->get();

       


    //     return view('camera-transaction.current-occupancy',compact('data','zones'));
    // }

}
