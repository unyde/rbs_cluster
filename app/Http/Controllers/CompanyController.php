<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   //Search Parameter
        $q=$request->q;
        $companies = Company::where(function($query) use ($q){
            if($q){
                $query->where('company_name','Like',"%$q%");
            }
        })->orderBy('id','desc')->get();
        $i=1;
        return view('company.index',compact('companies','i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required|unique:companies',
        ]);

        $input = $request->all();
        
        $company = Company::create($input);
        if($company){
            return redirect()->route('company.index')

            ->with('success','Company created successfully');
        }
        else{

        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {    

        $this->validate($request, [
            'company_name' => 'required|unique:companies,company_name,'.$company->id,
        ]);

        $company->company_name   = $request->company_name;
        $company->email          = $request->email;
        $company->contact_number = $request->contact_number;
        $company->latitude          = $request->latitude;
        $company->longitude = $request->longitude;

        if($company->save()){
            return redirect()->route('company.index')

            ->with('success','Company updated successfully');
        }
        else{
            return redirect()->route('company.index')

            ->with('warning','Please try again.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return redirect()->route('company.index')
    
                            ->with('success','Company deleted successfully');
    }
}
