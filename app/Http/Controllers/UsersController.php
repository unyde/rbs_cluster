<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Company;

class UsersController extends Controller
{
    private $companyList;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->companyList=Company::companyList();
        $this->middleware('auth');
        $this->middleware('permission:user-list',['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:change-password', ['only' => ['changePassword','postChangePassword']]);
        
    }
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $company_id=$request->company_id;
        $role_id   = $request->role_id;
        $search    = $request->search;
        $where=[];
        $roles=Role::pluck('name','id');
        
        if($role_id){
            $data = User::select('name','user.email','user.id','company_name','branch_name')
            ->join('model_has_roles','id','=','model_id')
            ->leftJoin('companies','company_id','=','companies.id')
            ->leftJoin('branches','branch_id','=','branches.id')
            ->where(function($query) use ($search,$company_id){
                if($search){
                    $query->where('email','LIKE','%'.$search.'%');
                }
                if($company_id){
                    $query->where('user.company_id',$company_id);
                }
                
            })
            ->where('role_id',$role_id)
            ->orderBy('id','DESC')->paginate(config('app.per_page'));
	    }
        else{
            $data = User::select('name','user.email','user.id','company_name','branch_name')
            ->leftJoin('companies','company_id','=','companies.id')
            ->leftJoin('branches','branch_id','=','branches.id')
            ->where(function($query) use ($search,$company_id){
                if($search){
                    $query->where('email','LIKE','%'.$search.'%');
                }
                if($company_id){
                    $query->where('user.company_id','=',$company_id);
                }
                
            })
            ->orderBy('user.id','DESC')->paginate(config('app.per_page'));
	     }
       
        $slno= ($request->input('page', 1) - 1) * config('app.per_page');
        return view('users.index',compact('data','roles'))
        ->with(['i'=>$slno,'companyList'=>$this->companyList]);



        // $search = $request->search;
        // $users = User::where(function($query) use ($search) {
        //     if($search){
        //         $query->where('name','like',"%$search%");
        //         $query->orWhere('email','like',"%$search%");
        //     }
        // })->orderBy('id','DESC')->paginate(config('app.per_page'));

        // return view('users.index',compact('users'))
        // ->with('i', ($request->input('page', 1) - 1) * config('app.per_page'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $roles = Role::pluck('name','name')->all();
       return view('users.create',compact('roles'))->with(['companyList'=>$this->companyList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $messages = [
            'email.required' => 'The username field is required.'
        ];


        $this->validate($request, [

            'email' => 'required|unique:user',
            'password' => 'required|same:confirm-password',
            // 'roles' => 'required'

        ],$messages);



        $input = $request->all();

        $input['password'] = Hash::make($input['password']);



        $user = User::create($input);

        $user->assignRole($request->input('roles'));
		#$user->givePermissionTo('role-list');
		#$user->givePermissionTo('role-list');
		



        return redirect()->route('users.index')

                        ->with('success','User created successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('name','name')->all();

        $userRole = $user->roles->pluck('name','name')->all();

        return view('users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'email.required' => 'The username field is required.'
        ];

        $this->validate($request, [

            'email' => 'required|unique:user,email,'.$id,

            'password' => 'same:confirm-password',

            'roles' => 'required'

        ],$messages);



        $input = $request->all();

        if(!empty($input['password'])){ 

            $input['password'] = Hash::make($input['password']);

        }else{
            // echo "<pre/>";
            // print_r($input);
            
            $input =Arr::except($input,array('password'));    

        }


       
        $user = User::find($id);
     
        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();



        $user->assignRole($request->input('roles'));



        return redirect()->route('users.index')

                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)

    {

        $user->delete();
    return redirect()->route('users.index')

                        ->with('success','User deleted successfully');

    }

    public function export(Request $request) 
    {
        $export_type= $request->export_type;
        $file_name="users.xlsx";
        if($export_type=="xls"){

        }
        else if($export_type="csv"){
            $file_name="users.csv";
        }
        $search= $request->search;
        return Excel::download(new UsersExport($search), $file_name);
    }

    function changePassword(){
        return view('users.changepassword');
    }

    function postChangePassword(Request $request){

        $this->validate($request, [
            'password' => 'required|same:confirm-password',
        ]);

        $id = Auth::user()->id; 
        $password = Hash::make($request->password);
        User::where(['id'=>$id])->update(['password'=>$password]);
        
        return redirect('/')->with('success','Password change successfully.');


    }




}
