<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;
use App\Company;
use App\User;
class BranchController extends Controller
{   private $companyList;
    public function __construct()
    {
        $this->companyList=Company::companyList();
        $this->middleware('auth');
        $this->middleware('permission:branch-list',['only' => ['index']]);
        $this->middleware('permission:branch-create', ['only' => ['create','store']]);
        $this->middleware('permission:branch-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:branch-delete', ['only' => ['destroy']]);
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       //Search Parameter
       $company_id=$request->company_id;
       $q=$request->q;

       $userInfo= User::getUserInfo();
       $branches = Branch::select('branches.*','company_name')
        ->join('companies','company_id','=','companies.id')
        ->where(function($query) use ($userInfo,$company_id,$q){
            if($company_id){
                //
            }
            else{
                $company_id=$userInfo['company_id'];
            }
           
            $branch_id=$userInfo['branch_id'];
          
            if($company_id){
                $query->where('company_id',$company_id);
            }
            if($branch_id){
                $query->where('branch_id',$branch_id);
            }
            if($q){
                $query->where('branch_name','Like',"%$q%");
            }
            
        })
        ->orderBy('branches.id','desc')->get();
        $i=0;
        $companyList=$this->companyList;
        return view('branch.index',compact('branches','i','companyList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userInfo= User::getUserInfo();
        return view('branch.create',compact('userInfo'))->with(['companyList'=>$this->companyList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'branch_name' => 'required|unique:branches',
        ]);

        $input = $request->all();
        
        $company = Branch::create($input);
        if($company){
            return redirect()->route('branch.index')

            ->with('success','Branch created successfully');
        }
        else{

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        $userInfo= User::getUserInfo();
        return view('branch.edit',compact('branch','userInfo'))->with(['companyList'=>$this->companyList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
      

        $branch->company_id   = $request->company_id;
        $branch->branch_name          = $request->branch_name;
        $branch->branch_address = $request->branch_address;
        $branch->contact_number = $request->contact_number;
        $branch->mobile = $request->mobile;
        $branch->email = $request->email;
        $branch->start_time = $request->start_time;
        $branch->end_time  = $request->end_time;
        if($branch->save()){
            return redirect()->route('branch.index')

            ->with('success','Branch updated successfully');
        }
        else{
            return redirect()->route('Branch.index')

            ->with('warning','Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        return redirect()->route('branch.index')
    
                            ->with('success','Branch deleted successfully');
    }

    /**
     * Branch list by compaany id 
     */
    public function branchList(Request $request,$companyId)
    {
      $branches = Branch::where(['company_id'=>$companyId])->pluck('branch_name','id');
      return response()->json(['data'=>$branches]);
    }

}
