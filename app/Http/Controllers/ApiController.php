<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use App\Helpers\Common;
use App\Jobs\CaptureRequest;
use Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
class ApiController extends Controller
{
    public function __construct(Request $request)
    {

    }

    public function sendResponse($data) {
        $dataArray = array();
        $dataArray['status']  = $data['status'];
        $dataArray['message'] = $data['message'];
        $dataArray['data']    = $data['data'];
        return $dataArray;
     }
     public function notFound($message) {
        $dataArray = array();
        $dataArray['status']  = false;
        $dataArray['message'] = $message;
        $dataArray['data']    = [];
        return $dataArray;
     }
     protected function respondWithError($message){
        return response()->json(['success' => false,'data'=>[],'message' => $message]);
    }
}
