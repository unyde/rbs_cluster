<?php

namespace App\Http\Controllers;

use App\TagUser;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Branch;
class TagUserController extends Controller
{
    private $companyList;
    public function __construct()
    {
        $this->companyList=Company::companyList();
        $this->middleware('auth');
        $this->middleware('permission:tag-user-list',['only' => ['index']]);
        $this->middleware('permission:tag-user-create', ['only' => ['create','store']]);
        $this->middleware('permission:tag-user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:tag-user-delete', ['only' => ['destroy']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userInfo= User::getUserInfo();
        $tag_users = TagUser::select('tag_users.id','username','tag_users.created_at','company_name','branch_name')
        ->join('companies','company_id','=','companies.id')
        ->join('branches','branch_id','=','branches.id')
        ->where(function($query) use ($userInfo){
            $company_id=$userInfo['company_id'];
            $branch_id=$userInfo['branch_id'];
            if($company_id){
                $query->where('tag_users.company_id',$company_id);
            }
            if($branch_id){
                $query->where('tag_users.branch_id',$branch_id);
            }
        })
        ->orderBy('tag_users.id','desc')->get();
        $i=0;
        return view('tag_user.index',compact('tag_users','i'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userInfo= User::getUserInfo();
        $branches = Branch::branchByCompanyId($userInfo['company_id']);
        return view('tag_user.create',compact('userInfo','branches'))->with(['companyList'=>$this->companyList]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'branch_id' => 'required',
            'username' => 'required',
            'password' => 'required|min:6',
        ]);

        $input = $request->all();
        
        $tagUser = TagUser::create($input);
        if($tagUser){
            return redirect()->route('tag-user.index')

            ->with('success','Tag User created successfully');
        }
        else{

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TagUser  $tagUser
     * @return \Illuminate\Http\Response
     */
    public function show(TagUser $tagUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TagUser  $tagUser
     * @return \Illuminate\Http\Response
     */
    public function edit(TagUser $tagUser)
    {
        $userInfo= User::getUserInfo();
        $branches = Branch::branchByCompanyId($tagUser->company_id);
        return view('tag_user.edit',compact('tagUser','userInfo','branches'))->with(['companyList'=>$this->companyList]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TagUser  $tagUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TagUser $tagUser)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'branch_id' => 'required',
            'username' => 'required',
            'password' => 'required|min:6',
        ]);


        $tagUser->company_id     = $request->company_id;
        $tagUser->branch_id      = $request->branch_id;
        $tagUser->username      = $request->username;
        $tagUser->password         = $request->password;
        if($tagUser->save()){
            return redirect()->route('tag-user.index')

            ->with('success','Tag User updated successfully');
        }
        else{
            return redirect()->route('tag-user.index')

            ->with('warning','Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TagUser  $tagUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(TagUser $tagUser)
    {
        $tagUser->delete();
        return redirect()->route('tag-user.index')
    
                            ->with('success','Tag user deleted successfully');
    }
}
