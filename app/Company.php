<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Company extends Model
{
    protected $fillable=['company_name','email','contact_number','latitude','longitude'];
    use SoftDeletes;

    static function companyList(){
        return Company::pluck('company_name','id');
    }
}
