<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage; 
use App\Helpers\Common;
use Illuminate\Database\Eloquent\SoftDeletes;
class CameraTransaction extends Model
{
    use SoftDeletes;
    protected $fillable=['delta_in_out','zone_total_in','zone_total_enter','enter','reset_device','zone_id','camera_id','duration','total_duration','in','out','current_occupancy','device','occupancy_violation','violation_start','violation_end','violation_duration','visit_count','base_value'];
    static function getFilePath($fileName){
        
        $fileName  = "data/$fileName";
        $defaultFile = "storage/"; 
        if(Storage::exists("public/".$fileName)) {
           $defaultFile.= $fileName;
        }
        else{
           $defaultFile.="data/default.csv"; 
        }
        $defaultFile=asset($defaultFile);
        return $defaultFile; 
        
    }

    static function getTimeSlot($branch_id,$selectedDate,$intervalTime){
        $selectedDate=trim($selectedDate,"");
        $timeSlots=[];
        $slots=[];
        $timeBreak=[];
        $branchInfo = Branch::where('id',$branch_id)->first();
        if($branchInfo){
        $start_time = $branchInfo->start_time;
        $end_time   = $branchInfo->end_time;
        if($start_time){
            $start_time=date('H:i',strtotime($start_time));
            $time=strtotime($start_time);
        }
        if($end_time){
            $end_time=date('H:i',strtotime($end_time));
        }
        if($start_time && $end_time){
            //Start Time
            $slot=['label'=>$start_time];
            $timeSlots[]=$slot;
            
            // $intervalTime=30;
            $interval = round(abs(strtotime($end_time) - strtotime($start_time)) / 60,2);
            $interval = ceil($interval/$intervalTime);
           
           // echo $interval; exit; 
            for($i=1;$i<$interval;$i++){
                $addTime=$i*$intervalTime;// Add in minute
                $newTime=date("H:i", strtotime("+$addTime minutes", $time));
                $slot=[];
                $slot['label']=$newTime;
                $timeSlots[]=$slot;
            }

            //End Time
            $slot=['label'=>$end_time];
            $timeSlots[]=$slot;
        }
        // echo "<pre/>";
        // print_r($timeSlots); exit; 
        if(is_array($timeSlots) && count($timeSlots)){
            foreach($timeSlots as $key=>$slot){
                $from_time ="";
                $to_time="";
               // $to_time=$slot['label'];
               $from_time = $timeSlots[$key]['label'];
                $timeSlots[$key]['label_text']=str_replace(':00','',date('g:i a',strtotime($from_time)));
              
                $slots[]= $timeSlots[$key]['label_text'];
                //print_r($timeSlots[$key]); exit; 
                // if($key){
                //     $previousTime=$key-1;
                //     $from_time = $timeSlots[$previousTime]['label'];
                // }
               
              
                $nextKey=$key+1;
                if(array_key_exists($nextKey,$timeSlots)){
                    $to_time=$timeSlots[$nextKey]['label'];
                }
                else{
                    $newFrom=strtotime($from_time);
                    $to_time=date("H:i", strtotime("+$intervalTime minutes", $newFrom));
                }
                $selectFromTime ="";
                $selectToTime="";
                if($from_time){
                    $selectFromTime=$selectedDate."".$from_time.":00";
                    $selectFromTime=date('Y-m-d H:i:s',strtotime($selectFromTime));

                }
             
               
                if($to_time){
                    $selectToTime=$selectedDate." ".$to_time.":00";
                    $selectToTime=date('Y-m-d H:i:s',strtotime($selectToTime));
                    
                    if(!$selectFromTime){
                        //$newTime=strtotime($selectToTime);
                       //$selectFromTime=date("Y-m-d H:i:s", strtotime("-$intervalTime minutes", $newTime));
   
                    }
                }
                $timeBreak[]=['from_time'=>$selectFromTime,'to_time'=>$selectToTime];
                $timeSlots[$key]['selectedDate'] = ['from_time'=>$selectFromTime,'to_time'=>$selectToTime];

            }
        }
        
    }
   
    $data= ['slots'=>$slots,'timeBreak'=>$timeBreak,'timeSlots'=>$timeSlots];
    // echo "<pre/>";
    // print_r($data); exit;
    return $data;
}

/**
 * Get max occupancy count by zone id with time
 * @zoneId - Id of the zone
 * @filterDate - List of date in array
 * $interval- Interval time in minute
 * @dateType - Which field access from table
 */
static function zoneByOccupancyCount($zoneId,$filterDate,$interval,$dataType){
    // echo "<pre/>";
    // print_r($filterDate); exit;
    $data=[];
    $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
  
    $totalTimeBreak=0;
    $totalTimeBreakData=[];
    if(is_array($filterDate)){
        $totalTimeBreak=count($filterDate);
        $totalTimeBreakData = array_fill(0, $totalTimeBreak,0);
    }

    if($cameras){
      if(is_array($filterDate)){
        foreach($filterDate as $indexKey=>$timeBreak){  
      
        $occupancyCount=0;    
        foreach($cameras as $camera){
            $device=$camera->device_id; 
            $from_time=$timeBreak['from_time'];
            $to_time=$timeBreak['to_time'];
            if($from_time && $to_time){
                $cameraTransaction=CameraTransaction::select('*')
                ->where('device',$device)
                ->where(function($query) use ($from_time,$to_time,$indexKey){
                    if($from_time && $to_time){
                        $query->where('camera_transactions.created_at','>=',$from_time);
                        $query->where('camera_transactions.created_at','<=',$to_time);
                    }
                

                // if($to_time){
                //     if(!$from_time){
                //       //  echo $indexKey."FromTime==".$from_time."<br/>";
                //         $query->where('camera_transactions.created_at','>=',$to_time);
                //     }
                //     $query->where('camera_transactions.created_at','<=',$to_time);
                    
                // }
                // $to_time=date('Y-m-d',strtotime($to_time));
                // $query->whereDate('camera_transactions.created_at','=',$to_time);
                })
                ->orderBy('id','desc')->first();
            
                if($cameraTransaction){
                    //Occupany
                    if($dataType=='current_occupancy'){
                        $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$cameraTransaction->current_occupancy;
                    }
                    else if($dataType=='in'){
                        $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$cameraTransaction->in;
                    }
                    else if($dataType=='occupancy_violation'){
                        $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$cameraTransaction->occupancy_violation;
                    }
                }
                else{
                    if($indexKey){
                        $from_time=$timeBreak['from_time'];
                        $to_time=$timeBreak['to_time'];
                        $currentTime = date('Y-m-d H:i:s');
                        if($from_time<=$currentTime){
                        $totalTimeBreakData[$indexKey] =  $totalTimeBreakData[$indexKey-1];
                        break;
                        }
                    }
                }
            }else{
                $totalTimeBreakData[$indexKey]=0;
            }

    }
   
    #$totalTimeBreakData[$indexKey]=$occupancyCount;
   
}}}
// print_r($totalTimeBreakData); exit; 
return $totalTimeBreakData; 
}

/**
 * Foot Fall Chart Data
 */
static function zoneWiseFootFall($zoneId,$filterDate,$interval,$dataType){
    $data=[];
    $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
    $totalTimeBreak=0;
    $totalTimeBreakData=[];
    if(is_array($filterDate)){
        $totalTimeBreak=count($filterDate);
        $totalTimeBreakData = array_fill(0, $totalTimeBreak,0);
    }

    if($cameras){
      if(is_array($filterDate)){
        foreach($filterDate as $indexKey=>$timeBreak){  
      
        $occupancyCount=0;    
        foreach($cameras as $camera){
            $device=$camera->device_id; 
            $from_time=$timeBreak['from_time'];
            $to_time=$timeBreak['to_time'];
            if($from_time && $to_time){
                $cameraTransaction=CameraTransaction::select('*')
                ->where('device',$device)
                ->where(function($query) use ($from_time,$to_time,$indexKey){
                    if($from_time && $to_time){
                        $query->where('camera_transactions.created_at','>=',$from_time);
                        $query->where('camera_transactions.created_at','<=',$to_time);
                    }
                })
                ->get();
            
                if($cameraTransaction){
                    foreach($cameraTransaction as $transaction){
                        
                        $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$transaction->enter;


                    }
                  

                    // //Occupany
                    // if($dataType=='current_occupancy'){
                    //     $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$cameraTransaction->current_occupancy;
                    // }
                    // else if($dataType=='in'){
                    //     $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$cameraTransaction->in;
                    // }
                    // else if($dataType=='occupancy_violation'){
                    //     $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$cameraTransaction->occupancy_violation;
                    // }
                }
                // else{
                //     if($indexKey){
                //         $from_time=$timeBreak['from_time'];
                //         $to_time=$timeBreak['to_time'];
                //         $currentTime = date('Y-m-d H:i:s');
                //         if($from_time<=$currentTime){
                //         $totalTimeBreakData[$indexKey] =  $totalTimeBreakData[$indexKey-1];
                //         break;
                //         }
                //     }
                // }
            }else{
                $totalTimeBreakData[$indexKey]=0;
            }

    }
   
    #$totalTimeBreakData[$indexKey]=$occupancyCount;
   
}}}
// print_r($totalTimeBreakData); exit; 
return $totalTimeBreakData; 
}


/**
 * Zone wise violation
 */
static function zoneWiseViolation($zoneId,$filterDate,$interval,$dataType){
    $data=[];
    $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
  
    $totalTimeBreak=0;
    $totalTimeBreakData=[];
    if(is_array($filterDate)){
        $totalTimeBreak=count($filterDate);
        $totalTimeBreakData = array_fill(0, $totalTimeBreak,0);
    }
   
    if($cameras){
      if(is_array($filterDate)){
        foreach($filterDate as $indexKey=>$timeBreak){  
        $occupancyCount=0;    
        foreach($cameras as $camera){
            $device=$camera->device_id; 
            $cameraTransaction=CameraTransaction::select('*')
            ->where('device',$device)
            ->where('occupancy_violation',1)
            ->where(function($query) use ($timeBreak,$indexKey){
                $from_time=$timeBreak['from_time'];
                $to_time=$timeBreak['to_time'];
                if($from_time){
                     $query->where('camera_transactions.created_at','>=',$from_time);
                 }
                if($to_time){
                if(!$from_time){
                    $query->where('camera_transactions.created_at','>=',$to_time);
                }
                $query->where('camera_transactions.created_at','<=',$to_time);
              }
            })
            ->orderBy('id','desc')->get();
           
            if($cameraTransaction){
               
               foreach($cameraTransaction as $violation){
                    $totalTimeBreakData[$indexKey] =$totalTimeBreakData[$indexKey]+$violation->occupancy_violation;
                }
            }
            else{
                // if($indexKey){
                //     $from_time=$timeBreak['from_time'];
                //     $to_time=$timeBreak['to_time'];
                //     $currentTime = date('Y-m-d H:i:s');
                //     if($from_time<=$currentTime){
                //     $totalTimeBreakData[$indexKey] =  $totalTimeBreakData[$indexKey-1];
                //     break;
                //     }
                // }
            }
    }
   
    #$totalTimeBreakData[$indexKey]=$occupancyCount;
   
}}}
return $totalTimeBreakData; 
}


/**
 * Zone wise violation Count Full Time interval sum
 */
static function zoneWiseViolationCount($zoneId,$filterDate,$interval,$dataType){
    $data=[];
    $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
    $total=0;
    $totalTimeBreak=0;
    $totalTimeBreakData=[];
    if(is_array($filterDate)){
        $totalTimeBreak=count($filterDate);
        $totalTimeBreakData = array_fill(0, $totalTimeBreak,0);
    }
   
    if($cameras){
      if(is_array($filterDate)){
        foreach($filterDate as $indexKey=>$timeBreak){  
        $occupancyCount=0;    
        foreach($cameras as $camera){
            $device=$camera->device_id; 
            $cameraTransaction=CameraTransaction::select('*')
            ->where('device',$device)
            ->where('occupancy_violation',1)
            ->where(function($query) use ($timeBreak,$indexKey){
                $from_time=$timeBreak['from_time'];
                $to_time=$timeBreak['to_time'];
                if($from_time){
                     $query->where('camera_transactions.created_at','>=',$from_time);
                 }
                if($to_time){
                if(!$from_time){
                    $query->where('camera_transactions.created_at','>=',$to_time);
                }
                $query->where('camera_transactions.created_at','<=',$to_time);
              }
            })
            ->orderBy('id','desc')->get();
           
            if($cameraTransaction){
               
               foreach($cameraTransaction as $violation){
                    $total =$total+$violation->occupancy_violation;
                }
            }
            else{
                // if($indexKey){
                //     $from_time=$timeBreak['from_time'];
                //     $to_time=$timeBreak['to_time'];
                //     $currentTime = date('Y-m-d H:i:s');
                //     if($from_time<=$currentTime){
                //     $totalTimeBreakData[$indexKey] =  $totalTimeBreakData[$indexKey-1];
                //     break;
                //     }
                // }
            }
    }
   
    #$totalTimeBreakData[$indexKey]=$occupancyCount;
   
}}}
return $total; 
}





 /**
 * Get max occupancy count by zone id with time
 * @zoneId - Id of the zone
 * @filterDate - List of date in array
 * $interval- Interval time in minute
 * @dateType - Which field access from table
 */
static function ZoneCount($zoneId,$selectedDate){
    $data=[];
    $occupancyCount=0;
    $footFall=0;
    $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
    foreach($cameras as $camera){
            $device=$camera->device_id; 
            $cameraTransaction=CameraTransaction::select('*')
                                ->where('device',$device)
                                ->whereDate('camera_transactions.created_at','=',$selectedDate)
                                ->orderBy('id','desc')
                                ->first();
            if($cameraTransaction){
              $occupancyCount+=  $cameraTransaction->current_occupancy;
              $footFall+=$cameraTransaction->in; 
            }
           
           
    }
    $data=['occupancy'=>$occupancyCount,'footFall'=>$footFall]; 
    return $data; 

 }


 static function ZoneWiseCount($zoneId,$selectedDate,$dataType){
    $cameras=Camera::select('device_id','id')->where('zone_id',$zoneId)->get();
    $startTime="";
    $endTime="";
    $branch_id =0;
    if($zoneId){
        $zone=Zone::select('branch_id')->where('id',$zoneId)->first();
        $branch_id=$zone->branch_id;
        if($branch_id){
            $branch=Branch::where('id',$branch_id)->first();
            if($branch){
                $startTime=$branch->start_time;
                $endTime=$branch->end_time;
                $startTime=CameraTransaction::getDateWithTime($selectedDate,$startTime);
                $endTime=CameraTransaction::getDateWithTime($selectedDate,$endTime);
            }
        }
    } 
    $total=0;
    foreach($cameras as $camera){
            $device=$camera->device_id; 
            $cameraTransaction=CameraTransaction::select('*')
                                ->where('device',$device)
                                ->where(function($query) use ($dataType,$startTime,$endTime,$selectedDate){
                                    if($dataType=='occupancy_violation'){
                                        $query->where('occupancy_violation','=',1);
                                    }
                                    if($startTime && $endTime){
                                        $query->where('camera_transactions.created_at','>=',$startTime);
                                        $query->where('camera_transactions.created_at','<=',$endTime);
                                    }
                                    else{
                                        $query->whereDate('camera_transactions.created_at','=',$selectedDate);
                                    }
                                })
                                
                                ->orderBy('id','desc')
                                ->first();
            if($cameraTransaction){
              if($dataType=='current_occupancy'){  
                $total+=  $cameraTransaction->current_occupancy;
              }
              if($dataType=='in'){  
                $total+=  $cameraTransaction->in;
              }
              if($dataType=='occupancy_violation'){  
                $total+=  1;
              }

            }
           
           
    }
    return $total; 

 }


 static function getDateWithTime($selectedDate,$time){
    if($time){
        $tiyme=date('H:i',strtotime($time));
        $time=strtotime($time);
        if($time){
            $time=$selectedDate." ".date('H:i:s',$time);
            return $time; 
        }
    }
 }

/**
 * Find last record from camera transaction table 
 */
static function findLastRecord($device,$fieldName,$whereCondition=null){
    $result=0;
    $where=[];
    if($whereCondition){
        $where=$whereCondition;
    }
    if($device){
        $where['device']=$device;
        $row=CameraTransaction::select('in')->where($where)->orderBy('id','desc')->first();
        if($row){
            //Get in
            if($fieldName=="in"){
                $result=$row->in;
            }
        }
    }
    
    return $result; 

}

/**
 * Find last record by zone id or device id
 */
static function lastTransaction($fieldName,$where,$branchTime){
    $startTime=@$branchTime['start_time'];
    $endTime=@$branchTime['end_time'];
    $result=0;
    $row=CameraTransaction::select("$fieldName")->where($where)
                    ->where(function($query) use ($startTime,$endTime){
                        if($startTime){
                            $query->where('camera_transactions.created_at','>=',$startTime);
                        }
                        if($endTime){
                            $query->where('camera_transactions.created_at','<=',$endTime);
                        }
                       
                    })
                    ->orderBy('id','desc')->first();
    if($row){
        $result=$row->$fieldName;
    }                
    return $result; 

}
/**
 * Find last record by zone id or device id
 */
static function violation($fieldName,$where,$branchTime){
    $startTime=@$branchTime['start_time'];
    $endTime=@$branchTime['end_time'];
    $result=0;
    $result=CameraTransaction::where($where)
                    ->where(function($query) use ($startTime,$endTime){
                        if($startTime){
                            $query->where('camera_transactions.created_at','>=',$startTime);
                        }
                        if($endTime){
                            $query->where('camera_transactions.created_at','<=',$endTime);
                        }
                       
                    })
                    ->where('occupancy_violation',1)
                    ->groupBy('zone_id')
                    ->sum("occupancy_violation");
                    
                
    return $result; 
                
}

/**
 * Get camera transaction
 */
static function cameraTransaction($whereCondition,$fetchField,$zoneOrdeviceId,$table="zone"){
    $where=[];
    $id="";
    $created_at="";
    $start_time="";
    $end_time="";
    
    if(array_key_exists('id',$whereCondition)){
        $id=$whereCondition['id'];
    }

    if(array_key_exists('zone_id',$whereCondition)){
        $where['zone_id']=$whereCondition['zone_id'];
    }
    if(array_key_exists('camera_id',$whereCondition)){
        $where['camera_id']=$whereCondition['camera_id'];
    }
    if(array_key_exists('start_time',$whereCondition)){
        $start_time=$whereCondition['start_time'];
    }
    if(array_key_exists('end_time',$whereCondition)){
        $end_time=$whereCondition['end_time'];
    }
  
    $rows=CameraTransaction::select('id',"created_at",'current_occupancy','zone_total_enter','occupancy_violation','enter')->where($where)
        ->where(function($query) use ($id,$start_time,$end_time,$fetchField){
            if($id){
                $query->where('id','>',$id);
            }
            if($start_time){
                $query->where('created_at','>=',$start_time);
            }
            if($end_time){
                $query->where('created_at','<=',$end_time);
            }
            if($fetchField=="occupancy_violation"){
               // $query->where('occupancy_violation','=',1);
            }

     })->get();

    // })->get()->toArray();
    // echo "<pre/>";
    //    print_r($rows); exit; 
       $data=[];
       $last_id=0;
     
       if($rows){
           foreach($rows as $row){
               $created_at=$row->created_at;
               $last_id=$row->id; 
                if($created_at){
                    $value=$row->$fetchField;
                    if($fetchField=="occupancy_violation"){
                        if($value==2){
                            $value=1;
                        }
                    }
                   $data["$created_at"]=$value?$value:0;
               }

           }
       }
    //    print_r($data); exit; 
       //Zone update
       $currentTime=Common::currentTime();
       $updateArray=[]; 
       if($last_id){
        if($table=='zone' && $zoneOrdeviceId){
            if($fetchField=="current_occupancy"){
                $updateArray['occupancy_last_id']=$last_id;
                $updateArray['occupancy_last_time']=$currentTime;
            }  
            else if($fetchField=="zone_total_enter"){
                $updateArray['footfall_last_id']=$last_id;
                $updateArray['footfall_last_time']=$currentTime;
            }  
            else if($fetchField=="occupancy_violation"){
                $updateArray['violation_last_id']=$last_id;
                $updateArray['violation_last_time']=$currentTime;
            }  
            if(is_array($updateArray) && count($updateArray)){
                Zone::where('id',$zoneOrdeviceId)->update($updateArray);
            }
        }
        else if($table=='device'){
            $updateArray['ct_last_id']=$last_id;
            $updateArray['ct_last_time']=$currentTime;
            
            Camera::where('id',$zoneOrdeviceId)->update($updateArray);
        }

        }

    //   print_r($data); exit; 
     
        return $data; 
    
}   

    /**
     * Fetch current occupancy 
     */
    static function  fetchOccupancy($where){
        $zone_id=$where['zone_id'];
        if(empty($zone_id)) return; 
        
        $id=$where['id'];
        $start_time = $where['start_time'];
        $end_time = $where['end_time'];
       
        return CameraTransaction::select('id','current_occupancy','created_at')->where(['zone_id'=>$zone_id])
                                   ->where(function($query) use($id,$start_time,$end_time){
                                       if($id){
                                           $query->where('id','>',$id);
                                       }
                                       if($start_time){
                                           
                                           $query->where('created_at','>=',$start_time);
                                       }
                                       if($end_time){
                                        $query->where('created_at','<',$end_time);
                                        }
                                     })
                                    ->get();

        
    }

    /**
     * Fetch last foot fall
     */
    static function  fetchFootFall($where){
        $zone_id=$where['zone_id'];
        if(empty($zone_id)) return; 
        
        $id=$where['id'];
        $start_time = $where['start_time'];
        $end_time = $where['end_time'];
       
        return CameraTransaction::select('id','in','created_at')->where(['zone_id'=>$zone_id])
                                   ->where(function($query) use($id,$start_time,$end_time){
                                       if($id){
                                           $query->where('id','>',$id);
                                       }
                                       if($start_time){
                                           $query->where('created_at','>=',$start_time);
                                       }
                                       if($end_time){
                                         $query->where('created_at','<',$end_time);
                                        }
                                     })
                                     ->orderBy('id','desc')
                                    ->first();

        
    }
    /**
     * Fetch foot fall date
     */
    static function  fetchFootFallDate($zones,$selectedDate=null){
        if(!$selectedDate){
            $selectedDate=Common::currentDate();
        }
        $data=[];
       
        if(is_array($zones)){
            foreach($zones as $zone_id=>$zone){
            $zone_total_in=0;    
            $row=CameraTransaction::select('zone_total_in')->where(['zone_id'=>$zone_id])
                                   ->where(function($query) use($selectedDate){
                                       if($selectedDate){
                                         $query->whereDate('created_at','=',$selectedDate);
                                        }
                                     })
                                     ->orderBy('id','desc')
                                     ->first();
                                     if($row){
                                         $zone_total_in=$row->zone_total_in;
                                     }
                                    $data[$zone_id]=$zone_total_in;
                                    }
                                }
                                return $data; 

        
    }

    /**
     * Fetch foot fall by device and date
     */
    static function  fetchDeviceFootFallDate($deviceIds,$selectedDate=null){
        if(!$selectedDate){
            $selectedDate=Common::currentDate();
        }
        $data=[];
       
        if(is_array($deviceIds)){
            foreach($deviceIds as $device_id=>$device){
            $total_in=0;    
            $row=CameraTransaction::select('in')->where(['camera_id'=>$device_id])
                                   ->where(function($query) use($selectedDate){
                                       if($selectedDate){
                                         $query->whereDate('created_at','=',$selectedDate);
                                        }
                                     })
                                     ->orderBy('id','desc')
                                     ->first();
                                     if($row){
                                         $total_in=$row->in;
                                     }
                                    $data[$device_id]=$total_in;
                                    }
                                }
                                return $data; 

        
    }




}

