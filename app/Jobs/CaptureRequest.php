<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Helpers\Common;
use Log;

class CaptureRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(isset($this->data['action']) && ($this->data['action'] === "capture-request")) {
            $this->captureApiRequest($this->data);
        }
    }
    
    
    public function captureApiRequest($data)
    {
        Common::captureRequest($data['data']);
        Log::info("Job CaptureRequest executed");
        
    }
}
