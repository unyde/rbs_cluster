<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Common;
class AverageOccupancy extends Model
{
    use SoftDeletes;
    protected $table = 'average_occupancy';
    protected $fillable=['zone_id','start_time','end_time','average_occupancy','ct_last_id','status','total_occupancy','total_record'];

    /**
     * Create empty record for time interval
     */
    static function createAverageOccupancy($zone_id,$time_intervals){
        if(empty($zone_id)) return ;
        foreach($time_intervals as $time_interval){
            $start_time=$time_interval['start_time'];
            $end_time=$time_interval['end_time'];
            $row =AverageOccupancy::select('id')->where('zone_id',$zone_id)
                                    ->where('start_time','>=',$start_time)
                                    ->where('end_time',"<=",$end_time)
                                    ->exists();
                                    
            if($row){
                //
            }     
            else{
                $data=[];
                $data['zone_id']=$zone_id;
                $data['start_time']=$start_time;
                $data['end_time']=$end_time;
                $data['ct_last_id']=0;
                AverageOccupancy::create($data);
            }                   

        }
    }
}
