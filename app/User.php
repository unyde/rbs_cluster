<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Notifications\Notifiable;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;



class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use HasRoles;
    protected $table="user";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','company_id','branch_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    private function getCreatedAt() {
        return date('m/d/Y', strtotime($this->attributes['created_at']));
    }

    static function getUserInfo(){
        $user = \Auth::user();
        $company_id = $user->company_id;
        $branch_id = $user->branch_id;
        $companyListShow = $company_id? false:true;
        return ['company_id'=>$company_id,'branch_id'=>$branch_id,'companyListShow'=>$companyListShow];

    }
    
    


}
