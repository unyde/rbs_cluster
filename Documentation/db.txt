ALTER TABLE `cameras` CHANGE `zone_id` `zone_id` INT(11) NOT NULL;
ALTER TABLE `cameras` ADD INDEX( `zone_id`);
ALTER TABLE `camera_transactions` CHANGE `camera_id` `camera_id` INT(11) NOT NULL;
ALTER TABLE `camera_transactions` ADD INDEX(`camera_id`);
ALTER TABLE `camera_transactions` ADD INDEX(`created_at`);
ALTER TABLE `camera_transactions` CHANGE `zone_id` `zone_id` INT(11) NOT NULL;
ALTER TABLE `camera_transactions` ADD INDEX(`zone_id`);
ALTER TABLE `camera_transactions` ADD INDEX(`delta_in_out`);
ALTER TABLE `camera_transactions` CHANGE `device` `device` BIGINT NOT NULL;


Add Camera api
EXPLAIN select `id`, `zone_id`, `reboot_type` from `cameras` where `device_id` = 10000010 and `cameras`.`deleted_at` is null limit 1

EXPLAIN select `id` from `cameras` where `zone_id` = 11 and `id` <> 13 and `cameras`.`deleted_at` is null


[2021-07-13 00:43:38] local.DEBUG: select `cameras`.`id`, `zone_id`, `reboot_type`, `max_occupancy`, `zones`.`branch_id`, `start_time`, `end_time` from `cameras` inner join `zones` on `zone_id` = `zones`.`id` inner join `branches` on `zones`.`branch_id` = `branches`.`id` where `device_id` = 10000010 and `cameras`.`deleted_at` is null limit 1  
[2021-07-13 00:43:38] local.DEBUG: select `id` from `cameras` where `zone_id` = 11 and `id` <> 13 and `cameras`.`deleted_at` is null  
[2021-07-13 00:43:38] local.DEBUG: select `in`, `out` from `camera_transactions` where `camera_id` = 11 and `created_at` >= 2021-07-13 07:00:00 and `camera_transactions`.`deleted_at` is null order by `id` desc limit 1  
[2021-07-13 00:43:38] local.DEBUG: select `in`, `out` from `camera_transactions` where `camera_id` = 15 and `created_at` >= 2021-07-13 07:00:00 and `camera_transactions`.`deleted_at` is null order by `id` desc limit 1  
[2021-07-13 00:43:38] local.DEBUG: select `in`, `out` from `camera_transactions` where `camera_id` = 32 and `created_at` >= 2021-07-13 07:00:00 and `camera_transactions`.`deleted_at` is null order by `id` desc limit 1  
[2021-07-13 00:43:38] local.DEBUG: select sum(`delta_in_out`) as aggregate from `camera_transactions` where `zone_id` = 11 and `delta_in_out` < 0 and `created_at` >= 2021-07-13 07:00:00 and `camera_transactions`.`deleted_at` is null  
[2021-07-13 00:43:38] local.DEBUG: select `zone_total_in` from `camera_transactions` where `zone_id` = 11 and `created_at` >= 2021-07-13 07:00:00 and `camera_transactions`.`deleted_at` is null order by `id` desc limit 1  
[2021-07-13 00:43:38] local.DEBUG: select `in` from `camera_transactions` where (`device` = 10000010) and `camera_transactions`.`deleted_at` is null order by `id` desc limit 1  
[2021-07-13 00:43:38] local.DEBUG: insert into `camera_transactions` (`enter`, `in`, `out`, `current_occupancy`, `device`, `occupancy_violation`, `zone_id`, `camera_id`, `delta_in_out`, `zone_total_in`, `zone_total_enter`, `updated_at`, `created_at`) values (0, 20, 3, 17, 10000010, 1, 11, 13, 17, 20, 0, 2021-07-13 00:43:38, 2021-07-13 00:43:38)  

13/07/21
ALTER TABLE `branches` CHANGE `status` `status` TINYINT NOT NULL DEFAULT '1';
UPDATE `branches` SET `status` = '1'
ALTER TABLE `zones` ADD INDEX(`branch_id`);
