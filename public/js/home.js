// Enable pusher logging - don't include this in production
Pusher.logToConsole = false;
var pusher = new Pusher(PUSHER_APP_KEY, {
    cluster: "ap2",
});
var channel = pusher.subscribe("my-channel");
channel.bind("home", function (data) {
    // console.log("Pusher Data", data);
    let res = JSON.stringify(data);
    res = JSON.parse(res);
    // console.log("res=", res);

    //Zone current occupancy update
    let zone_id = res["zone_id"];
    let selectedZoneId = document.getElementById("zone_id").value;
    // console.log("Test", selectedZoneId);
    if (selectedZoneId) {
        if (selectedZoneId == zone_id) {
            // document.getElementById("occupany-count").innerHTML =
            //     res.current_occupancy;
            // console.log("Yes working ");
            $(".occupany-count").html(res.current_occupancy);
            document.getElementById("foot-fall-count").innerHTML =
                res.totalFootFall;
            document.getElementById("occupancy-violation").innerHTML =
                res.occupancyViolation;

            //Occupancy chart
            let occupanyGraphData = res.occupanyChartData;
            displayWaterMark("watermark-1", occupanyGraphData);
            occupancyChart.data.datasets.forEach((dataset) => {
                occupanyGraphData.forEach((element, index) => {
                    if (dataset.data[index] != element) {
                        dataset.data[index] = element;
                    }
                });
                occupancyChart.update();
            });

            //Foot Fall chart
            let footFallGraphData = res.footFallChartData;
            displayWaterMark("watermark-2", footFallGraphData);
            footFallChart.data.datasets.forEach((dataset) => {
                footFallGraphData.forEach((element, index) => {
                    dataset.data[index] = element;
                });
                footFallChart.update();
            });

            //Violation chart
            let oVGraphData = res.violationChartData;
            displayWaterMark("watermark-3", oVGraphData);
            oVChart.data.datasets.forEach((dataset) => {
                if (Array.isArray(oVGraphData)) {
                    oVGraphData.forEach((element, index) => {
                        dataset.data[index] = element;
                    });
                    oVChart.update();
                }
            });
        }
    }
});
