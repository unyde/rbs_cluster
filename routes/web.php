<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return redirect('login');

});

Auth::routes();
Route::group(['middleware' => ['auth']], function() {

    Route::resource('roles','RoleController');    
    Route::get('/home', 'HomeController@index');
    Route::get('/change-password','UsersController@changePassword');
    Route::post('/change-password','UsersController@postChangePassword');
    Route::resource('/users','UsersController');
    Route::resource('/company','CompanyController');
    Route::get('/branchList/{companyId}','BranchController@branchList');
    Route::resource('/branch','BranchController');
    Route::resource('/tag-user','TagUserController');
    Route::resource('/zone','ZoneController');
    Route::get('/zoneList/{branchId}','ZoneController@zoneList');
    Route::get('/current-occupancy','CameraTransactionController@currentOccupancy');
    Route::get('/foot-fall','CameraTransactionController@footFall');
    Route::get('/violation','CameraTransactionController@violation');
    Route::post('/postzonesanitisation','HomeController@postZoneSanitisation');
    Route::get('/zone-notification','ZoneController@zoneNotification');

    #Route::get('/current-occupany','ZoneController@currentOccupany');
//currentOccupany
    Route::get('/device','CameraController@device');
    Route::get('/foot-fall-device/{id}','CameraTransactionController@footFallDevice')->name('foot_by_device');
    Route::resource('/camera','CameraController');
    Route::get('getZoneOccupancy','CameraTransactionController@getZoneOccupancy');
    Route::get('home-page','HomeController@homePage')->name('home');
    Route::get('chart_file_content','CameraTransactionController@chartFileContent')->name('chart_content');


});


