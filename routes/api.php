<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => 'apiLog'], function () {
Route::post('/login', 'Api\TagUserController@login');
Route::post('/camera', 'Api\TagUserController@camera');
Route::get('/addCameraTransaction', 'Api\CameraTransactionController@addCameraTransaction');
Route::any('/getDeviceRebootStatus', 'Api\CameraTransactionController@getDeviceRebootStatus');
Route::any('/updateDeviceRebootStatus', 'Api\CameraTransactionController@updateDeviceRebootStatus');
Route::any('/resetCamera', 'Api\CameraTransactionController@resetCamera');
Route::any('/updateCamera', 'Api\CameraTransactionController@updateCamera');
Route::any('/chart_file_create', 'Api\CameraTransactionController@chartFileCreate');

/**
 * File write 
 */
Route::get('/cron/zone_file_write/{file_type}', 'Api\CameraTransactionController@zoneFileWrite');
Route::get('/cron/device_file_write', 'Api\CameraTransactionController@deviceFileWrite');
Route::get('zone_foot_fall','Api\CameraTransactionController@zone_foot_fall')->name('zone_foot_fall');
Route::get('device_foot_fall','Api\CameraTransactionController@device_foot_fall')->name('device_foot_fall');
    
    


});




