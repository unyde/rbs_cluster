# Occupany

> ### Occupancy

This repo is functionality complete — Occupancy.

5 Branches in this repo.

1.development - Use this branch for local running project

2.staging - Use this branch to upload your code to the staging server.

3.production - Use this branch to upload your code to the production server.

4.local - Use this branch to testing any code in your local system

5.master - Merge final code in this branch

---

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)

Alternative installation is possible

Clone the repository

    git clone https://username@bitbucket.org/unyde/safetyoccupancy_portal.git

Switch to the repo folder

    cd safetyoccupancy_portal

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone https://username@bitbucket.org/unyde/safetyoccupancy_portal.git
    cd safetyoccupancy_portal
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan serve

The api can be accessed at [http://localhost:8000/api](http://localhost:8000/api).

---

# Code overview

## Folders

## Environment variables

-   `.env` - Environment variables can be set in this file

**_Note_** : You can quickly set the database information and other variables in this file and have the application fully working.

---

# Testing API

Run the laravel development server

    php artisan serve

The api can now be accessed at

    http://localhost:8000/api

Request headers

| **Required** | **Key**          | **Value**        |
| ------------ | ---------------- | ---------------- |
| Yes          | Content-Type     | application/json |
| Yes          | X-Requested-With | XMLHttpRequest   |

Refer the [api specification](#api-specification) for more info.

---

# Authentication

---

# Cross-Origin Resource Sharing (CORS)
