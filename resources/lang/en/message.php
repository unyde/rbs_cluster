<?php
return [
    'add' => ':name has been created successfully.',
	'update' => ':name has been updated successfully.',
	'delete' => ':name has been deleted successfully.',
	'status' => 'Status has been updated successfully.',
	'add_new' => 'Add New :title',
	'try_again' => 'Please try again.',
	'exist' => 'Already exist.',
	'no_record'=>'No record found.',
	'validation'=>'Validation is failed.'
];