@extends('layouts.admin')
@section('content')
    <script src="{{ asset('js/home.js') }}?t=<?php echo time(); ?>" defer></script>

    <script>
        const PUSHER_APP_KEY = "{{ Config::get('app.pusher_app_key') }}";
    </script>

    <?php if ($selectedDate) {
    $selectedDate = date('d-m-Y', strtotime($selectedDate));
    } ?>

    <div class="row">
        <div class="col-12">
            <form action="" onchange="" method="get" class="dashboard-form">

                <div class="row">
                    <div class="col-6 col-md-2 pr-0">
                        <h6>Selected Zone</h6>
                        @if ($zones)
                            <input type="hidden" value="{{ $selectedZone }}" id="zone_id" name="zone_id">
                            <h6>{{ $zones[$selectedZone] }}</h6>
                        @endif
                        {{-- {!! Form::select('zone_id', $zones, $selectedZone, ['placeholder' => 'Select Zone', 'class' => 'form-control border', 'onchange' => 'this.form.submit()', 'id' => 'zone_id']) !!} --}}
                    </div>
                    <div class=" col-6  col-md-2 pl-0 margin-left">

                        <h6>Date</h6>
                        <h5>{{ $selectedDate }}</h5>
                        {{-- <input type="text" value="{{ $selectedDate }}" name="filter_date" class="form-control datepicker"
                            placeholder="MM/DD/YY"> --}}
                    </div>
                    {{-- <div class="form-group col-md-2 col-sm-6">
                        <label for="">&nbsp;</label>
                        <input type="submit" class="btn btn-primary form-control" value="Go">
                    </div> --}}
                </div>
            </form>
        </div>
    </div><!-- ./ end top section -->

    <!-- Second section -->

    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
        <div class="col-md-4 col-sm-12">
            <div class="card panel-box radius-10 border-start border-0 border-3 border-info">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>


                            <p class="mb-0 text-secondary">Current Occupancy</p>
                            <h4 class="occupany-count" class="my-1 text-info">{{ $occupancyCount }}</h4>

                            <p class="mb-0 font-13">
                                <a href="current-occupancy?filter_date={{ $selectedDate }}">View more</a>
                            </p>
                        </div>
                        <div class="widgets-icons bg-info rounded-circle bg-gradient-scooter text-white ms-auto"><i
                                class="fas fa-users" style="font-size:18px;margin:14px 0"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="card panel-box radius-10 border-start border-0 border-3 border-success">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <h3 class="mb-0 text-secondary card-heading3">Foot Fall</h3>
                            <h4 class="my-1 text-success" id="foot-fall-count">{{ $footFallCount }}</h4>
                            <p class="mb-0 font-13">
                                <a href="foot-fall?filter_date={{ $selectedDate }}">View more</a>
                            </p>
                        </div>
                        <div class="widgets-icons bg-success rounded-circle bg-gradient-bloody text-white ms-auto"><i
                                class="fas fa-walking" style="font-size:18px;margin:14px 0"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="card panel-box radius-10 border-start border-0 border-3 border-danger">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <h3 class="mb-0 text-secondary card-heading3">Occupancy Violation
                            </h3>
                            <h4 class="my-1 text-danger" id="occupancy-violation">{{ $occupancyViolationCount }}</h4>
                            <p class="mb-0 font-13">
                                <a href="violation?filter_date={{ $selectedDate }}">View more</a>
                                | <a href="{{ route('zone.edit', $selectedZone) }}">Set Threshold</a>
                            </p>
                        </div>
                        <div class="widgets-icons bg-danger rounded-circle bg-gradient-ohhappiness text-white ms-auto"><i
                                class="fas fa-users-slash" style="font-size:18px;margin:14px 0"></i>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col">
            <div class="card radius-10 border-start border-0 border-3 border-warning">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="mb-0 text-secondary">Zones</p>
                            <h4 class="my-1 text-warning"><?php echo $zones->count(); ?>
                            </h4>
                            <p class="mb-0 font-13"> &nbsp; </p>
                        </div>
                        <div class="widgets-icons bg-warning rounded-circle bg-gradient-blooker text-white ms-auto">
                            <i class="fas fa-chart-area" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>

    <!-- //end second section -->

    <!-- Chart section -->
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Occupancy Overview</h6>
                </div>
                <div class="card-body">
                    <div class="text-center mt-2">
                        <h6>Current Occupancy: <span class="occupany-count">{{ $occupancyCount }}</span> </h6>
                    </div>
                    <div class="chart-container-1">

                        <canvas id="occupancy-chart"></canvas>
                        <div id="watermark-1" class="watermark none">No Data</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Foot Fall Chart Section -->
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box">
                <div class="card-header">
                    <h6>Foot Fall Overview</h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        <canvas id="foot-fall-chart"></canvas>
                        <div id="watermark-2" class="watermark none">No Data</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box">
                <div class="card-header">
                    <h6>Occupancy Violation Overview</h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        <canvas id="occupancy-violation-chart"></canvas>
                        <div id="watermark-3" class="watermark none">No Data</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let labelOption = [];
        let chartData = [];
        let occupancyChartWatermark = true;
        <?php
            $slots = $intervalBreakData['slots'];

            if ($slots) {
                foreach ($slots as $key => $interval) { ?>
        labelOption.push('<?php echo $interval; ?>');
        <?php }
            }
            if ($occupanyChartData) {
                foreach ($occupanyChartData as $key => $itemCount) { ?>
        chartData.push('<?php echo $itemCount; ?>');


        <?php }
            }
            ?>
        console.log(labelOption);
        displayWaterMark('watermark-1', chartData);
        window.onload = function() {
            occupancyChartEl = document.getElementById("occupancy-chart").getContext("2d");
            occupancyChart = new Chart(occupancyChartEl, {
                type: 'bar',
                data: {
                    labels: labelOption,
                    datasets: [{
                        label: "Occupancy",
                        backgroundColor: `${CHART_COLOR}`,
                        data: chartData,
                        // barPercentage: 0.5,
                        // barThickness: 6,
                        // maxBarThickness: 8,
                        // minBarLength: 2,

                    }]
                },
                options: chartOptions
            });


            let footFallOptions = {
                ...chartOptions
            };
            footFallOptions.scales.yAxes[0].scaleLabel.labelString = "Foot Fall";
            let footFallChartData = [];
            <?php if ($footFallChartData) {
                    foreach ($footFallChartData as $key => $itemCount) { ?>
            footFallChartData.push('<?php echo $itemCount; ?>');
            <?php }
                } ?>
            displayWaterMark('watermark-2', footFallChartData);
            footFallChartEl = document.getElementById("foot-fall-chart").getContext("2d");
            footFallChart = new Chart(footFallChartEl, {
                type: 'bar',
                data: {
                    labels: labelOption,
                    datasets: [{
                        label: "Foot Fall",
                        backgroundColor: `${CHART_COLOR}`,
                        data: footFallChartData
                    }]
                },
                options: chartOptions
            });

            let occupancyViolationOptions = {
                ...chartOptions
            };
            occupancyViolationOptions.scales.yAxes[0].scaleLabel.labelString = "Occupancy Violation";
            let occupancyViolationChartData = [];
            <?php if ($occupancyViolationChartData) {
                    foreach ($occupancyViolationChartData as $key => $itemCount) { ?>
            occupancyViolationChartData.push('<?php echo $itemCount; ?>');
            <?php }
                } ?>
            displayWaterMark('watermark-3', occupancyViolationChartData);
            oVChartEl = document.getElementById("occupancy-violation-chart").getContext("2d");
            oVChart = new Chart(oVChartEl, {
                type: 'bar',
                data: {
                    labels: labelOption,
                    datasets: [{
                        label: "Occupancy Violation",
                        backgroundColor: `${CHART_COLOR}`,
                        data: occupancyViolationChartData
                    }]
                },
                options: chartOptions
            });


        };
    </script>




@endsection
