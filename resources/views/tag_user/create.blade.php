@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title padleft20">Add New App User</div>
        </div>
        <div class="col-6">
            <div class="btn">
                <a href="{{ url('tag') }}" class="back btn-primary btn no-radius ">Back </a>
            </div>
        </div>

        <div class="col-12">
            <div class="bottom-border"></div>
        </div>


        @if (count($errors) > 0)
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

        @endif


    </div>
    <!--/.row-->
    <div class="card">
        <div class="card-body">


            {!! Form::open(['route' => 'tag-user.store', 'method' => 'POST', 'class' => 'form']) !!}
            <div class="row">
                @if ($userInfo['companyListShow'])
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <Label class="req">Company List</Label>

                            {!! Form::select('company_id', $companyList, null, ['placeholder' => 'Select Company', 'class'
                            => 'form-control']) !!}
                        </div>
                    </div>
                @else
                    {!! Form::hidden('company_id', $userInfo['company_id'], ['placeholder' => '', 'class' =>
                    'form-control']) !!}

                @endif
                @if (!$userInfo['branch_id'])
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <Label class="req">Branch Name</Label>
                            {!! Form::select('branch_id',$branches, null, ['placeholder' => 'Select Branch', 'class' =>
                            'form-control']) !!}
                        </div>
                    </div>
                @else
                    {!! Form::hidden('branch_id', $userInfo['branch_id'], ['placeholder' => '', 'class' => 'form-control'])
                    !!}
                @endif



                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <Label class="req">Login ID</Label>
                        {!! Form::text('username', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <Label class="req">Password</Label>
                        {!! Form::text('password', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                    </div>
                </div>
              




            
            </div>




            <div class="row">
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-block">Add</button>

                </div>
            </div>

            <script>
                $(document).ready(function() {


                    $('.form').validate({
                        rules: {
                            company_id: {
                                required: true
                            },
                            branch_id: {
                                required: true
                            },
                            username: {
                                required: true
                            },
                            password: {
                                required: true,
                                minlength: 6,
                                maxlength: 15
                            }

                        }
                    });
                });

            </script>


            {!! Form::close() !!}
        </div>
    </div>

@endsection
