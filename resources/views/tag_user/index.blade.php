@extends('layouts.admin')

@section('content')
 
<div class="row">
  <div class="col-12"> <div class="page-title padleft20">App User List</div></div>
  @can('tag-user-create')
  <div class="col-6">
    <div class="btn">
          <a href="tag-user/create" class="add_new btn-primary btn no-radius ">Add New App User </a> 
      </div>
  </div>
  @endcan
  <div class="col-12"><div class="bottom-border"></div></div>
  <div class="col-md-12">
  @if ($message = Session::get('success'))
    <div class="alert alert-message alert-success">
        {{ $message }}
    </div>
@endif

  </div>

</div><!--/.row-->
<div class="card mt-3">
  <div class="card-body">
    <table class="table table-striped table-responsive1 mt-4">
        <thead>
          <tr>
            <th>Slno</th>
            <th>Company Name</th> 
            <th>Branch Name</th> 
            <th>Username</th> 
            <th>Date</th>
            <th>Action</th>
          </tr>

        </thead>
        <tbody>
        <input type="hidden" name="posted" value="1">
           
        @forelse($tag_users as $tag_user)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$tag_user->company_name}}</td>
              <td>{{$tag_user->branch_name}}</td>
              <td>{{$tag_user->username}}</td>
              <td>
              <?php
              $created_at=$tag_user->created_at;
              if($created_at){
                echo date('d M Y, h:i a',strtotime($created_at));
              }
              ?>
              </td>
              <td class="action">
              
              @can('tag-user-create')
              <a href="{{ route('tag-user.edit',$tag_user->id) }}">
<i class="fas fa-pencil"></i> Edit
</a>&nbsp;&nbsp;&nbsp;
@endcan

@can('tag-user-delete')


<!-- Delete action -->
<a class="delete-icon" data-toggle="modal" data-target="#delete<?php echo $tag_user->id; ?>" ></a>
<div class="modal fade pp-modal" id="delete<?php echo $tag_user->id; ?>" tabindex="-1" role="dialog" aria-labelledby="ppModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
      	<div class="pp-icon"><i class="fas fa-question-square"></i></div>
        <h5 class="modal-title">Confirm</h5>
       <p>Are you sure, You want to delete <br> this record.</p>
      </div>
      <div class="modal-footer">
      	<div class="btn-list" style="display:block;">
        {!! Form::open(['method' => 'DELETE','class'=>"delete",'route' => ['tag-user.destroy', $tag_user->id]]) !!}
{!! Form::submit('Yes, Delete', ['class' => 'btn btn-danger ok']) !!}
{!! Form::close() !!} 

          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	    </div>    
      </div>
    </div>
  </div>
</div>
<!-- End delete action  -->
@endcan

                
              </td>
              
              </tr>

              @empty
              <tr>
              <td colspan="7">
                <p class="text-center text-danger">No record found.</p>
              </td>
              </tr>
@endforelse

          
        </tbody>
      </table>
    

 
</div>
</div>

@endsection
