@extends('layouts.admin')

@section('content')
 
<div class="row">
  <div class="col-12"> <div class="page-title padleft20">Change Password</div></div>
  <!-- <div class="col-6">
    <div class="btn">
          <a href="{{url('users')}}" class="back btn-primary btn no-radius ">Back </a> 
      </div>
  </div> -->

  <div class="col-12"><div class="bottom-border"></div></div>
    

@if (count($errors) > 0)
  <div class="col-md-12">
  <div class="alert-message alert alert-danger">
    <Label>Whoops!</Label> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
  </div>
  
@endif

  
</div><!--/.row-->
<div class="card mt-3">
  <div class="card-body">


  {!! Form::open(array('url' => 'change-password','method'=>'POST','class'=>'form')) !!}

   <div class="row">    
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group @error('password') has-error @enderror ">
            <Label class="req">Password</Label>
            {!! Form::password('password', array('id'=>'password','placeholder' => 'Password','class' => 'form-control')) !!}
              @error('password')
    		<span class="help-block">{{ $message }}</span> 
			@enderror
            
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group @error('confirm-password') has-error @enderror ">
            <Label class="req">Confirm Password</Label>
            {!! Form::password('confirm-password', array('id'=>'confirm-password','placeholder' => 'Confirm Password','class' => 'form-control')) !!}
            @error('confirm-password')
    		<span class="help-block">{{ $message }}</span> 
			@enderror
            
        </div>
    </div>
</div>



<div class="row">
	<div class="col-md-2">
        <button type="submit" class="btn btn-primary btn-block">Change Password</button>
    
    </div>
</div>

<script>
    $(document).ready(function () {
    
      

	$('.form').validate({ 
        rules: {
			password: {
                required: true,
                minlength:6,
                maxlength:15
            },
			'confirm-password': {
                required: true,
                equalTo: "#password"
            }
	    }
    });
});
</script>


{!! Form::close() !!}
</div>
</div>

@endsection
