@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title ">Branch</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ route('branch.index') }}" class=" btn-primary btn ">Back </a>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif



    <!--/.row-->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Edit Branch</h6>
                </div>
                <div class="card-body">

                    {!! Form::model($branch, ['method' => 'PATCH', 'autocomplete' => 'off', 'route' => ['branch.update', $branch->id], 'class' => 'form']) !!}
                    <div class="row">
                        @if ($userInfo['companyListShow'])
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <Label class="req">Company List</Label>

                                    {!! Form::select('company_id', $companyList, null, ['placeholder' => 'Select Company', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        @else
                            {!! Form::hidden('company_id', $userInfo['company_id'], ['placeholder' => '', 'class' => 'form-control']) !!}

                        @endif
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Branch Name</Label>
                                {!! Form::text('branch_name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Contact Number</Label>
                                {!! Form::number('contact_number', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Mobile</Label>
                                {!! Form::number('mobile', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Email</Label>
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">

                                <Label class="req">Open Time</Label>
                                {!! Form::text('start_time', null, ['class' => 'timepicker form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Close Time</Label>
                                {!! Form::text('end_time', null, ['class' => 'timepicker form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Branch Address</Label>
                                {!! Form::textarea('branch_address', null, ['rows' => 4, 'class' => 'form-control']) !!}


                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.form').validate({
                rules: {
                    company_id: {
                        required: true
                    },
                    branch_name: {
                        required: true
                    }

                }
            });
        });
    </script>


@endsection
