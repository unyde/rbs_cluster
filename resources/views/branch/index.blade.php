@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title ">Branch</h5>
        </div>
        @can('branch-create')
            <div class="col-2 text-right">
                <a href="branch/create" class=" btn-primary btn ">Add New Branch </a>
            </div>
        @endcan
    </div>
    <!--/.row-->

    @if ($message = Session::get('success'))
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert alert-message alert-success">
                    {{ $message }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">

        <div class="col-12">
            <form action="" method="get" autocomplete="off">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-3">
                                {!! Form::select('company_id', $companyList, app('request')->input('company_id'), ['placeholder' => 'Select Company', 'class' => 'form-control']) !!}

                            </div>


                            <div class="col-sm-3">
                                <input type="text" value="{{ app('request')->input('q') }}" class="form-control" name="q"
                                    placeholder="Search by name" autocomplete="off">
                            </div>

                            <div class="col-sm-3">
                                <input type="submit" value="Search" class="btn btn-primary full-width">

                            </div>

                        </div>
                        <!--/.row-->
                    </div>
                    <!--/./col-6-->

                </div>
                <!--/.row-->
            </form>
        </div>
        <!--/.col-12-->
    </div>




    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Branch List</h6>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Slno</th>
                                <th>Company Name</th>
                                <th>Branch Name</th>
                                <th>Address</th>
                                <th>Open Time</th>
                                <th>Close Time</th>

                                <th>Contact No.</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <input type="hidden" name="posted" value="1">

                            @forelse($branches as $branch)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $branch->company_name }}</td>
                                    <td>{{ $branch->branch_name }}</td>
                                    <td>{{ $branch->branch_address }}</td>
                                    <td>{{ $branch->start_time }}</td>
                                    <td>{{ $branch->end_time }}</td>
                                    <td>{{ $branch->contact_number }}</td>
                                    <td>
                                        <?php
                                        $created_at = $branch->created_at;
                                        if ($created_at) {
                                        echo date('d M Y, h:i a', strtotime($created_at));
                                        }
                                        ?>
                                    </td>
                                    <td class="action">

                                        @can('branch-create')
                                            <a href="{{ route('branch.edit', $branch->id) }}">
                                                <i class="fas fa-pencil"></i> Edit
                                            </a>&nbsp;&nbsp;&nbsp;
                                        @endcan

                                        @can('branch-delete')


                                            <!-- Delete action -->
                                            <a class="delete-icon" data-toggle="modal"
                                                data-target="#delete<?php echo $branch->id; ?>"></a>
                                            <div class="modal fade pp-modal"
                                                id="delete<?php echo $branch->id; ?>"
                                                tabindex="-1" role="dialog" aria-labelledby="ppModalCenterTitle"
                                                aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <div class="pp-icon"><i class="fas fa-question-square"></i></div>
                                                            <h5 class="modal-title">Confirm</h5>
                                                            <p>Are you sure, You want to delete <br> this record.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="btn-list" style="display:block;">
                                                                {!! Form::open(['method' => 'DELETE', 'class' => 'delete', 'route' => ['branch.destroy', $branch->id]]) !!}
                                                                {!! Form::submit('Yes, Delete', ['class' => 'btn btn-danger ok']) !!}
                                                                {!! Form::close() !!}

                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End delete action  -->
                                        @endcan


                                    </td>

                                </tr>

                            @empty
                                <tr>
                                    <td colspan="9">
                                        <p class="text-center text-danger">No record found.</p>
                                    </td>
                                </tr>
                            @endforelse


                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>


@endsection
