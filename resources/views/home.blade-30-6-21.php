@extends('layouts.admin')
@section('content')
{{-- <meta http-equiv="refresh" content="30"> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<script>
  // Enable pusher logging - don't include this in production
  Pusher.logToConsole = true;
  let company_id=<?php echo $company_id;?>;
  let branch_id=<?php echo $branch_id;?>;
  var pusher = new Pusher('{{  Config::get('app.pusher_app_key') }}', {
    cluster: 'ap2'
  });
  var channel = pusher.subscribe('my-channel');
  channel.bind('home', function(data) {
    console.log("Pusher Data",data);
    let res = JSON.stringify(data);
        res = JSON.parse(res);
        console.log('res',res);

    //Zone current occupancy update    
    let zone_id=res['zone_id'];
    let selectedZoneId = document.getElementById('zone_id');
    if(selectedZoneId){
      if(selectedZoneId.value==zone_id){
          document.getElementById('total-current-occupancy').innerHTML = res.current_occupancy;
          document.getElementById('totalFootFall').innerHTML     = res.totalFootFall;
          document.getElementById('yesterdayCount').innerHTML    = res.yesterdayCount;
          document.getElementById('yesterday_compare').style.color = res.yesterdayColor;
          document.getElementById('fallInWeek').innerHTML   = res.weekCount;
          document.getElementById('week_compare').style.color = res.weekColor;
          document.getElementById('fallInMonth').innerHTML  = res.monthCount;
          document.getElementById('month_compare').style.color = res.monthColor;
          document.getElementById('occupancyViolation').innerHTML = res.occupancyViolation;
      }
    }
        let device_company_id=res.company_id;
        let device_branch_id=res.branch_id;
        if(device_company_id==company_id && device_branch_id==branch_id){
          //document.getElementById('current_occupancy').innerHTML = res.current_occupancy;
          // document.getElementById('totalFootFall').innerHTML     = res.totalFootFall;
          // document.getElementById('yesterdayCount').innerHTML    = res.yesterdayCount;
          // document.getElementById('yesterday_compare').style.color = res.yesterdayColor;
          

          // document.getElementById('fallInWeek').innerHTML   = res.weekCount;
          // document.getElementById('week_compare').style.color = res.weekColor;
          
          // document.getElementById('fallInMonth').innerHTML  = res.monthCount;
          // document.getElementById('month_compare').style.color = res.monthColor;

          
          // document.getElementById('occupancyViolation').innerHTML = res.occupancyViolation;
        
        }
  });
</script>



<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12 pos-relative">
        <div class="line-animation"><img src="{{asset('img/beats_shapee.gif')}}" alt="line"/></div>
      </div>
    </div><!--/.row-->
    <div class="row mg-bottom5">
      <div class="col-8 col-sm-10">
        <div class="name text-left">
          <div class="hd-with-filter">
            <h1 class="pull-left">{{$companyName}}</h1>
            <form>
            <div class="filter-date">
              <div class="pull-left filter-input">
                <i class="far fa-calendar date-icon"></i>
               <?php
               $selected_date=$currentDate;
               if($selected_date){
                 $selected_date=date('d-m-Y',strtotime($selected_date));
               }
               ?> 
               <input type="text" value="{{$selected_date}}" name="selected_date" class="form-control datepicker" placeholder="MM/DD/YY">
             </div>
             <div class="pull-left filter-btn">
              <input type="submit" value="Go" class="btn btn-primary no-radius">
            </div>
            </div><!--/.filter-date-->
            </form>

            </div>     
          <p style="clear:both;float:none;width:100%">Dashboard</p>
        </div>
      </div>
      {{-- <div class="col-4 col-sm-2 text-right">
        <div class="active-number"><span class="active-number-text">0</span> <br> Active</div>
      </div> --}}
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-4 col-sm-12">
        <div class="content-box box-radius white-bg">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <h3 class="box-title name_of_zone">Name of Zone</h3>
                 <div class="row">
                   <div class="col-md-6">
                   <form action="" onchange="" method="get">
                    {!! Form::select('zone_id', $zones, $zone_id, ['placeholder' => 'Select Zone', 'class' =>
        'form-control border','onchange'=>"this.form.submit()",'id'=>'zone_id']) !!}
        </form>
              <?php 
              $max_occupancy="";
              if($zone['max_occupancy']){
                $max_occupancy=$zone['max_occupancy'];
              }
              ?>
              @if($max_occupancy)
              <span class="occupancy_limit">Occupany Limit:{{$max_occupancy}}</span>
             @endif
        </div>
        <div class="col-md-6 text-right zone_graph">
          <?php
         
              $current=0;
              $colorCode="#008000";
              if($max_occupancy){
                $current=($currentOccupancyTotal/$max_occupancy)*100;
                
              }
              if($current>100){
                $colorCode="#FF0000";
              }
              ?>
              @if($max_occupancy)
              <div class="pie-chart-box">
                <div id="zone" class="chart chart_single" data-percent="{{$current}}">
                  <span class="zone_count">{{$current}}%</span>
                        </div>
              </div>
              @else
              <div class="pie-chart-box">
              </div>
              @endif
              
             
        </div>
                 </div>
              </div>
              
              <div class="col-md-6">
                <h2 class="box-title inline" id="current_occupancy">
                <p><span>Current:</span><span id="total-current-occupancy">{{$currentOccupancyTotal}}</span></p>

                </h2>
              </div>
              <div class="col-6 text-right">
                <a class="view-link" href="current-occupancy?filter_date={{$currentDate}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>

            </div>
          </div>  
        </div><!--/.content-box-->
      </div><!--/.com-md-4-->

      <script>
                            
                            $(document).ready(function(){
                            
                            $(".chart_list").easyPieChart({
                                    size:60,
                                    barColor : '#008000',
                                    scaleColor:false,
                                    lineWidth:6,
                                    trackColor:'#dcdcdc',
                                    lineCap:'circle',
                                    animate:1700
                                  });
                            
                                  $(".chart_single").easyPieChart({
                                    size:60,
                                    barColor : '<?php echo $colorCode; ?>',
                                    scaleColor:false,
                                    lineWidth:6,
                                    trackColor:'#dcdcdc',
                                    lineCap:'circle',
                                    animate:1700
                                  });
                                  
                                  
                            });
                            
                                                        </script>


      <div class="col-md-4 col-sm-12">
         <div class="content-box box-radius white-bg">
          <div class="container">
             <div class="row">
              <div class="col-9">
                <h3 class="box-title">Total FootFall</h3>
              </div>
              <div class="col-3">
                <div class="box-icon-img center">
                <span class="dash-icon">
                <i class="fas fa-walking" style="color:#006eff;font-size:18px;margin:14px 0"></i>
                <!-- 
                <i class="fas fa-walking"></i> -->
                </span>
                </div>
              </div>
              <div class="col-md-12">
              <h2 class="value-color value-font" id="totalFootFall">{{$totalFootFall}}</h2>
              </div>
              <div class="col-8">
               <!--  <p class="light-grey-color"><span class="highlight-font red-color">+ 0</span> than yesterday</p>   -->
               <ul class="list_inline">
                 <li class="green-color" style="font-size:24px;font-weight:normal;">-</li>
                 <?php
                  $yesterdayColor="#ff0000";
                  $yesterdayCount=0;
                  if($totalFootFallYesterday>$totalFootFall){
                    $yesterdayCount=$totalFootFallYesterday-$totalFootFall;
                  }
                  else {
                   
                    $yesterdayCount=$totalFootFall-$totalFootFallYesterday;
                    $yesterdayColor="#228B22";
                  }
                  //Week
                  $weekColor="#ff0000";
                  $weekCount=0;
                  if($totalFallInWeek>$totalFootFall){
                    $weekCount=$totalFallInWeek-$totalFootFall;
                  }
                  else {
                   
                    $weekCount=$totalFootFall-$totalFallInWeek;
                    $weekColor="#228B22";
                  }

                  //Month
                  $monthColor="#ff0000";
                  $monthCount=0;
                  if($totalFallInMonth>$totalFootFall){
                    $monthCount=$totalFallInMonth-$totalFootFall;
                  }
                  else {
                   
                    $monthCount=$totalFootFall-$totalFallInMonth;
                    $monthColor="#228B22";
                  }



                 ?>
                  <li id="yesterday_compare" style="color:{{$yesterdayColor}}"><span class="break" id="yesterdayCount">{{$yesterdayCount}}</span> Yesterday</li>
                  <li id="week_compare" style="color:{{$weekColor}}"  ><span class="break"   id="fallInWeek">{{$weekCount}}</span> Week</li>

                  <li id="month_compare" style="color:{{$monthColor}}" ><span class="break"  id="fallInMonth">{{$monthCount}}</span> Month</li>

               </ul>
              </div>
              <div class="col-4 text-right">
                <a class="view-link" style="margin:5px 0 0" href="foot-fall?filter_date={{$currentDate}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
            
            </div>
          </div>  
        </div><!--/.content-box-->
      </div><!--/.col-md-4-->
     <div class="col-md-4 col-sm-12">
         <div class="content-box box-radius white-bg">
          <div class="container">
             <div class="row">
              <div class="col-9">
                <h3 class="box-title">Occupancy Violation</h3>
              </div>
              <div class="col-3">
                <div class="box-icon-img center">
                <span class="dash-icon">
                <i class="fas fa-users-slash"  style="color:#006eff;font-size:18px;margin:14px 0"></i>
               <!-- 
                <i class="fas fa-walking"></i> -->
                </span>
                </div>
              </div>
              <div class="col-md-12">
              <h2 class="value-color value-font" id="occupancyViolation">{{$occupancyViolation}}</h2>
              </div>
              <div class="col-6">
                <!-- <p class="light-grey-color">Today</p>  --> 
              </div>
              <div class="col-6 text-right">
                <a class="view-link" href="violation?filter_date={{$currentDate}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>  
        </div><!--/.content-box-->
      </div><!--/.col-md-4-->
    </div><!--/.row-->
  </div><!--/.col-md-9-->
</div><!--/.row-->  
<div class="graph-row-list">
  <div class="row">
    <div class="col-md-6">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
         <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">Occupancy Violation Trend</h3>
            </div>
            <div class="col-7 text-right">
              <!-- <a class="view-link" href="#">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> -->
              <ul class="tab-list graph_tab_list footfall_trends">
                <li class="active" data-id="today-tab">Today</li>
                <li data-id="Week-tab">Week</li>  
                <li data-id="month-tab">Month</li>
              </ul><!--/.tab-list-->
            </div>
          </div><!--/.row-->
          <div class="graph-tab today-tab footfall_trends_graph_tab block">
            <canvas id="todaylinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('todaylinechart').getContext('2d');
              let todaySlot=[];
              let fallTrendToday=[];
              let fallTrendYesterday=[];
              let fallTrendLastMonth=[];
              let currentWeek=[];
              let lastWeek=[];
              
              <?php 
               forEach($timeSlots as $timeSlot){
                 ?>
                 todaySlot.push('<?php echo $timeSlot["label_text"] ?>');
                 <?php
               }
               $today=$fallTrends['today'];
               if(is_array($today)){
                 forEach($today['today'] as $key=>$item){
                  ?>
                  fallTrendToday.push('<?php echo $item; ?>');
                  fallTrendYesterday.push("<?php echo $today['yesterday'][$key]; ?>");
                  fallTrendLastMonth.push("<?php echo $today['lastMonth'][$key]; ?>");
                 <?php
                 }
               }

               //Week 
               forEach($weekData['weekDay'] as $key=>$item){
                ?>
                currentWeek.push("<?php echo $weekData['currentWeek'][$key]; ?>");
                lastWeek.push("<?php echo $weekData['lastWeek'][$key]; ?>");
                  <?php
               }
              ?>
              
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:todaySlot,
                  datasets:[
                  {
                    label: 'Today',
                    data: fallTrendToday,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Yesterday',
                    data: fallTrendYesterday,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Month Day',
                  //   data: fallTrendLastMonth,
                  //   fill:false,
                  //   borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false,
                              count:'number'
                           },
                           count:'number'
                        }]
                   }
                }
              });
            </script>
          </div>
           <div class="graph-tab Week-tab footfall_trends_graph_tab">
            <canvas id="weeklinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('weeklinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Sunday','Monday','Tuesday','Wenesday','Thursday','Friday','Saturaday'],
                  datasets:[
                     {
                    label: 'Current Week',
                    data: currentWeek,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Week',
                    data: lastWeek,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Month Week',
                  //   data: [0,20,15,40,30,35,10],
                  //   fill:false,
                  //   borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
          <div class="graph-tab month-tab footfall_trends_graph_tab">
            <canvas id="monthlinechart" class="none"></canvas>
            <script type="text/javascript">
              let currentMonth=[];
              let lastMonth=[];
              <?php 
              $currentMonths=$fallTrendMonth['currentMonth'];
              $lastMonths=$fallTrendMonth['lastMonth'];
              
              forEach($currentMonths as $currentMonth){
                 ?>
                 currentMonth.push('<?php echo $currentMonth ?>');
                 <?php
               } 
               
               forEach($lastMonths as $lastMonth){
                 ?>
                 lastMonth.push('<?php echo $lastMonth ?>');
                 <?php
               }

               ?>

              var linectx = document.getElementById('monthlinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Day-1','Day-2','Day-3','Day-4','Day-5','Day-6','Day-7','Day-8','Day-9','Day-10','Day-11','Day-12','Day-13','Day-14','Day-15','Day-16','Day-17','Day-18','Day-19','Day-20','Day-21','Day-22','Day-23','Day-24','Day-25','Day-26','Day-27','Day-28','Day-29','Day-30','Day-31'],
                  datasets:[
                  {
                    label: 'Cuurent Month',
                    data: currentMonth,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                   {
                    label: 'Last Month',
                    data: lastMonth,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Year Month',
                  //   data: [0,10,30,15,30,10,15,10,40,15,30,10,20,15,10,12,40,20,10,10,40,15,40,30,20,30,10,35,40,20],
                  //   fill:false,
                  //    borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
        </div>
      </div> 
      <script type="text/javascript">
        $(document).ready(function() {
          $(".footfall_trends li").click(function(){
            $(".footfall_trends li").removeClass('active');
            $(this).addClass('active');
            var tab = $(this).attr("data-id");
            $(".footfall_trends_graph_tab").removeClass("block");
            $("."+tab).addClass("block");
          });
        });
      </script>   
    </div><!--/.col-md-6-->
       <div class="col-md-6">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
         <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">AVG. TIME SPEND</h3>
            </div>
            <div class="col-7 text-right">
              <!-- <a class="view-link" href="#">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> -->
              <ul class="tab-list graph_tab_list avg_graph_tab_list">
                <li class="active" data-id="avg-today-tab">Today</li>
                <li data-id="avg-Week-tab">Week</li>
                <li data-id="avg-month-tab">Month</li>
              </ul><!--/.tab-list-->
            </div>
          </div><!--/.row-->
          <div class="graph-tab avg_graph_tab avg-today-tab block">
            <canvas id="avgtodaylinechart" class="none"></canvas>
            <script type="text/javascript">
              let todaySpendTime=[];
              let yesterdaySpendTime=[];
              let currentWeekSpendTime=[];
              let lastWeekSpendTime=[];
              let currentMonthSpendTime=[];
              let lastMonthSpendTime=[];
              <?php 
                $todaySpendTime=$timeSpend['today'];
                $yesterdaySpendTime=$timeSpend['yesterday'];
                $currentWeekSpendTime=$timeSpend['currentWeek'];
                $lastWeekSpendTime=$timeSpend['lastWeek'];
                $currentMonthSpendTime=$timeSpend['currentMonth'];
                $lastMonthSpendTime=$timeSpend['lastMonth'];
                forEach($todaySpendTime as $item){
                  ?>
                  todaySpendTime.push(<?php echo $item?>);
                  <?php
                }
                forEach($yesterdaySpendTime as $item){
                  ?>
                  yesterdaySpendTime.push(<?php echo $item?>);
                  <?php
                }
                forEach($currentWeekSpendTime as $item){
                  ?>
                  currentWeekSpendTime.push(<?php echo $item?>);
                  <?php
                }
                forEach($lastWeekSpendTime as $item){
                  ?>
                  lastWeekSpendTime.push(<?php echo $item?>);
                  <?php
                }
                forEach($currentMonthSpendTime as $item){
                  ?>
                  currentMonthSpendTime.push(<?php echo $item?>);
                  <?php
                }
                forEach($lastMonthSpendTime as $item){
                  ?>
                  lastMonthSpendTime.push(<?php echo $item?>);
                  <?php
                }
                

              ?>
              var linectx = document.getElementById('avgtodaylinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:todaySlot,
                  datasets:[
                  {
                    label: 'Today',
                    data: todaySpendTime,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Yesterday',
                    data: yesterdaySpendTime,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Month Day',
                  //   data: [0,20,15,40,30,35],
                  //   fill:false,
                  //   borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
           <div class="graph-tab avg-Week-tab avg_graph_tab">
            <canvas id="avgweeklinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('avgweeklinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Monday','Tuesday','Wenesday','Thursday','Friday','Saturaday','Sunday'],
                  datasets:[
                     {
                    label: 'Current Week',
                    data: currentWeekSpendTime,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Week',
                    data: lastWeekSpendTime,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Month Week',
                  //   data: [0,20,15,40,30,35,10],
                  //   fill:false,
                  //   borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
          <div class="graph-tab avg-month-tab avg_graph_tab">
            <canvas id="avgmonthlinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('avgmonthlinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Day-1','Day-2','Day-3','Day-4','Day-5','Day-6','Day-7','Day-8','Day-9','Day-10','Day-11','Day-12','Day-13','Day-14','Day-15','Day-16','Day-17','Day-18','Day-19','Day-20','Day-21','Day-22','Day-23','Day-24','Day-25','Day-26','Day-27','Day-28','Day-29','Day-30','Day-31'],
                  datasets:[
                  {
                    label: 'Cuurent Month',
                    data: currentMonthSpendTime,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                   {
                    label: 'Last Month',
                    data: lastMonthSpendTime,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Year Month',
                  //   data: [0,10,30,15,30,10,15,10,40,15,30,10,20,15,10,12,40,20,10,10,40,15,40,30,20,30,10,35,40,20],
                  //   fill:false,
                  //    borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
        </div>
      </div> 
      <script type="text/javascript">
        $(document).ready(function() {
          
          $(".avg_graph_tab_list li").click(function(){
            $(".avg_graph_tab_list li").removeClass('active');
            $(this).addClass('active');
            var tab = $(this).attr("data-id");
            $(".avg_graph_tab").removeClass("block");
            $("."+tab).addClass("block");
          });

          $(".violation_graph_tab_list li").click(function(){
            $(".violation_graph_tab_list li").removeClass('active');
            $(this).addClass('active');
            var tab = $(this).attr("data-id");
            $(".violation_graph_tab").removeClass("block");
            $("."+tab).addClass("block");
          });

          $(".sanitisation_tab_list li").click(function(){
            $(".sanitisation_tab_list li").removeClass('active');
            $(this).addClass('active');
            var tab = $(this).attr("data-id");
            $(".tab_group").removeClass("block");
            $("."+tab).addClass("block");
          });



        });
      </script>   
    </div><!--/.col-md-6-->

     <div class="col-md-6" >
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
           <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">Occupancy Violation</h3>
            </div>
            <div class="col-7 text-right">
              <ul class="tab-list graph_tab_list violation_graph_tab_list">
                <li class="active" data-id="violation-today-tab">Today</li>
                <li data-id="violation-Week-tab">Week</li>
                <li data-id="violation-month-tab">Month</li>
              </ul><!--/.tab-list-->
            </div>
          </div><!--/.row-->
          <div class="graph-tab violation_graph_tab violation-today-tab block">
    <canvas id="violationtodaylinechart" class="none"></canvas>
    <script type="text/javascript">
      let todayViolation=[];
      let yesterdayViolation=[];

      <?php 
      $todayViolation=$occupancyGraphData['today'];
      $yesterdayViolation=$occupancyGraphData['yesterday'];
      forEach($todayViolation as $item){
      ?>
      todayViolation.push(<?php echo $item?>);
      <?php
      }
      forEach($yesterdayViolation as $item){
      ?>
      yesterdayViolation.push(<?php echo $item?>);
      <?php
      }

      ?>


         var linectx = document.getElementById('violationtodaylinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:todaySlot,
                  datasets:[
                  {
                    label: 'Today',
                    data: todayViolation,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Yesterday',
                    data: yesterdayViolation,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>


          <div class="graph-tab violation-Week-tab violation_graph_tab">
            <canvas id="violationweeklinechart" class="none"></canvas>
            <script type="text/javascript">
              let currentWeekViolation=[];
      let lastWeekViolation=[];

      <?php 
      $currentWeekViolation=$occupancyGraphData['currentWeek'];
      $lastWeekViolation=$occupancyGraphData['lastWeek'];
      forEach($currentWeekViolation as $item){
      ?>
      currentWeekViolation.push(<?php echo $item?>);
      <?php
      }
      forEach($lastWeekViolation as $item){
      ?>
      lastWeekViolation.push(<?php echo $item?>);
      <?php
      }

      ?>

              var linectx = document.getElementById('violationweeklinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Monday','Tuesday','Wenesday','Thursday','Friday','Saturaday','Sunday'],
                  datasets:[
                     {
                    label: 'Current Week',
                    data: currentWeekViolation,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Week',
                    data: lastWeekViolation,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Month Week',
                  //   data: [0,20,15,40,30,35,10],
                  //   fill:false,
                  //   borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
          <div class="graph-tab violation-month-tab violation_graph_tab">
            <canvas id="violationmonthlinechart" class="none"></canvas>
            <script type="text/javascript">
              let currentMonthViolation=[];
      let lastMonthViolation=[];

      <?php 
      $currentMonthViolation=$occupancyGraphData['currentMonth'];
      $lastMonthViolation=$occupancyGraphData['lastMonth'];
      forEach($currentMonthViolation as $item){
      ?>
      currentMonthViolation.push(<?php echo $item?>);
      <?php
      }
      forEach($lastMonthViolation as $item){
      ?>
      lastMonthViolation.push(<?php echo $item?>);
      <?php
      }

      ?>

              var linectx = document.getElementById('violationmonthlinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Day-1','Day-2','Day-3','Day-4','Day-5','Day-6','Day-7','Day-8','Day-9','Day-10','Day-11','Day-12','Day-13','Day-14','Day-15','Day-16','Day-17','Day-18','Day-19','Day-20','Day-21','Day-22','Day-23','Day-24','Day-25','Day-26','Day-27','Day-28','Day-29','Day-30','Day-31'],
                  datasets:[
                  {
                    label: 'Cuurent Month',
                    data: currentMonthViolation,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                   {
                    label: 'Last Month',
                    data: lastMonthViolation,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  // {
                  //   label: 'Last Year Month',
                  //   data: [0,10,30,15,30,10,15,10,40,15,30,10,20,15,10,12,40,20,10,10,40,15,40,30,20,30,10,35,40,20],
                  //   fill:false,
                  //    borderColor: 'rgb(244, 208, 63)',
                  //   tension: 0.1,
                  // },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                          ticks: {
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        },
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
          
            
        </div>
      </div>    
     </div><!--/.col-md-4-->
     <div class="col-md-6">
      <div class="content-box white-bg box-radius max-height sanitisationbox">
        <div class="container-fluid">
           <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">&nbsp;</h3>
            </div>
            <div class="col-7 text-right">
              <ul class="tab-list sanitisation_tab_list">
                <li class="active" data-id="sanitisation-tab">Sanitisation</li>
                <li data-id="repurpose-tab" class="">Repurpose</li>
              </ul><!--/.tab-list-->
            </div>
          </div>
              <div class=" tab_group sanitisation-tab block">
                <table class="table table-striped table-responsive1">
      <thead>
        <tr>
          <th>Sr.no</th>
          <th>Zone Name</th>
          <th>Notification Sent</th>
         
          <th>Last Sanitisation</th>
          <th>Action</th>
           </tr>
      </thead>
      <tbody>
      @forelse($sanitisationZones as $row)
      <tr>
        <td>1</td>
        <td>{{$row['zone_name']}}</td>
        <td>
          <?php 
          if($row['notification_sent']){
            $notification_sent=$row['notification_sent'];
            echo date('d-m-y h:i a',strtotime($notification_sent));
          }
          $zoneid=$row['id'];
          ?>
        </td>

        <td>
        <?php 
          $last_sanitisation="";
          if($row['last_sanitisation']){
            $last_sanitisation=$row['last_sanitisation'];
            echo date('d-m-y h:i a',strtotime($last_sanitisation));
          }
          ?>
        </td>
      
        <td>
        @if($last_sanitisation)
        <a href="{{url('zone-notification')}}">View</a>
        @else
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popup_{{$zoneid}}">
  Action
</button>
@endif

        <div class="modal fade" id="popup_{{$zoneid}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{url('postzonesanitisation')}}" method="post">
      @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Zone Sanitisation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body zone_popup">
      <input type="hidden" name="zone_id" value="{{$row['id']}}" id="">
         <input type="hidden" name="zone_notification_id" id="" value="{{$row['zone_notification_id']}}">
          <div class="form-group">
            <label for="">Remark</label>
            <textarea name="remark" id="" cols="3" rows="5" class="form-control"></textarea>
          </div>
          <div class="form-group">
          <label for="">Status</label>
          <select name="status" id="" class="form-control">
            <option value="1">Complete</option>
            <option value="0">Pending</option>
           
          </select>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form> 
    </div>
  </div>
</div>

        </td>
      </tr>

      @empty
              <tr>
              <td colspan="5">
                <p class="text-center text-danger">No record found.</p>
              </td>
              </tr>
@endforelse
@if($sanitisationZones)
 <tr>
   <td colspan="5" align="center">
   <a href="{{url('zone-notification')}}" class="link">View All</a>
   </td>
 </tr>
 @endif

      </tbody>
                </table>  
              </div>

              <div class=" tab_group repurpose-tab none">
                <table class="table table-striped table-responsive1 mt-4">
        <thead>
          <tr>
            <th>Slno</th>
            <th>Company Name</th> 
            <th>Branch Name</th> 
            <th>Zone Name</th> 
            <th>Max Occupany</th>
            <th>Occupancy</th> 
            {{-- <th>Date</th> --}}
          </tr>

        </thead>
        <tbody>
        @forelse($zoneOccupancies as $i=>$row)
        <?php
             $occupancy_percentage=$row['occupancy_percentage'];
              if($occupancy_percentage<70){ 
                 ?>
            <tr>
              <td>{{++$i}}</td>
              <td>{{$row['company_name']}}</td>
              <td>{{$row['branch_name']}}</td>
              <td>{{$row['zone_name']}}</td>
              <td>{{$row['max_occupancy']}}</td>
              <td>
              @if($row['max_occupancy']) 
              <div class="pie-chart-box">
                <div id="zone" class="chart chart_list" data-percent="{{$occupancy_percentage}}">
                  <span class="zone_count">{{$occupancy_percentage}}%</span>
                        </div>
              </div>
              @endif

              </td>
              
              
              </tr>
              <?php }?>
              @empty
              <tr>
              <td colspan="8">
                <p class="text-center text-danger">No record found.</p>
              </td>
              </tr>
@endforelse

          
        </tbody>
      </table>
    

              </div>
              


           <!--/.row-->
        </div></div>
  </div><!--/.row-->
</div><!--/.graph-row-list-->
<div class="graph-row-list">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
          <div class="row mg-bottom20">
            <div class="col-8">
              <h3 class="box-title inline">Device Status</h3>
              <div class="box-title2 inline blue-bg">
               <h4> {{$activeCameras->count()}} <span class="small-heading light-grey-color">Device</span></h4>
              </div>
            </div>
             <div class="col-4" style="width:100%">
              {{-- <div class="inline-width-text search-input last" style="width:100%">
              <input type="text" value="" class="form-control home_white_bg" name="search" placeholder="Device Id,Device Name,Area,office" autocomplete="off">
            </div> --}}
        </div>
          </div>
          <div class="home_box_table_scroll">
         <table class="table table-striped table-responsive1">
      <thead>
        <tr>
          <th>Sr.no</th>
          <th>Office Name</th>
          <th>Zone</th>
          <th>Device Name</th>
          <th>Device Id</th>
          <th>Status</th>
          <th>DateTime</th>
          <th>Device Health</th>
        </tr>
      </thead>
      <tbody>
      <form method="post">
      <input type="hidden" name="posted" value="1">
        <?php
        $activeLists=[];
        ?>
        @foreach ($activeCameras as $device)
        <tr>
          <td>1</td>
          <td>{{$device->branch_name}}</td>
           <td>{{$device->zone_name}}</td>
           <td>{{$device->device_name}}</td>
           <td>{{$device->device_id}}</td>
          
          <td>
          <?php 
            $created_at=$device->getDeviceStatus($currentDate,$device->device_id);
            
            ?>
           @if($created_at)
           Active
           @else 
           InActive
           @endif
            </td>
          <td>
            <?php
            
              if($created_at){
                echo $created_at;//date('d-m-Y h:i a',strtotime($created_at));
              }
              ?></td>
         
          <td>
            <span class="device_status"><i class="fas fa-fan blue-color"></i>
             60</span> <span class="device_status">
             <i class="fas fa-power-off text-info"></i> ON</span> 
             <span class="device_status"><i class="fas fa-wifi text-info"></i> NO</span> <span class="device_status"><i class="fas fa-thermometer-full blue-color"></i> 30 &#8451;</span>
          </td>
          </tr>
        @endforeach
        
       
         
            
        </form>
      </tbody>
    </table>
  </div>
  <div class="col-12 text-right" style="display: none">
      <a class="view-link" href="device-status-list.php">View More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
    </div>

        </div>
      </div>
    </div><!--/.col-md-12-->
  </div><!--/.row-->
</div><!--/.graph-row-list-->

@endsection
