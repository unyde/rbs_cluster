@extends('layouts.admin')

@section('content')
{{-- <meta http-equiv="refresh" content="30"> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script>
    <?php
    $currentDate =date('d-m-Y');
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12 pos-relative">
            <div class="line-animation">
               <div class="row">
                   <div class="col-md-2">
                    <span class="dashboard-title">Dashboard</span>   
                   </div>
                   <div class="col-md-10">
                    <img src="{{ asset('img/beats_shapee.gif')}}" alt="line"></div>

                   </div>
               </div>
          </div>
        </div><!--/.row-->
        <div class="row mg-bottom5">
          <div class="col-8 col-sm-10">
            <div class="name text-left">
              <div class="hd-with-filter">
                <h1 class="pull-left">{{$companyName}}</h1>
                <form class="invisible">
                <div class="filter-date">
                  <div class="pull-left filter-input">
                    <i class="far fa-calendar date-icon"></i>
                   <input type="text" value="{{$currentDate}}" name="selected_date" class="form-control datepicker" placeholder="DD-MM-YY">
                 </div>
                 <div class="pull-left filter-btn">
                  <input type="submit" value="Go" class="btn btn-primary no-radius">
                </div>
                </div><!--/.filter-date-->
                </form>
  
                </div>   
               
            </div>
          </div>
         
        </div><!--/.row-->
        <div class="row top-margin">
          <div class="col-md-3 col-sm-12">
            <div class="content-box box-radius white-bg" id="panic_alert_section">
              <div class="container">
                <div class="row">
                  <div class="col-9">
                    <h3 class="box-title">Office </h3>
                  </div>
                  <div class="col-3">
                    <div class="box-icon-img center">
                      <i class="fas fa-building"></i>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h2 class="value-color value-font">
                   {{$totalBranch}}               </h2>
                  </div>
                  <div class="col-6">
                    {{-- <p class="light-grey-color"><span class="highlight-font red-color">+ 0</span>than yesterday</p>   --}}
                  </div>
                  <div class="col-6 text-right">
                        <a class="view-link"  href="{{route('branch.index')}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                  </div>
                </div>
              </div>  
            </div><!--/.content-box-->
          </div><!--/.com-md-4-->
          <div class="col-md-3 col-sm-12">
             <div class="content-box box-radius white-bg">
              <div class="container">
  
                    
                <div class="row">
                  <div class="col-9">
                    <h3 class="box-title">Camera</h3>
                  </div>
                  <div class="col-3">
                    <div class="box-icon-img center">
                    <i class="fa fa-camera"></i>  
                    </div>
                  </div>
                  <div class="col-md-12">
                                  <h2  class="value-color value-font">{{$totalCamera}}</h2>
                  </div>
                  <div class="col-6">
                    {{-- <p class="light-grey-color"> Today</p>   --}}
                  </div>
                  <div class="col-6 text-right">
                    <a class="view-link" href="{{url('camera')}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                  </div>
                </div>
                      
  
  
              </div>  
            </div><!--/.content-box-->
          </div><!--/.col-md-4-->


          <div class="col-md-3 col-sm-12">
            <div class="content-box box-radius white-bg">
             <div class="container">
 
                   
               <div class="row">
                 <div class="col-9">
                   <h3 class="box-title">App User</h3>
                 </div>
                 <div class="col-3">
                   <div class="box-icon-img center">
                  <i class="fa fa-user"></i>   
                  </div>
                 </div>
                 <div class="col-md-12">
                                 <h2  class="value-color value-font">{{$totalAppUser}}</h2>
                 </div>
                 <div class="col-6">
                   {{-- <p class="light-grey-color"> Today</p>   --}}
                 </div>
                 <div class="col-6 text-right">
                   <a class="view-link link-update" href="{{url('tag-user')}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                 </div>
               </div>
                     
 
 
             </div>  
           </div><!--/.content-box-->
         </div><!--/.col-md-4-->

          <div class="col-md-3 col-sm-12">
             <div class="content-box box-radius blue-bg">
              <div class="container">
                <div class="tracing-box">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="img-content">
                        <div class="row">
                          <div class="col-6 col-sm-6"><img style="width:116px;margin-bottom:22px" src="{{ asset('img/dashboard/contact_tracing.png')}}" alt="Contact Tracing"></div>
                          <div class="col-6 col-sm-6 text-left">
                            <h2 style="margin:12px 0 0">Occupancy</h2>
                          </div>
                      </div>
                    </div>
                  </div>  
                    <div class="col-md-12 text-right"><a class="tracing-box-link view-link" href="#">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
                </div>  
                </div>
              </div>
            </div><!--/.content-box-->
          </div><!--/.col-md-4--->

          

        </div><!--/.row-->
      </div><!--/.col-md-9-->


    </div><!--/.row-->  

    <!-- section 2-->
      <div class="row">
        <div class="col-md-12 mb-2">
          <form action="" class="form-inline" method="get">
            <div class="form-group col-md-1 pl-0 mr-3">
              {!! Form::select('branch_id',$branches, app('request')->input('branch_id'), array('placeholder'=>'Select Branch','class' => 'form-control')) !!}
            </div>
            <div class="form-group col-md-1 mr-3">
              {!! Form::select('floor_id',$floors, app('request')->input('floor_id'), array('placeholder'=>'Select Floor','class' => 'form-control')) !!}

           </div>
           <div class="form-group col-md-2">
             <input type="submit" value="Search" class="btn btn-primary">
           </div>
           </form> 
        </div>
      </div>
        <div class="row">
          <div class="col-md-12">
            <div class="content-box white-bg box-radius max-height">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-7">
                    <h3 class="box-title inline">Camera</h3>
                                  
                  </div>
                  <div class="col-5 text-right">
                    {{-- <a class="view-link" style="pointer-events:none" href="">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> --}}
                  </div>
                </div><!--/.row-->
                @foreach ($cameras as $camera)
                <div class="camera_container bg-success">
                  <p>{{$camera->branch_name}}</p>
                  <p>{{$camera->floor_name}}</p>
                  <p>{{$camera->area}}</p>
                  <p>Occupancy: <span class="camera_{{$camera->id}}">1</span> </p>
                </div>
                @endforeach
               
               
             
              
                    {{-- <p  class="last_update"><b>Last Update:</b>  <span id="last_update"><?php  ?></span></p> --}}
                 
                  </div><!--/.pie-chhart-list-->

              </div><!--/.container-->
            </div><!--/.content-box-->
          </div><!--/.col-md-8-->
       
      
    <!-- end section -->

    
             
              

              </div><!--/.container-->
            </div><!--/.content-box-->
          </div><!--/.col-md-8-->
          
         

            
          <!--/.col-md-4-->
        </div><!--/.row-->
      </div>
    <!-- end section -->






    
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
      // Enable pusher logging - don't include this in production
      Pusher.logToConsole = true;
  
      var pusher = new Pusher("24e6a1c894ae01d9c931", {
        cluster: "mt1",
      });
  
      var channel = pusher.subscribe("my-channel");
      channel.bind("my-event", function (data) {
        //alert(JSON.stringify(data));
        let res = JSON.stringify(data);
        res = JSON.parse(res);
        console.log("In", res.current_occupancy);
        document.getElementById("current_occupancy").innerHTML = res.current_occupancy;
        document.getElementById("in").innerHTML = res.in;
        document.getElementById("out").innerHTML = res.out;
      });
  
      channel.bind("panic_alert", function (data) {
        //alert(JSON.stringify(data));
        let res = JSON.stringify(data);
        res = JSON.parse(res);
        document.getElementById("totalPanic").innerHTML = res.totalPanic;
        // /panic_alert_section
        var panic_alert_section =  document.getElementById(`panic_alert_section`);
        panic_alert_section.setAttribute('class','content-box box-radius white-bg bg-danger text-white panic_alert_section');
  
      });
  
  
      channel.bind("home", function (data) {
        console.log(JSON.stringify(data));
        let res = JSON.stringify(data);
        res = JSON.parse(res);
        document.getElementById("totalPanic").innerHTML = res.totalPanic;
        document.getElementById("aciveUser").innerHTML = res.aciveUser;
        document.getElementById("totalPerson").innerHTML = res.totalPerson;
        document.getElementById("voilation_count").innerHTML = res.voilation_count;
        let from_time = res.from_time;
        let to_time = res.to_time;
        let last_update=res.last_update;
        document.getElementById("last_update").innerHTML = res.last_update;
        // console.log('from_time',from_time);
        $('.link-update').each(function() {
          var currentElement = $(this);
          var url = currentElement.attr('href');
          url=updateQueryStringParameter(url,'from_time',from_time);
          url=updateQueryStringParameter(url,'to_time',to_time);
          currentElement.attr('href',url);
        });
  
        //Zones
        let zoneUsers=res.zoneUsers;
       
        for(let zone in zoneUsers){
           console.log('zone',zoneUsers[zone])
           deveui=zoneUsers[zone].deveui;
           zone_count=zoneUsers[zone].zone_count;
           className=zoneUsers[zone].className;
           data_percentage=zoneUsers[zone].data_percentage;
           if(className=='default_chart'){
               $('#zone_'+deveui).data('easyPieChart').update(data_percentage);
               $('#zone_'+deveui).data('easyPieChart').options.barColor='#007bff';
               $('#zone_'+deveui).find('.zone_count').html(zone_count);
           }
           else{
               $('#zone_'+deveui).data('easyPieChart').update(data_percentage);
               $('#zone_'+deveui).data('easyPieChart').options.barColor='#FF0000';
               $('#zone_'+deveui).find('.zone_count').html(zone_count);
           }
        }
        //Sensors
        let sensors=res.sensors;
        for(let sensor in sensors){
          deveui=sensors[sensor].deveui;
          sensor_count=sensors[sensor].sensor_count;
          default_bg=sensors[sensor].default_bg;
          opacity_label=sensors[sensor].opacity_label;
          console.log('deveui',deveui);
          console.log('sensor_count',sensor_count);
          console.log('default_bg',default_bg);
          console.log('opacity_label',opacity_label);
          
          var element =  document.getElementById(`sensor_${deveui}`);
          element.setAttribute('class',`marker ${default_bg}`);
          //element.setAttribute('class',`marker ${default_bg}`);
  
          // element.classList.add(`marker ${default_bg}`);
           element.style.opacity=opacity_label;
  
         var elementSensor= document.getElementById('sensor_count_'+deveui).innerText=sensor_count;
          console.log('elementSensor',elementSensor);
  
        }
  
        //$('#zone_F47CCDFFFF18AD89').data('easyPieChart').update(50);
  //     $('#zone_F47CCDFFFF18AD89').find('.zone_count').html(10);
  
        
      });
  
    </script>
  <script type="text/javascript">

function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
        if (value === undefined) {
          if (uri.match(re)) {
            return uri.replace(re, "$1$2");
          } else {
            return uri;
          }
        } else {
          if (uri.match(re)) {
            return uri.replace(re, "$1" + key + "=" + value + "$2");
          } else {
            var hash = "";
            if (uri.indexOf("#") !== -1) {
              hash = uri.replace(/.*#/, "#");
              uri = uri.replace(/#.*/, "");
            }
            var separator = uri.indexOf("?") !== -1 ? "&" : "?";
            return uri + separator + key + "=" + value + hash;
          }
        }
    }

    
    


      function fetchdata(){
 $.ajax({
  url: 'camera',
  type: 'get',
  dataType:'JSON',
  success: function(response){
    let data =JSON.stringify(response);
    data=JSON.parse(data);
    $('.in').html(data.in);
    $('.out').html(data.out);
    $('.current_occupancy').html(data.current_occupancy);
    
    


  }
 });
}

$(document).ready(function(){

  $('.link-update').each(function() {
    // var currentElement = $(this);
    // var value = currentElement.attr('href');
    // console.log("Href",value); 

    // var elementSensor= document.getElementById('sensor_count_F47CCDFFFF18AD89').innerHTML="A";



});


 //setInterval(fetchdata,5000);

//  $('.last_update').click(function(e){
//     $('#zone_F47CCDFFFF18AD89').data('easyPieChart').update(50);
//     $('#zone_F47CCDFFFF18AD89').find('.zone_count').html(10); 
    

//  });
});



        $('.carousel').carousel({
          interval: 2000
        }) //carousel

        $(".default_chart").easyPieChart({
                             size:120,
                             barColor : '#007bff',
                             scaleColor:false,
                             lineWidth:6,
                             trackColor:'#dcdcdc',
                             lineCap:'circle',
                             animate:1700
                           });

        $(".default_chart_danger").easyPieChart({
          size:120,
          barColor : '#FF0000',
          scaleColor:false,
          lineWidth:6,
          trackColor:'#dcdcdc',
          lineCap:'circle',
          animate:1700
        });


                               /* chart.js chart examples */
                            // chart colors
                            var colors = ['#007bff','#000000'];
                             /* 3 donut charts */
                            var donutOptions = {
                              cutoutPercentage: 80, 
                              legend: {position:'bottom', padding:0, labels: {pointStyle:'boder', usePointStyle:false}},
                              legend: {
                                      display: false
                                    },
                            };
                            // donut 1
                            var chDonutData1 = {
                                datasets: [
                                  {
                                    backgroundColor: colors.slice(0,3),
                                    borderWidth: 0,
                                    data: [0,0]
                                  }
                                ],
                                labels: [
                                          'Total Tags',
                                          'Assign Tags'
                                      ]
                            };
                            var chDonut1 = document.getElementById("regEmployees");
                              if (chDonut1) {
                                new Chart(chDonut1, {
                                    type: 'pie',
                                    data: chDonutData1,
                                    options: donutOptions
                                });
                              }
          

      </script>  

@endsection
