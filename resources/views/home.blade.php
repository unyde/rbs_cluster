@extends('layouts.admin')
@section('content')
    {{-- <script src="{{ asset('js/home.js') }}?t=<?php echo time(); ?>" defer></script> --}}
    <?php
    $selectedDate = date('d-m-Y', strtotime($data['selectedDate']));
    $selectedZoneId = $data['selectedZoneId'];
    ?>

    <div class="row">
        <div class="col-12">
            <form action="" onchange="" method="get" class="dashboard-form" style="margin:0">
                <div class="row">
                    <div class="col-12">
                        <h5>{{ config('app.name', 'Laravel') }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-md-2 pr-0">
                        <!-- <h6 class="card-heading3">Selected Zone</h6> -->
                        <!-- <h6 class="card-heading3">{{ session('zone_name') }}</h6> -->
                        <h6 class="card-heading3">{{ Auth::user()->name }}</h6>
                        <input type="hidden" value="{{ $selectedZoneId }}" id="zone_id" name="zone_id">
                        <!-- <h6 class="heading6 form-child-text">{{ session('zone_name') }}</h6> -->

                    </div>
                    <div class=" col-6 col-md-2 pl-0 margin-left">

                        <!-- <h6 class="card-heading3">Date</h6> -->
                        <h6 class="card-heading3">Today</h6>
                        <!-- <h6 class="heading6 form-child-text">{{ $selectedDate }}</h6> -->
                    </div>
                </div>
            </form>
        </div>
    </div><!-- ./ end top section -->

    <!-- Second section -->

    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
        <div class="col-md-4 col-sm-12">
            <div class="card panel-box radius-10 border-start border-0 border-3 border-info">
                <div class="card-body" style="min-height:104px">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <h3 class="mb-0 text-secondary card-heading3">Occupancy</h3>
                            <span class="span-panel-data">
                                <h4 id="occupany-count" class="occupany-count" class="my-1 text-info"
                                    style="margin:5px 0 !important">
                                    {{ $data['totalOccupancy'] }}</h4>
                                <p>Total <br> Occupancy</p>
                            </span>
                            <?php $peak_occupancy = $data['peak_occupancy']; ?>
                            <span class="span-panel-data">
                                <h4 id="peak_occupancy" title="{{ $peak_occupancy }}" class="occupany-count"
                                    class="my-1 text-info" style="margin:5px 0 !important">
                                    {{ $peak_occupancy }} </h4>
                                <p>Peak <br> Count</p>
                            </span>
                            <span class="span-panel-data">
                                <h4 id="average_occupancy" class="occupany-count" class="my-1 text-info"
                                    style="margin:5px 0 !important">
                                    {{ $data['average_occupancy'] }}</h4>
                                <p>Avg Occupancy <br>(Hour)</p>
                            </span>
                        </div>
                        <div class="rgt-icon">
                            <div class="question-icon popup" onclick="popupFunction(this.id)" title="Current Occupancy"
                                id="occupancy_popup_info">
                                <i class="far fa-question-circle"></i>
                                <span class="popuptext occupancy_popup_info_text"
                                    id="occupancy_popup_info_text">Occupancy</span>
                            </div>
                            <div class="widgets-icons bg-info rounded-circle bg-gradient-scooter text-white ms-auto">
                                <i class="fas fa-users" style="font-size:18px;margin:14px 0"></i>
                            </div>
                            <p class="mb-0 font-13" style="margin-top:5px">
                                <a href="current-occupancy?filter_date={{ $selectedDate }}">View more</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="card panel-box radius-10 border-start border-0 border-3 border-success">
                <div class="card-body" style="min-height:104px">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <h3 class="mb-0 text-secondary card-heading3">Footfall</h3>
                            <span class="span-panel-data">
                                <h4 class="my-1 text-success" id="foot-fall-count">{{ $data['totalFootFall'] }}</h4>
                                <p class="text-success">Total <br> Footfall</p>
                                <small style="font-size: 12px">&nbsp;</small>
                            </span>
                            <span class="span-panel-data" style="margin-bottm:10px">
                                <h4 id="peak_time" title="{{ $data['peak_foot_fall']['peak_foot_fall'] }}"
                                    class="my-1 text-info" style="margin:5px 0 !important">
                                    {{ $data['peak_foot_fall']['peak_foot_fall'] }}
                                </h4>
                                <p>Peak <br> Footfall</p>
                                <small id="peak_time_hour"
                                    style="font-size: 12px">{{ $data['peak_foot_fall']['peak_time'] }}</small>
                            </span>

                            <span class="span-panel-data">
                                <h4 id="average_foot_fall" class="occupany-count" class="my-1 text-info"
                                    style="margin:5px 0 !important">
                                    {{ $data['average_foot_fall'] }}</h4>
                                <p>Avg Footfall <br>(Hour)</p>
                                <small style="font-size: 12px">&nbsp;</small>
                            </span>
                            </span>
                        </div>
                        <div class="rgt-icon" style="margin-bottom:10px">
                            <div class="question-icon popup" onclick="popupFunction(this.id)" title="Footfall"
                                id="footfall_popup_info">
                                <i class="far fa-question-circle"></i>
                                <span class="popuptext footfall_popup_info_text"
                                    id="footfall_popup_info_text">Footfall</span>
                            </div>
                            <div class="widgets-icons bg-success rounded-circle bg-gradient-bloody text-white ms-auto"><i
                                    class="fas fa-walking" style="font-size:18px;margin:14px 0"></i>
                            </div>
                            <p class="mb-0 font-13" style="margin-top:5px">
                                <a href="foot-fall?filter_date={{ $selectedDate }}">View more</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
            <div class="card panel-box radius-10 border-start border-0 border-3 border-danger">
                <div class="card-body" style="min-height:100px">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <h3 class="mb-0 text-secondary card-heading3">Occupancy Violation
                            </h3>
                            <span class="span-panel-data">
                                <h4 class="my-1 text-danger" id="occupancy-violation">{{ $data['totalViolation'] }}</h4>
                                <p class="text-danger">Total <br> Violation</p>
                            </span>
                            <p class="mb-0 font-13" style="margin:0">
                                <a href="{{ route('zone.edit', $selectedZoneId) }}">Set Threshold</a> |
                                @if ($max_occupancy)
                                    <span class="span-panel-data">
                                        <span id="max_occupancy_allow" class="my-1 text-info"
                                            style="margin:5px 0 !important;font-size:12px;color:#000 !important">
                                            {{ $max_occupancy }} <span>Spend Threshold</span>
                                        </span>
                                        <!-- <p>Maximum <br> Occupancy</p> -->
                                    </span>
                                @endif
                            </p>
                        </div>
                        <div class="rgt-icon">
                            <div class="question-icon popup" onclick="popupFunction(this.id)" title="Occupancy Violation"
                                id="violation_popup_info">
                                <i class="far fa-question-circle"></i>
                                <span class="popuptext violation_popup_info_text" id="violation_popup_info_text">Occupancy
                                    Violation</span>
                            </div>
                            <div class="widgets-icons bg-danger rounded-circle bg-gradient-ohhappiness text-white ms-auto">
                                <i class="fas fa-users-slash" style="font-size:18px;margin:14px 0"></i>
                            </div>
                            <p class="mb-0 font-13" style="margin-top:6px">
                                <a href="violation?filter_date={{ $selectedDate }}">View more</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- //end second section -->
    <!--Info Section-->
    <div class="row">
        <div class="col-12">
            <div class="card zoom-card panel-box radius-10">
                <div class="card-body">
                    <p class="no-bottom-margin zoom-info"><span class="bold-text">Zoomable Statistic</span> : Select the
                        range in the chart you want to zoom in</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Chart section -->
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box radius-10">
                <div class="card-header">
                    <h6>Occupancy Overview</h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        <div id="occupancy-chart" class="chart-container-1"></div>
                        <div id="watermark-1" class="watermark none">No Data</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Foot Fall Chart Section -->
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box">
                <div class="card-header">
                    <h6>Footfall Overview</h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        <div id="foot-fall-chart" class="chart-container-1"></div>
                        {{-- <div id="watermark-2" class="watermark none">No Data</div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box">
                <div class="card-header">
                    <h6>Occupancy Violation Overview </h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        @if ($max_occupancy)
                            <div id="occupancy-violation-chart" class="chart-container-1"></div>
                        @else <div id="watermark-3" class="watermark ">Max threshold not set

                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let homeFetchUrl = "{{ route('home') }}?";
        homeFetchUrl += "<?php echo $data['zone_where']; ?>";

        setInterval(() => {
            fetch(homeFetchUrl)
                .then(response => response.json())
                .then(result => {
                    let response = JSON.stringify(result);
                    response = JSON.parse(response);
                    // console.log('Api Response', response)
                    document.getElementById("occupany-count").innerHTML =
                        response.totalOccupancy;

                    //Average Occupancy
                    document.getElementById("average_occupancy").innerText = response.average_occupancy;
                    let occupancy = response.totalOccupancy;
                    let peakOccupancyEl = document.getElementById("peak_occupancy");
                    let peak_occupancy = +peakOccupancyEl.innerText;
                    if (occupancy > peak_occupancy) {
                        peakOccupancyEl.innerText = occupancy;
                    }
                    //peak_occupancy

                    //Foot Fall peak_time    
                    document.getElementById("foot-fall-count").innerHTML =
                        response.totalFootFall;
                    document.getElementById("peak_time").innerText =
                        response.peak_foot_fall.peak_foot_fall;
                    document.getElementById('peak_time_hour').innerText = response.peak_foot_fall
                        .peak_time;
                    document.getElementById("average_foot_fall").innerText =
                        response.average_foot_fall;

                    average_foot_fall


                    document.getElementById("occupancy-violation").innerHTML =
                        response.totalViolation;

                });
        }, 10000); //10 Second


        const occupancyFileUrl = "{{ $data['occupancyFileUrl'] }}";
        const footFallFileUrl = "{{ $data['footFallFileUrl'] }}";
        const violationFileUrl = "{{ $data['violationFileUrl'] }}";

        const OccupancyChart = {
            ...highchartOption
        };
        OccupancyChart.data.csvURL = occupancyFileUrl;
        // occupancyFileUrl;
        Highcharts.chart("occupancy-chart", OccupancyChart);

        //FootFall 
        const footFallChart = {
            ...highchartOption
        };
        footFallChart.data.csvURL = footFallFileUrl;
        Highcharts.chart("foot-fall-chart", footFallChart);

        //Violation
        @if ($max_occupancy)
            const violationChart = {
            ...highchartOption
            };
            violationChart.data.csvURL = violationFileUrl;
            Highcharts.chart("occupancy-violation-chart", violationChart);
        @endif
    </script>
@endsection
