@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title ">User</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ url('users') }}" class=" btn-primary btn  ">Back </a>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif



    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Add New User</h6>
                </div>

                <div class="card-body">


                    {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'class' => 'form']) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Name</Label>
                                {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group  @error('email') has-error @enderror ">
                                <Label class="req">Email Id(Login ID)</Label>
                                {!! Form::text('email', null, ['placeholder' => 'Email Id', 'class' => 'form-control']) !!}
                                @error('email')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Company Name</Label>
                                {!! Form::select('company_id', $companyList, null, ['placeholder' => 'Select Company', 'class' => 'form-control']) !!}


                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group @error('confirm-password') has-error @enderror ">
                                <Label class="req">Branch Name</Label>
                                {!! Form::select('branch_id', $companyList, null, ['placeholder' => 'Select branch', 'class' => 'form-control']) !!}


                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group @error('password') has-error @enderror ">
                                <Label class="req">Password</Label>
                                {!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password', 'class' => 'form-control']) !!}
                                @error('password')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group @error('confirm-password') has-error @enderror ">
                                <Label class="req">Confirm Password</Label>
                                {!! Form::password('confirm-password', ['id' => 'confirm-password', 'placeholder' => 'Confirm Password', 'class' => 'form-control']) !!}
                                @error('confirm-password')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group @error('roles') has-error @enderror">
                                <Label class="req">Role</Label>
                                {!! Form::select('roles[]', $roles, [], ['class' => 'form-control']) !!}
                                @error('roles')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">Add</button>

                        </div>
                    </div>



                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {


            $('.form').validate({
                rules: {
                    company_id: {
                        required: true
                    },
                    name: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 15,
                    },
                    "confirm-password": {
                        required: true,
                        minlength: 6,
                        maxlength: 15,
                        equalTo: "#password"
                    },
                    roles: {
                        required: true
                    }
                }
            });
        });
    </script>

@endsection
