@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title ">User</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ route('users.index') }}" class=" btn-primary btn  ">Back </a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="row mt-2">
        <div class="col-md-12">
            <div class="alert-message alert alert-danger">
                <Label>Whoops!</Label> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    @endif


 

    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Edit User</h6>
                </div>

        <div class="card-body">

            {!! Form::model($user, ['method' => 'PATCH', 'autocomplete' => 'off', 'route' => ['users.update', $user->id]]) !!}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <Label class="req">Name</Label>
                        {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group  @error('email') has-error @enderror ">
                        <Label class="req">Username(Login ID)</Label>
                        {!! Form::text('email', null, ['placeholder' => 'Username', 'class' => 'form-control']) !!}
                        @error('email')
                            <span class="help-block">{{ $message }}</span>
                        @enderror

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group @error('password') has-error @enderror ">
                        <Label>Password(Leave blank if no password change)</Label>
                        {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
                        @error('password')
                            <span class="help-block">{{ $message }}</span>
                        @enderror

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group @error('confirm-password') has-error @enderror ">
                        <Label>Confirm Password(Leave blank if no password change)</Label>
                        {!! Form::password('confirm-password', ['placeholder' => 'Confirm Password', 'class' => 'form-control']) !!}
                        @error('confirm-password')
                            <span class="help-block">{{ $message }}</span>
                        @enderror

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group @error('roles') has-error @enderror">
                        <Label class="req">Role</Label>
                        {!! Form::select('roles[]', $roles, $userRole, ['class' => 'form-control', 'multiple']) !!} </td>
                        @error('roles')
                            <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group ">
                        <Label class="req">Status </Label>
                        {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'form-control']) !!}
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@endsection
