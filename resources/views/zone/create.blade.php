@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title">Add New Zone</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ url('zone') }}" class=" btn-primary btn  ">Back </a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif


    <!--/.row-->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Add New Zone</h6>
                </div>
                <div class="card-body">


                    {!! Form::open(['route' => 'zone.store', 'method' => 'POST', 'class' => 'form']) !!}
                    <div class="row">
                        @if ($userInfo['companyListShow'])
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <Label class="req">Company List</Label>

                                    {!! Form::select('company_id', $companyList, null, ['placeholder' => 'Select Company', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        @else
                            {!! Form::hidden('company_id', $userInfo['company_id'], ['placeholder' => '', 'class' => 'form-control']) !!}

                        @endif

                        @if (!$userInfo['branch_id'])
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <Label class="req">Branch Name</Label>
                                    {!! Form::select('branch_id', $branches, null, ['placeholder' => 'Select Branch', 'class' => 'form-control zone-by-address']) !!}
                                </div>
                            </div>
                        @else
                            {!! Form::hidden('branch_id', $userInfo['branch_id'], ['placeholder' => '', 'class' => 'form-control']) !!}
                        @endif




                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Building Name</Label>
                                {!! Form::text('building_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Floor</Label>
                                {!! Form::select('floor_id', $floors, null, ['placeholder' => 'Select Floor', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Zone name</Label>
                                {!! Form::text('zone_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label>Sub Title</Label>
                                {!! Form::text('sub_title', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Max Occupancy</Label>
                                {!! Form::number('max_occupancy', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 d-none">
                            <div class="form-group">
                                {!! Form::checkbox('sanitisation', 1, false, ['id' => 'sanitisation']) !!}
                                <Label class="req" for="sanitisation">Sanitisation</Label>

                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-6 visit_limit display_none">
                            <div class="form-group">
                                <Label class="req">Visit Limit</Label>
                                {!! Form::number('visit_limit', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>




                    </div>




                    <div class="row">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">Add</button>

                        </div>
                    </div>




                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            $('#sanitisation').click(function() {
                var inputValue = $(this).attr("value");
                $(".visit_limit").toggle();
            });
            $('.form').validate({
                rules: {
                    company_id: {
                        required: true
                    },
                    branch_id: {
                        required: true
                    },
                    zone_name: {
                        required: true
                    }

                }
            });
        });
    </script>

@endsection
