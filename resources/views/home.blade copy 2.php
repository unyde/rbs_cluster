@extends('layouts.admin')

@section('content')
{{-- <meta http-equiv="refresh" content="30"> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script>
<?php
$currentDate=date('d-m-Y');
?>
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12 pos-relative">
        <div class="line-animation"><img src="{{asset('img/beats_shapee.gif')}}" alt="line"/></div>
      </div>
    </div><!--/.row-->
    <div class="row mg-bottom5">
      <div class="col-8 col-sm-10">
        <div class="name text-left">
          <div class="hd-with-filter">
            <h1 class="pull-left">{{$companyName}}</h1>
            <form>
            <div class="filter-date">
              <div class="pull-left filter-input">
                <i class="far fa-calendar date-icon"></i>
               <input type="text" value="{{$currentDate}}" name="selected_date" class="form-control datepicker" placeholder="MM/DD/YY">
             </div>
             <div class="pull-left filter-btn">
              <input type="submit" value="Go" class="btn btn-primary no-radius">
            </div>
            </div><!--/.filter-date-->
            </form>

            </div>     
          <p style="clear:both;float:none;width:100%">Dashboard</p>
        </div>
      </div>
      {{-- <div class="col-4 col-sm-2 text-right">
        <div class="active-number"><span class="active-number-text">0</span> <br> Active</div>
      </div> --}}
    </div><!--/.row-->
    <div class="row">
      <div class="col-md-4 col-sm-12">
        <div class="content-box box-radius white-bg">
          <div class="container">
            <div class="row">
              <div class="col-9">
                <h3 class="box-title">Current Occupancy</h3>
              </div>
              <div class="col-3">
                <div class="box-icon-img center">
                <span class="dash-icon">
                <i class="fas fa-users" style="color:#006eff;font-size:18px;margin:14px 0"></i>
              </span>
                </div>
              </div>
              <div class="col-md-12">
                <h2 class="value-color value-font">
                {{$currentOccupancyTotal}}
                </h2>
              </div>
              <div class="col-md-6"></div>
              <div class="col-6 text-right">
                <a class="view-link" href="current-occupancy?filter_date={{$currentDate}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>

            </div>
          </div>  
        </div><!--/.content-box-->
      </div><!--/.com-md-4-->
      <div class="col-md-4 col-sm-12">
         <div class="content-box box-radius white-bg">
          <div class="container">
             <div class="row">
              <div class="col-9">
                <h3 class="box-title">Total FootFall</h3>
              </div>
              <div class="col-3">
                <div class="box-icon-img center">
                <span class="dash-icon">
                <i class="fas fa-walking" style="color:#006eff;font-size:18px;margin:14px 0"></i>
                <!-- 
                <i class="fas fa-walking"></i> -->
                </span>
                </div>
              </div>
              <div class="col-md-12">
              <h2 class="value-color value-font">{{$totalFootFall}}</h2>
              </div>
              <div class="col-8">
               <!--  <p class="light-grey-color"><span class="highlight-font red-color">+ 0</span> than yesterday</p>   -->
               <ul class="list_inline">
                 <li class="green-color" style="font-size:24px;font-weight:normal;">-</li>
                 <?php
                  $yesterdayColor="#228B22";
                  $yesterdayCount=0;
                  if($totalFootFallYesterday>$totalFootFall){
                    $yesterdayCount=$totalFootFallYesterday-$totalFootFall;
                  }
                  else {
                 
                    $yesterdayCount=$totalFootFall-$totalFootFallYesterday;
                    $yesterdayColor="#ff0000";
                  }
                 ?>
                  <li style="color:{{$yesterdayColor}}"><span class="break">{{$yesterdayCount}}</span> Yesterday</li>
                  {{-- <li><span class="break">15</span> Week</li>
                  <li><span class="break">30</span> Month</li> --}}
               </ul>
              </div>
              <div class="col-4 text-right">
                <a class="view-link" style="margin:5px 0 0" href="foot-fall?filter_date={{$currentDate}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
            
            </div>
          </div>  
        </div><!--/.content-box-->
      </div><!--/.col-md-4-->
     <div class="col-md-4 col-sm-12">
         <div class="content-box box-radius white-bg">
          <div class="container">
             <div class="row">
              <div class="col-9">
                <h3 class="box-title">Occupancy Violation</h3>
              </div>
              <div class="col-3">
                <div class="box-icon-img center">
                <span class="dash-icon">
                <i class="fas fa-users-slash"  style="color:#006eff;font-size:18px;margin:14px 0"></i>
               <!-- 
                <i class="fas fa-walking"></i> -->
                </span>
                </div>
              </div>
              <div class="col-md-12">
              <h2 class="value-color value-font">{{$occupancyViolation}}</h2>
              </div>
              <div class="col-6">
                <!-- <p class="light-grey-color">Today</p>  --> 
              </div>
              <div class="col-6 text-right">
                <a class="view-link" href="violation?filter_date={{$currentDate}}">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>  
        </div><!--/.content-box-->
      </div><!--/.col-md-4-->
    </div><!--/.row-->
  </div><!--/.col-md-9-->
</div><!--/.row-->  
<div class="graph-row-list">
  <div class="row">
    <div class="col-md-6">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
         <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">FOOTFALL TREND</h3>
            </div>
            <div class="col-7 text-right">
              <!-- <a class="view-link" href="#">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> -->
              <ul class="tab-list graph_tab_list footfall_trends">
                <li class="active" data-id="today-tab">Today</li>
                <li data-id="Week-tab">Week</li>  
                <li data-id="month-tab">Month</li>
              </ul><!--/.tab-list-->
            </div>
          </div><!--/.row-->
          <div class="graph-tab today-tab footfall_trends_graph_tab block">
            <canvas id="todaylinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('todaylinechart').getContext('2d');
              let labelText=[];
              let fallTrendToday=[];
              let fallTrendYesterday=[];
              let fallTrendLastMonth=[];
              let currentWeek=[];
              let lastWeek=[];
              <?php 
               forEach($timeSlots as $timeSlot){
                 ?>
                 labelText.push('<?php echo $timeSlot["label_text"] ?>');
                 <?php
               }
               $today=$fallTrends['today'];
               if(is_array($today)){
                 forEach($today['today'] as $key=>$item){
                  ?>
                  fallTrendToday.push('<?php echo $item; ?>');
                  fallTrendYesterday.push("<?php echo $today['yesterday'][$key]; ?>");
                  fallTrendLastMonth.push("<?php echo $today['lastMonth'][$key]; ?>");
                 <?php
                 }
               }

               //Week 
               forEach($weekData['weekDay'] as $key=>$item){
                ?>
                currentWeek.push("<?php echo $weekData['currentWeek'][$key]; ?>");
                lastWeek.push("<?php echo $weekData['lastWeek'][$key]; ?>");
                  <?php
               }
              ?>
              
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:labelText,
                  datasets:[
                  {
                    label: 'Today',
                    data: fallTrendToday,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Yesterday',
                    data: fallTrendYesterday,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Month Day',
                    data: fallTrendLastMonth,
                    fill:false,
                    borderColor: 'rgb(244, 208, 63)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
           <div class="graph-tab Week-tab footfall_trends_graph_tab">
            <canvas id="weeklinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('weeklinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Sunday','Monday','Tuesday','Wenesday','Thursday','Friday','Saturaday'],
                  datasets:[
                     {
                    label: 'Current Week',
                    data: currentWeek,
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Week',
                    data: lastWeek,
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Month Week',
                    data: [0,20,15,40,30,35,10],
                    fill:false,
                    borderColor: 'rgb(244, 208, 63)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
          <div class="graph-tab month-tab footfall_trends_graph_tab">
            <canvas id="monthlinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('monthlinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Apr-1','Apr-2','Apr-3','Apr-4','Apr-5','Apr-6','Apr-7','Apr-8','Apr-9','Apr-10','Apr-11','Apr-12','Apr-13','Apr-14','Apr-15','Apr-16','Apr-17','Apr-18','Apr-19','Apr-20','Apr-21','Apr-22','Apr23','Apr-24','Apr-25','Apr-26','Apr-27','Apr-28','Apr-29','Apr-30',],
                  datasets:[
                  {
                    label: 'Cuurent Month',
                    data: [0,10,20,15,40,20,5,10,20,15,40,20,15,10,20,15,40,20,10,10,20,15,40,20,20,10,20,15,40,20],
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                   {
                    label: 'Last Month',
                    data: [0,15,25,15,30,10,15,10,20,15,30,10,55,30,40,25,60,20,10,20,20,15,40,20,10,30,20,25,10,20],
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Year Month',
                    data: [0,10,30,15,30,10,15,10,40,15,30,10,20,15,10,12,40,20,10,10,40,15,40,30,20,30,10,35,40,20],
                    fill:false,
                     borderColor: 'rgb(244, 208, 63)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
        </div>
      </div> 
      <script type="text/javascript">
        $(document).ready(function() {
          $(".footfall_trends li").click(function(){
            $(".footfall_trends li").removeClass('active');
            $(this).addClass('active');
            var tab = $(this).attr("data-id");
            $(".footfall_trends_graph_tab").removeClass("block");
            $("."+tab).addClass("block");
          });
        });
      </script>   
    </div><!--/.col-md-6-->
       <div class="col-md-6">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
         <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">AVG. TIME SPEND</h3>
            </div>
            <div class="col-7 text-right">
              <!-- <a class="view-link" href="#">View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> -->
              <ul class="tab-list graph_tab_list avg_graph_tab_list">
                <li class="active" data-id="avg-today-tab">Today</li>
                <li data-id="avg-Week-tab">Week</li>
                <li data-id="avg-month-tab">Month</li>
              </ul><!--/.tab-list-->
            </div>
          </div><!--/.row-->
          <div class="graph-tab avg_graph_tab avg-today-tab block">
            <canvas id="avgtodaylinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('avgtodaylinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['10:00','10:30','11:00','11:30','12:00','12:30'],
                  datasets:[
                  {
                    label: 'Today',
                    data: [0,10,20,30,40,50],
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Yesterday',
                    data: [0,15,5,20,10,40],
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Month Day',
                    data: [0,20,15,40,30,35],
                    fill:false,
                    borderColor: 'rgb(244, 208, 63)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
           <div class="graph-tab avg-Week-tab avg_graph_tab">
            <canvas id="avgweeklinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('avgweeklinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Monday','Tuesday','Wenesday','Thursday','Friday','Saturaday','Sunday'],
                  datasets:[
                     {
                    label: 'Current Week',
                    data: [0,10,20,30,40,50,40],
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Week',
                    data: [0,15,5,20,10,40,20],
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Month Week',
                    data: [0,20,15,40,30,35,10],
                    fill:false,
                    borderColor: 'rgb(244, 208, 63)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
          <div class="graph-tab avg-month-tab avg_graph_tab">
            <canvas id="avgmonthlinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('avgmonthlinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['Apr-1','Apr-2','Apr-3','Apr-4','Apr-5','Apr-6','Apr-7','Apr-8','Apr-9','Apr-10','Apr-11','Apr-12','Apr-13','Apr-14','Apr-15','Apr-16','Apr-17','Apr-18','Apr-19','Apr-20','Apr-21','Apr-22','Apr23','Apr-24','Apr-25','Apr-26','Apr-27','Apr-28','Apr-29','Apr-30',],
                  datasets:[
                  {
                    label: 'Cuurent Month',
                    data: [0,10,20,15,40,20,5,10,20,15,40,20,15,10,20,15,40,20,10,10,20,15,40,20,20,10,20,15,40,20],
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  },
                   {
                    label: 'Last Month',
                    data: [0,15,25,15,30,10,15,10,20,15,30,10,55,30,40,25,60,20,10,20,20,15,40,20,10,30,20,25,10,20],
                    fill:false,
                    borderColor: 'rgb(220, 20, 60)',
                    tension: 0.1,
                  },
                  {
                    label: 'Last Year Month',
                    data: [0,10,30,15,30,10,15,10,40,15,30,10,20,15,10,12,40,20,10,10,40,15,40,30,20,30,10,35,40,20],
                    fill:false,
                     borderColor: 'rgb(244, 208, 63)',
                    tension: 0.1,
                  },
                  ]
                },
                options:{
                  legend: {
                    display: true
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
          </div>
        </div>
      </div> 
      <script type="text/javascript">
        $(document).ready(function() {
          $(".avg_graph_tab_list li").click(function(){
            $(".avg_graph_tab_list li").removeClass('active');
            $(this).addClass('active');
            var tab = $(this).attr("data-id");
            $(".avg_graph_tab").removeClass("block");
            $("."+tab).addClass("block");
          });
        });
      </script>   
    </div><!--/.col-md-6-->
     <div class="col-md-6">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
           <div class="row mg-bottom20">
            <div class="col-5">
              <h3 class="box-title inline" style="margin:8px 0 0 ">AVG. TIME - SPEND</h3>
            </div>
          </div><!--/.row-->
           <canvas id="boxlinechart" class="none"></canvas>
            <script type="text/javascript">
              var linectx = document.getElementById('boxlinechart').getContext('2d');
              var myLineChart = new Chart(linectx, {
                type: 'line',
                data:{
                  labels:['10:00','11:00','12:00','13:00','14:00','15:00','16:00'],
                  datasets:[{
                    label: 'FOOTFALL TRENDS',
                    data: [0,10,20,15,40,20,10],
                    fill:false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1,
                  }]
                },
                options:{
                  legend: {
                    display: false
                  },
                  scales: {
                        xAxes: [{
                           gridLines: {
                              display: false
                           }
                        }],
                        yAxes: [{
                           gridLines: {
                              display: false
                           }
                        }]
                   }
                }
              });
            </script>
        </div>
      </div>    
     </div><!--/.col-md-4-->
  </div><!--/.row-->
</div><!--/.graph-row-list-->
<div class="graph-row-list">
  <div class="row">
    <div class="col-md-12">
      <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
          <div class="row mg-bottom20">
            <div class="col-8">
              <h3 class="box-title inline">Device Status</h3>
              <div class="box-title2 inline blue-bg">
               <h4> {{$activeCameras->count()+$inActiveCameras->count()}} <span class="small-heading light-grey-color">Device</span></h4>
              </div>
            </div>
             <div class="col-4" style="width:100%">
              <div class="inline-width-text search-input last" style="width:100%">
              <input type="text" value="" class="form-control home_white_bg" name="search" placeholder="Device Id,Device Name,Area,office" autocomplete="off">
            </div>
        </div>
          </div>
          <div class="home_box_table_scroll">
         <table class="table table-striped table-responsive1">
      <thead>
        <tr>
          <th>Sr.no</th>
          <th>Office Name</th>
          <th>Zone</th>
          <th>Device Name</th>
          <th>Device Id</th>
          <th>Status</th>
          <th>DateTime</th>
          <th>Device Health</th>
        </tr>
      </thead>
      <tbody>
      <form method="post">
      <input type="hidden" name="posted" value="1">
        @foreach ($activeCameras as $device)
        <tr>
          <td>1</td>
          <td>{{$device->branch_name}}</td>
           <td>{{$device->zone_name}}</td>
           <td>{{$device->device_name}}</td>
           <td>{{$device->device_id}}</td>
          
          <td>
           @if($device->in)
           Active
           @endif
            </td>
          <td>
            <?php
              $created_at=$device->created_at;
              if($created_at){
                echo date('d-m-Y h:i a',strtotime($created_at));
              }
              ?></td>
         
          <td>
            <span class="device_status"><i class="fas fa-fan blue-color"></i> 60</span> <span class="device_status"><i class="fas fa-power-off red-color"></i> OFF</span> <span class="device_status"><i class="fas fa-wifi red-color"></i> NO</span> <span class="device_status"><i class="fas fa-thermometer-full blue-color"></i> 30 &#8451;</span>
          </td>
          </tr>
        @endforeach

        @foreach ($inActiveCameras as $device)
        <tr>
          <td>1</td>
          <td>{{$device->branch_name}}</td>
           <td>{{$device->zone_name}}</td>
           <td>{{$device->device_name}}</td>
           <td>{{$device->device_id}}</td>
          
          <td>
          InActive
            </td>
          <td>
            <?php
              $created_at=$device->created_at;
              if($created_at){
                echo date('d-m-Y h:i a',strtotime($created_at));
              }
              ?></td>
         
          <td>
            <span class="device_status"><i class="fas fa-fan blue-color"></i> 60</span> <span class="device_status"><i class="fas fa-power-off red-color"></i> OFF</span> <span class="device_status"><i class="fas fa-wifi red-color"></i> NO</span> <span class="device_status"><i class="fas fa-thermometer-full blue-color"></i> 30 &#8451;</span>
          </td>
          </tr>
        @endforeach
         
            
        </form>
      </tbody>
    </table>
  </div>
  <div class="col-12 text-right">
      <a class="view-link" href="device-status-list.php">View More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
    </div>

        </div>
      </div>
    </div><!--/.col-md-12-->
  </div><!--/.row-->
</div><!--/.graph-row-list-->

@endsection
