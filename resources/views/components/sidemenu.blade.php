<ul class="navbar-nav mr-auto nav-sidebar scroll-navsidebar">
    <li class="label-check">
    <a href="#"> <label for="check"><i class="fas fa-align-justify"></i></label></a>
    </li>

    <li class="nav-item  @if ($controller=='HomeController' && !$zone_id) active @endif">
        <a title="Dashboard" class="nav-link" href="{{ url('home') }}"><span class=" dash-icon"><i
                    class="fas fa-home"></i></span> <span class="navtext">Dashboard</span></a>
    </li>

    @if ($zones)
        @foreach ($zones as $key => $zone)
            <li class="nav-item @if ($controller=='HomeController' && $zone_id==$zone->id) active @endif">
                <?php
                $menuClass = 'no-sub-title';
                $sub_title = $zone->sub_title;
                if ($sub_title);
                $menuClass = 'menu-sub-title';
                ?>
                <a title="{{ $zone->zone_name }}" class="nav-link {{ $menuClass }}"
                    href="{{ url('home') }}?zone_id={{ $zone->id }}">
                    <span class="dash-icon"><i class="fad fa-person-booth"></i></span>
                    <span class="navtext">{{ $zone->zone_name }}
                        @if ($sub_title) <br>{{ $sub_title }}@endif
                    </span>

                </a>
            </li>
        @endforeach
    @endif


    @canany(['company-list', 'company-edit', 'company-create'])
        <li class="nav-item @if ($controller=='CompanyController' ) active @endif">
            <a title="Company" class="nav-link" href="{{ url('company') }}"><span class="dash-icon"><i
                        class="fas fa-business-time"></i></span> <span class="navtext">Company</span> </a>
        </li>
    @endcan
    @canany(['branch-list', 'branch-edit', 'branch-create'])
        <li class="nav-item @if ($controller=='BranchController' ) active @endif">
            <a title="Branch" class="nav-link" href="{{ url('branch') }}"><span class="dash-icon"><i
                        class="fas fa-building"></i></span> <span class="navtext">Branch</span> </a>
        </li>
    @endcan

    @canany(['zone-list', 'zone-edit', 'zone-create'])
        <li class="nav-item @if ($controller=='ZoneController' ) active @endif">
            <a title="Zone" class="nav-link" href="{{ url('zone') }}"><span class="dash-icon">
                    <i class="fas fa-chart-area"></i>
                </span> <span class="navtext">Zone</span> </a>
        </li>
    @endcan
    @canany(['branch'])
        <li class="nav-item @if ($controller=='CameraController' ) active @endif">
            <a title="Device" class="nav-link" href="{{ url('device') }}"><span class="dash-icon"><i
                        class="fas fa-camera"></i></span> <span class="navtext">Device</span> </a>
        </li>
    @endcan
    @canany(['camera-list', 'camera-edit', 'camera-create'])
        <li class="nav-item @if ($controller=='CameraController' ) active @endif">
            <a title="Camera" class="nav-link" href="{{ url('camera') }}"><span class="dash-icon"><i
                        class="fas fa-camera"></i></span> <span class="navtext">Camera</span> </a>
        </li>
    @endcan

    {{-- @canany(['tag-user-list', 'tag-user-edit', 'tag-user-create'])
        <li class="nav-item @if ($controller == 'TagUserController') active @endif">
            <a title="App User" class="nav-link" href="{{ url('tag-user') }}"><span class="dash-icon">
                    <i class="fas fa-user"></i>
                </span> <span class="navtext">App User</span> </a>
        </li>
    @endcan --}}

    @canany(['user-list', 'user-edit', 'user-create'])
        <li class="nav-item @if ($controller=='UsersController' ) active @endif">
            <a title="Users" class="nav-link" href="{{ url('users') }}"><span class="dash-icon"><i
                        class="fas fa-users"></i></span> <span class="navtext">User</span> </a>
        </li>
    @endcan


    @canany(['role-list', 'role-edit', 'role-create'])
        <li class="nav-item @if ($controller=='RoleController' ) active @endif">
            <a title="Role" class="nav-link" href="{{ route('roles.index') }}"><span class="dash-icon"><i
                        class="fas fa-lock"></i></span> <span class="navtext">Role</span> </a>
        </li>
    @endcan



</ul>
