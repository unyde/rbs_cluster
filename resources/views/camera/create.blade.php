@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title">Camera</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ url('camera') }}" class=" btn-primary btn  ">Back </a>
        </div>

    </div>

    @if (count($errors) > 0)
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif



    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Add New Camera</h6>
                </div>

                <div class="card-body">


                    {!! Form::open(['route' => 'camera.store', 'method' => 'POST', 'class' => 'form']) !!}
                    <div class="row">
                        @if ($userInfo['companyListShow'])
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <Label class="req">Company List</Label>

                                    {!! Form::select('company_id', $companyList, null, ['placeholder' => 'Select Company', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        @else
                            {!! Form::hidden('company_id', $userInfo['company_id'], ['placeholder' => '', 'class' => 'form-control']) !!}

                        @endif

                        @if (!$userInfo['branch_id'])
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <Label class="req">Branch Name</Label>
                                    {!! Form::select('branch_id', $branches, null, ['placeholder' => 'Select Branch', 'class' => 'form-control zoneByBranch']) !!}
                                </div>
                            </div>
                        @else
                            {!! Form::hidden('branch_id', $userInfo['branch_id'], ['placeholder' => '', 'class' => 'form-control ']) !!}
                        @endif





                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Zone Name</Label>
                                {!! Form::select('zone_id', [], null, ['id' => 'zone_id', 'placeholder' => 'Select Floor', 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Device Name</Label>
                                {!! Form::text('device_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Device Id</Label>
                                {!! Form::text('device_id', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Device Mac</Label>
                                {!! Form::text('device_mac', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Device Reboot</Label>
                                {!! Form::select('reboot', [1 => 'Yes', 0 => 'No'], 0, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Counter Restart</Label>
                                {!! Form::select('counter_restart', [1 => 'Yes', 0 => 'No'], 0, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Height(Ft.)</Label>
                                {!! Form::number('height', 150, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Reboot Type
                                </Label>
                                {!! Form::select('reboot_type', ['normal' => 'Normal', 'interrupt' => 'Interrupt'], null, ['class' => 'form-control']) !!}

                            </div>
                        </div>

                    </div>

                    <div class="row mt-3 float-left w-100">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">Add</button>

                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {

            $('.form').validate({
                rules: {
                    company_id: {
                        required: true
                    },
                    branch_id: {
                        required: true
                    },
                    zone_id: {
                        required: true
                    },
                    camera_name: {
                        required: true
                    },
                    device_id: {
                        required: true
                    }
                }
            });
        });
    </script>
@endsection
