@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title padleft20">Device Status </h5>
        </div>
    </div>



    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box radius-10">
                <div class="card-header">
                    <h6>Device Status</h6>
                </div>

                <div class="card-body">
                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th>Slno</th>
                                @can('admin-only')
                                    <th>Company Name</th>
                                    <th>Branch Name</th>
                                @endcan
                                <th>Zone Name</th>
                                <th>Device Name</th>
                                <th>Device Id</th>
                                <th>Last Request</th>
                                <th>Status</th>
                                {{-- <th>Action</th> --}}
                            </tr>

                        </thead>
                        <tbody>
                            <input type="hidden" name="posted" value="1">

                            @forelse($data as $row)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    @can('admin-only')
                                        <td>{{ $row->company_name }}</td>
                                        <td>{{ $row->branch_name }}</td>
                                    @endcan
                                    <td>{{ $row->zone_name }}</td>
                                    <td>{{ $row->device_name }}</td>
                                    <td>{{ $row->device_id }}</td>
                                    <td>
                                        <?php
                                        $last_request = $row->last_request;
                                        if ($last_request);
                                        echo date('d M Y, h:i a', strtotime($last_request));
                                        ?>
                                    </td>
                                    <td>
                                        <?php $deviceStatus = $row->deviceStatus($last_request); ?>
                                        @if ($deviceStatus)
                                            <span class="indicator online"></span> Online
                                        @else


                                            <span class="indicator offline"></span> Offline
                                        @endif
                                    </td>

                                </tr>

                            @empty
                                <tr>
                                    <td colspan="9">
                                        <p class="text-center text-danger">No record found.</p>
                                    </td>
                                </tr>
                            @endforelse


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
