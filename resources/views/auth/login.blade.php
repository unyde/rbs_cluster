@extends('layouts.app')

@section('content')
    <section class="section login-section section-bg" style="background:#EFF4F5;min-height:100%;padding-bottom:10%">
        <div class="container">
            <div class="row justify-content-center">
                <!-- <div class="col-12 col-sm-8">
             <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </p>
            </div> -->
                <div class="col-12 col-md-4">
                    <div class="white-bg rad-conner" style="padding:30px 15px; padding-bottom:0px">
                        <form method="POST" class="form" id="login_form" action="{{ route('login') }}">
                            @csrf

                            <h3 class="font-20 font-500" style="margin-bottom:15px">Login</h3>




                            <div class="form-group">
                                <input id="email" type="email"
                                    class=" input-field form-control @error('email') is-invalid @enderror" name="email"
                                    placeholder="Enter your E-mail id" value="{{ old('email') }}" required
                                    autocomplete="email" place autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <!--/.form-group-->

                            <div class="form-group">
                                <input id="password" type="password"
                                    class="input-field form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">
                                <!-- <input type="password" class="input-field" id="user_phone" name="password" placeholder="Password" autocomplete="off">
                                     -->
                            </div>
                            <!--/.form-group-->
                            <div class="form-group">
                                <input class="btn frm-btn" type="submit" name="login" value="Login">
                                <!-- @if (Route::has('password.request')) <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a> @endif  -->

                            </div>
                            <div class="form-group text-center">
                                <!-- <a class="font-500" href="forgot.php">Forget Password?</a>
              -->
                                &nbsp;
                            </div>
                            <div class="form-group text-center">
                                &nbsp;
                            </div>

                        </form>

                        <script>
                            $(document).ready(function() {


                                $('.form').validate({
                                    rules: {
                                        email: {
                                            required: true
                                        },
                                        password: {
                                            required: true,
                                        },
                                    }
                                });
                            });
                        </script>



                    </div>
                    <!--/.white-bg-->
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
    <!--/.section-->
@endsection
