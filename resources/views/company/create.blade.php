@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10 ">
            <h5 class="page-title">Company</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ url('company') }}" class=" btn-primary btn ">Back </a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="row">
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    <!--/.row-->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Add New Company</h6>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'company.store', 'method' => 'POST', 'class' => 'form']) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Company Name</Label>
                                {!! Form::text('company_name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group  @error('email') has-error @enderror ">
                                <Label class="req">Email Id</Label>
                                {!! Form::text('email', null, ['placeholder' => '', 'class' => 'form-control']) !!}
                                @error('email')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <Label class="req">Contact Number</Label>
                                {!! Form::text('contact_number', null, ['class' => 'form-control']) !!}


                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">Add</button>

                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.form').validate({
                rules: {
                    company_name: {
                        required: true
                    },
                    email: {
                        email: true
                    }
                }
            });
        });
    </script>

@endsection
