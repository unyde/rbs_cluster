<!DOCTYPE html>
<head>
  <title>Pusher Test</title>
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('1bfe7150070c0692de68', {
      cluster: 'mt1'
    });

//     var channel = pusher.subscribe('my-channel');
//     channel.bind('my-event', function(data) {
//       alert(JSON.stringify(data));
//     });

//     channel.bind('pusher:subscription_succeeded', function(members) {
//     alert('successfully subscribed!');
// });

var channel = pusher.subscribe('my-channel');

channel.bind('pusher:subscription_succeeded', function(members) {
  //alert('A');
})



  </script>
</head>
<body>
  <h1>Pusher Test</h1>
  <p>
    Try publishing an event to channel <code>my-channel</code>
    with event name <code>my-event</code>.
  </p>
</body>