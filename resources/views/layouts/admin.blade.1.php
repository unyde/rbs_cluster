<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
     <!--/.chart-js-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script src="{{ asset('bootstrap/js/jquery.min.js')}}"></script>
    <script src="{{ asset('bootstrap/js/jquery.validate.js')}}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap-datepicker.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}?t=<?php echo time();?>">
     <link rel="stylesheet" href="{{ asset('css/mobile.css')}}?t=<?php echo time();?>">
     <link rel="stylesheet" href="{{ asset('css/newstyle.css')}}?t=<?php echo time();?>">

  <script>
  function ConfirmDelete()
{
  var x = confirm("Are you sure you want to delete?");
  if (x)
      return true;
  else
    return false;
}



  </script>
    
</head>
<body style="background:#eff5f5">
<input style="position: relative;z-index:100" type="checkbox" id="check">
  <nav class="navbar dashboad-navbar">
  <div class="dash-navtoggle-bar mobile-view"><label for="check"><i class="fas fa-align-justify"></i></label></a></li></div>
  <a class="navbar-brand" href="dashboard.php">
  {{ config('app.name', 'Laravel') }}
  </a>
  <ul class="navbar-nav top-right-navbar no-mobile-view">
    
    <li>

      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ asset('img/dashboard/account_user.png')}}" alt="User" width="20" height="20"/>{{ Auth::user()->name }}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

        </div>
      </div><!--/.drop-down-->
    </li>
  </ul>
  <ul class="navbar-nav top-right-navbar mobile-view dashboard-nav-right">
    <li>
      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ asset('img/dashboard/account_user.png')}}" alt="User" width="20" height="20"/>{{ Auth::user()->name }}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        
        <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

         </div><!--/.drop-down-menu-->
      </div><!--/.drop-down-->
    </li>
  </ul>
</nav>
<div class="side-navbar slim">
   <ul class="navbar-nav mr-auto nav-sidebar scroll-navsidebar">
    <li class="label-check"><a href="#"><label for="check"><i class="fas fa-align-justify"></i></label></a></li>
     <li class="nav-item  @if($controller=='HomeController') active @endif">
     
     <a title="Dashboard" class="nav-link" href="{{url('home')}}"><span class=" dash-icon"><i class="fas fa-home"></i></span> <span class="navtext">Dashboard</span></a>
    </li>
    <li class="nav-item @if($controller=='AssetController') active @endif ">
      <a title="Asset" class="nav-link" href="assets"><span class="dash-icon"><i class="fas fa-list"></i></span> <span class="navtext">Asset</span></a>
    </li>
    
    <!-- <li class="nav-item">
      <a title="Category" class="nav-link" href="home"><span class="dash-icon"><i class="fas fa-tag"></i></span> <span class="navtext">Category</span></a>
    </li> -->

    <li class="nav-item @if($controller=='SurveyorController') active @endif">
      <a title="Surveyors" class="nav-link" href="{{url('surveyors')}}"><span class="dash-icon"><i class="fas fa-user"></i></span> <span class="navtext">Surveyors</span></a>
    </li>

    
     <li class="nav-item @if($controller=='UsersController') active @endif">
        <a title="Users" class="nav-link" href="{{url('users')}}"><span class="dash-icon"><i class="fas fa-users"></i></span> <span class="navtext">User</span> </a>
    </li>
<!--     
    <li class="nav-item">
        <a title="Role" class="nav-link" href="role"><span class="dash-icon"><i class="fas fa-lock"></i></span> <span class="navtext">User</span> </a>
    </li>
     -->

   </ul>
</div><!--/.side-navbar-->
<div id="content-wrapper">
<div class="container-fluid slide-content">
  <div class="child-container">
  @yield('content')

