<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    {{-- <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"> --}}
    <link rel="icon" href="{{ asset('assets/img/fav.png') }}" type="image/png" sizes="16x16">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}?v1=<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('css/mobile.css') }}?v1=<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('css/newstyle.css') }}?v1=<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap-timepicker.min.css') }}">

    <script src="{{ asset('bootstrap/js/jquery.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>


    <script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/jquery.validate.js') }}"></script>
    {{-- <script src="{{ asset('js/chart.js') }}?t=<?php echo time(); ?>"></script> --}}
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

    <!-- Hightchart -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>


    <script>
        const APP_URL = `{{ Config::get('app.url') }}`
        const PUSHER_APP_KEY = "{{ Config::get('app.pusher_app_key') }}";
        const highchartOption = {
            chart: {
                type: "area", //spline,area
                zoomType: "x",
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: "Count",
                },
            },
            xAxis: {
                title: {
                    text: "Time",
                },
            },
            title: {
                text: "",
            },

            accessibility: {
                announceNewData: {
                    enabled: true,
                    minAnnounceInterval: 15000,
                    announcementFormatter: function(allSeries, newSeries, newPoint) {
                        if (newPoint) {
                            return "New point added. Value: " + newPoint.y;
                        }
                        return false;
                    },
                },
            },
            data: {
                csvURL: '',
                enablePolling: true,
                // dataRefreshRate: 15,
            },
        }

        // const chartOptions = {
        //     maintainAspectRatio: false,
        //     scales: {
        //         xAxes: [{
        //             scaleLabel: {
        //                 display: true,
        //                 labelString: 'Time'
        //             },
        //             ticks: {
        //                 autoSkip: false,
        //                 maxRotation: 90,
        //                 minRotation: 90
        //             }
        //             // borderSkipped: true
        //             // maxBarThickness: 5,
        //             // barThickness: 3
        //         }],
        //         yAxes: [{
        //             afterBuildTicks: (x) => {
        //                 console.log(x);
        //             },
        //             scaleLabel: {
        //                 display: true,
        //                 labelString: 'Count'
        //             },
        //             ticks: {
        //                 beginAtZero: true,
        //                 callback: function(value) {
        //                     if (value % 1 === 0) {
        //                         return value;
        //                     }
        //                 }
        //             },
        //         }]
        //     }
        // };

        function goBack() {
            console.log('Back');
            window.history.back();
        }

        function displayWaterMark(containerId, data) {
            let watermark = true;
            let watermarkClass = "";
            if (Array.isArray(data)) {
                for (let element of data) {
                    console.log(element);
                    if (element > 0) {
                        console.log('element', element);
                        watermark = false;
                        break;
                    }

                }
            }
            console.log('containerId', containerId);
            console.log('watermark', watermark);
            let waterMarkEl = document.getElementById(containerId);
            if (watermark) {
                $('#' + containerId).removeClass('none');
                // waterMarkEl.classList.remove("none");
            } else {
                waterMarkEl.classList.add("none");
            }
        }

        const CHART_COLOR = "#5f8de4";
        $(document).ready(function() {
            var screensize = document.documentElement.clientWidth;
            if (screensize < 700) {
                $(".sidebar-checkbox").prop('checked', false)
            }
            $('[name=company_id]').click(function(e) {
                let selectedValue = $(this).val();

                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                //http-url, https- secure_url
                jQuery.ajax({
                    url: `${APP_URL}/branchList/${selectedValue}`,
                    dataType: "json",
                    success: function(response) {
                        let branches = JSON.parse(JSON.stringify(response.data));
                        let branchList = `<option value="">Select branch</option>`;
                        for (let branch in branches) {
                            branchList +=
                                `<option value="${branch}">${branches[branch]}</option>`;
                        }
                        $('[name=branch_id]').html(branchList);
                    }
                });



            });

            $('.zoneByBranch').change(function(e) {
                let selectedValue = $(this).val();
                if (selectedValue == "") {
                    $('[name=zone_id]').html('<option>Select Zone</option>');
                }
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                jQuery.ajax({
                    url: `${APP_URL}/zoneList/${selectedValue}`,
                    dataType: "json",
                    success: function(response) {
                        let zones = JSON.parse(JSON.stringify(response.data));
                        let zoneList = `<option value="">Select Zone</option>`;
                        for (let zone in zones) {
                            zoneList += `<option value="${zone}">${zones[zone]}</option>`;
                        }
                        $('[name=zone_id]').html(zoneList);
                    }
                });
            });


            $('.zone-by-address').click(function(e) {
                let selectedValue = $(this).val();
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                jQuery.ajax({
                    url: `${APP_URL}/zoneList/${selectedValue}`,
                    dataType: "json",
                    success: function(response) {
                        let zones = JSON.parse(JSON.stringify(response.data));
                        let zoneList = `<option>Select Zone</option>`;
                        for (let zone in zones) {
                            zoneList += `<option value="${zone}">${zones[zone]}</option>`;
                        }
                        console.log('zoneList', zoneList);
                        $('[name=zone_id]').html(zoneList);
                    }
                });



            });

            $("#check_All").change(function() {
                $("input:checkbox.selected_item").prop('checked', $(this).prop("checked"));
            });


            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
            });
            $('.timepicker').timepicker({
                icons: {
                    up: 'fa fa-arrow-up',
                    down: 'fa fa-arrow-down'
                }
            });

            var screensize = document.documentElement.clientWidth;
            if (screensize > 640) {
                $(".sidebar-checkbox").prop('checked', true)
            } else {
                $(".sidebar-checkbox").prop('checked', false)
            }
        });


        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

        function popupFunction(event) {
            event = event || window.event;
            if (event == "occupancy_popup_info") {
                //popup = event+"_text";
                popup = document.getElementById("occupancy_popup_info_text");
                popup.classList.toggle("show");
            }
            if (event == "footfall_popup_info") {
                //popup = event+"_text";
                popup = document.getElementById("footfall_popup_info_text");
                popup.classList.toggle("show");
            }
            if (event == "violation_popup_info") {
                //popup = event+"_text";
                popup = document.getElementById("violation_popup_info_text");
                popup.classList.toggle("show");
            }
            // var popup = document.getElementById();
            // alert(popup);
            // popup.classList.toggle("show");
        }
    </script>

</head>

<body>
    <div id="mySidenav" class="sidenav">
        <input class="sidebar-checkbox" style="position: relative;z-index:100" checked type="checkbox" id="check">


        <div class="side-navbar print_media slim">

            <x-sidemenu controller="{{ $controller }}" action="{{ $action }}"></x-sidemenu>


        </div>
    </div>
    <!--/.side-navbar-->
    <main id="main">
        <nav class="navbar dashboad-navbar print_media">
            <div class="dash-navtoggle-bar mobile-view"><label for="check"><i
                        class="fas fa-align-justify"></i></label></a>
                </li>
            </div>
            <a class="navbar-brand" href="{{ url('/home') }}">
                <img class="logo" style="width:80px; margin:0 10px 0 0" src="{{ asset('assets/img/logo.png') }}?v=1"
                    alt="">
                <!-- {{ config('app.name', 'Laravel') }} -->
            </a>

            <ul class="navbar-nav top-right-navbar no-mobile-view">

                <li>

                    <div class="dropdown my-account">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('img/dashboard/account_user.png') }}" alt="User" width="20"
                                height="20" />{{ Auth::user()->name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ url('change-password') }}">Change Password</a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                        </div>
                    </div>
                    <!--/.drop-down-->
                </li>
            </ul>
            <ul class="navbar-nav top-right-navbar mobile-view dashboard-nav-right">
                <li>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('img/dashboard/account_user.png') }}" alt="User" width="20"
                                height="20" />{{ Auth::user()->name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                        </div>
                        <!--/.drop-down-menu-->
                    </div>
                    <!--/.drop-down-->
                </li>
            </ul>
        </nav>
        <div id="content-wrapper">
            <div class="container-fluid slide-content">
                <div class="child-container">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
