<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"> -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{{ asset('assets/img/fav.png') }}" type="image/png" sizes="16x16">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets/css/style.css') }}?t=<?php echo time(); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Raleway:wght@300;400;500;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700;800;900&family=Ovo&display=swap"
        rel="stylesheet">
    <script type="text/javascript" src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/jquery.validate.js') }}"></script>
</head>
{{-- <style>
.body{
	background:#EFF4F5 !important;
}
.header{
	background:#FFF !important;
}
</style> --}}

<body class="body">
    <header class="header border-bottom">
        <div class="top-nav-content">
            <div class="container">
                <div class="row">
                    <div class="col-3 col-md-1">
                        <img class="logo" style="width:100%;padding:13px 0;margin:0 20px 0 0" src="{{ asset('assets/img/logo.png') }}?v=1" alt="">
                    </div>
                    <div class="col-8 col-sm-8">
                        <div class="logo">
                            <a class="navbar-brand" style="margin-left:-20px" href="{{ url('/') }}">
                                {{ config('app.name', 'Laravel') }}
                            </a>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
        </div>
        <!--/.top-nav-content-->
    </header>
    <!--/.header-->
    <div class="content">
        @yield('content')
    </div>

    <footer class="footer footer-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <!-- <a class="footer-logo" href="index.php">
                        {{ config('app.name', 'Laravel') }}
                            </a> -->
                    <!-- <p class="w60" style="margin:1rem 0 5rem">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
            -->

                </div>
                <!-- <div class="col-sm-4">
      <p class="font-900">Get Social</p>
      <ul class="list-inline social-list">
       <li><a target="_blank" href="#"><img src="{{ asset('assets/img/facebook.png') }}" width="8" height="17" alt="Facebook"/></a></li>
       <li><a target="_blank" href="#"><img src="{{ asset('assets/img/youtube.png') }}" width="20" height="14" alt="Youtube"/></a></li>
       <li><a target="_blank" href="#"><img src="{{ asset('assets/img/linkedin.png') }} " width="15" height="15" alt="Linkdin"/></a></li>
      </ul>
     </div> -->
            </div>

            <!--/.row-->
            <!-- <div class="row">
     <div class="col-12 col-sm-12"><div class="ft-border-top"></div></div>
     <div class="col-12 col-sm-6">
      <ul class="list-inline footer-nav text-left">
       <li><a href="#">Home</a></li>
       <li><a href="#">About</a></li>
       <li><a href="#">Privacy Policy</a></li>
       <li><a href="#">Terms and Conditions</a></li>
      </ul>
     </div>
     <div class="col-12 col-sm-6">
      <p class="font-10 text-right sm-center">&#169 2020 All right resersed</p>
     </div>
    </div> -->
        </div>
        <!--/.container-->
    </footer>
    </div>
    <!--/.content-->
    <!-- Modal -->
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
 <h5 class="modal-title" id="exampleModalLongTitle">Video</h5>
 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
 <span aria-hidden="true">&times;</span>
 </button>
 </div> -->
                <div class="modal-body">
                    <video id="myVideo" width="100%">
                        <source src="video/mov_bbb.mp4" type="video/mp4">
                        <source src="video/mov_bbb.ogv" type="video/ogg">
                        Your browser does not support HTML video.
                    </video>
                </div>
            </div>
            <!--/.modal-content-->
        </div>
        <!--/.modal-dialog-->
    </div>
    <!--/.modal-->
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        var vid = document.getElementById("myVideo");

        function enableAutoplay() {
            vid.autoplay = true;
            vid.load();
        }
        $(".modal").click(function() {
            vid.autoplay = false;
            vid.load();
        }); //modal
        //document
        $(document).ready(function() {
            $(".tagAnimation li").click(function() {
                $(".tagAnimation li").removeClass("tag-sm-active");
                $(this).addClass("tag-sm-active");
                $(".tag-lg-img").hide();
                var tag = $(this).attr("data-img");
                $("." + tag).show();
            });
        }); //document
    </script>
</body>

</html>
