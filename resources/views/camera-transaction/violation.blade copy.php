@extends('layouts.admin')


@section('content')
 
<div class="row">
  <div class="col-12"> <div class="page-title padleft20">Occupancy Violation</div></div>
  <div class="col-12"><div class="bottom-border"></div></div>
</div><!--/.row-->
<div class="col-12">
  <form  action="" method="get" autocomplete="off">
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
      <div class="form-group col-xs-12 col-sm-12 col-md-3 ">
          {!! Form::select('zone_id',$zones, app('request')->input('zone_id'), array('placeholder'=>'Select Zone','class' => 'form-control')) !!}
    </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php 
            $filter_date =app('request')->input('filter_date') ;
            if($filter_date){
              $filter_date=date('d-m-Y',strtotime($filter_date));
            }
            ?>
          <input type="text" value="{{ $filter_date }}" class="form-control datepicker" name="filter_date" placeholder="" autocomplete="off">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
          <input type="submit" value="Search" class="btn btn-primary full-width">
        
        </div>
        

            

      </div><!--/.row-->  
    </div><!--/./col-6-->
 
  </div><!--/.row-->
</form>
</div><!--/.col-12-->


<div class="card">
  <div class="card-body">
    <table class="table table-striped table-responsive1 mt-4">
        <thead>
          <tr>
            <th>Slno</th>
            <th>Company Name</th> 
            <th>Branch Name</th> 
            <th>Zone Name</th> 
            {{-- <th>Device Name</th>  --}}
            <th>Start Time</th>
            <th>End Time</th> 
            <th>Duration(Min)</th> 
          </tr>

        </thead>
        <tbody>
        @forelse($data as $i=>$row)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$row->company_name}}</td>
              <td>{{$row->branch_name}}</td>
              <td>{{$row->zone_name}}</td>
              {{-- <td>{{$row->device_name}}</td> --}}
              <td>
                <?php
                  $violation_start=$row->violation_start;
                  if($violation_start){
                echo date('d M Y, h:i:s a',strtotime($violation_start));
              }
              
                  ?>
              </td>
              <td>
                <?php
                  $violation_end=$row->violation_end;
                  if($violation_end){
                echo date('d M Y, h:i:s a',strtotime($violation_end));
              }
              
                  ?>
              </td>

              <td>
                <?php
                  $violation_duration=$row->violation_duration;
                  if($violation_duration){
                 echo $violation_duration;
              }
              
                  ?>
              </td>
              

              
              
              </tr>

              @empty
              <tr>
              <td colspan="8">
                <p class="text-center text-danger">No record found.</p>
              </td>
              </tr>
@endforelse

          
        </tbody>
      </table>
    

 
</div>
</div>

@endsection
