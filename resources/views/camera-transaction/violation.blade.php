@extends('layouts.admin')

@section('content')

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script> --}}
    {{-- <script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script> --}}
    {{-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script> --}}



    <div class="row">
        <div class="col-12">
            <h5>Foot Fall </h5>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-md-12">
            <form action="" method="get" autocomplete="off">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-3 ">
                        {!! Form::select('zone_id', $data['zone_names'], app('request')->input('zone_id'), ['placeholder' => 'Select Zone', 'class' => 'form-control']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <?php
                        $filter_date = app('request')->input('filter_date');
                        if ($filter_date) {
                        $filter_date = date('d-m-Y', strtotime($filter_date));
                        }
                        ?>
                        <input type="text" id="filter_date" value="{{ $filter_date }}" class="form-control datepicker"
                            name="filter_date" placeholder="" autocomplete="off">
                    </div>
                    {{-- <div class="col-xs-12 col-sm-12 col-md-3">
                        <?php
                        if (app('request')->input('interval')) {
                            $selectedInterval = app('request')->input('interval');
                        }
                        $intervalLists = ['15' => '15 Minute', '30' => '30 Minute', '60' => '1 Hour'];
                        ?>
                        {!! Form::select('interval', $intervalLists, $selectedInterval, ['placeholder' => 'Select Interval', 'class' => 'form-control', 'id' => 'interval']) !!}

                    </div> --}}

                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <input type="submit" value="Search" class="btn btn-primary full-width">

                    </div>



                </div>

            </form>
        </div>
    </div>
    <!--/.col-12-->



    <?php
    $zones = $data['zones'];

    if ($zones) {
    foreach ($zones as $key => $zone) {
    $zone_id = $zone['id']; ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box radius-10">
                <div class="card-header">
                    <h6>{{ $zone['zone_name'] }} Overview</h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        <div id="zone_chart_{{ $zone['id'] }}" class="chart-container-1"></div>
                        {{-- <div id="watermark-{{ $zone['id'] }}" class="watermark none">No Data</div> --}}
                    </div>

                </div>

            </div>
        </div>
    </div>



    <?php
    }
    }
    ?>
    <script>
        <?php if ($zones) {
                foreach ($zones as $key => $zone) {

                    $file_url = $zone->file_url;
                    $zoneId = $zone->id;
                    ?>
        chartData = {
            ...highchartOption
        };
        //var updateChart = window[`zone_${zone_id}`];

        //window[`highChart_{{ $zoneId }}`].data.csvURL = occupancyFileUrl;
        chartData.data.csvURL = `{{ $file_url }}`;
        Highcharts.chart("zone_chart_{{ $zoneId }}", chartData);
        <?php
                }
            } ?>
    </script>








@endsection
