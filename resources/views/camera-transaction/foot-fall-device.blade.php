@extends('layouts.admin')

@section('content')

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script> --}}
    {{-- <script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script> --}}
    {{-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script> --}}



    <div class="row">
        <div class="col-12">
            <h5>{{ $zone_name }} - Foot Fall By All Gate</h5>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-md-12">
            <form action="" method="get" autocomplete="off">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-2">
                        {!! Form::select('zone_id', $data['camera_names'], app('request')->input('zone_id'), ['placeholder' => 'Select Device', 'class' => 'form-control']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <?php
                        $filter_date = app('request')->input('filter_date');
                        if ($filter_date) {
                            $filter_date = date('d-m-Y', strtotime($filter_date));
                        }
                        ?>
                        <input type="text" id="filter_date" value="{{ $filter_date }}" class="form-control datepicker"
                            name="filter_date" placeholder="" autocomplete="off">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <input type="submit" value="GO" class="btn btn-primary full-width">

                    </div>



                </div>

            </form>
        </div>
    </div>
    <!--/.col-12-->



    <?php
    $cameras = $data['cameras'];

    if ($cameras) {
    foreach ($cameras as $key => $camera) {
    $camera_id = $camera['id']; ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box radius-10">
                <div class="card-header">
                    <h6>{{ $camera['device_name'] }} {{ $camera['device_id'] }} Overview [ Foot Fall
                        <strong id="device_foot_fall_{{ $camera_id }}">{{ $device_foot_fall[$camera_id] }}</strong>]
                    </h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        <div id="chart_{{ $camera_id }}" class="chart-container-1"></div>
                        {{-- <div id="watermark-{{ $zone['id'] }}" class="watermark none">No Data</div> --}}
                    </div>

                </div>

            </div>
        </div>
    </div>



    <?php
    }
    }
    ?>
    <script>
        <?php if ($cameras) {
                foreach ($cameras as $key => $row) {

                    $file_url = $row->file_url;
                    $camera_id = $row->id;
                    ?>
        chartData = {
            ...highchartOption
        };
        chartData.data.csvURL = `{{ $file_url }}`;
        Highcharts.chart("chart_{{ $camera_id }}", chartData);
        <?php
                }
            } ?>

        let fetchFootFall = "{{ route('device_foot_fall') }}";
        fetchFootFall += "<?php echo $url; ?>";
        console.log('fetchFootFall', fetchFootFall);
        setInterval(() => {
            fetch(fetchFootFall)
                .then(response => response.json())
                .then(result => {
                    let response = JSON.stringify(result);
                    response = JSON.parse(response);
                    // console.log(response.data.foot_fall);
                    let foot_fall = response.data.foot_fall;
                    for (let deviceId in foot_fall) {
                        footFall = foot_fall[deviceId];
                        document.getElementById(`device_foot_fall_${deviceId}`).innerText = footFall;

                        console.log(footFall);
                    }
                });
        }, 2000);
    </script>








@endsection
