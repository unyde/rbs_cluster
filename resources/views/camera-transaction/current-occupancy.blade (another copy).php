@extends('layouts.admin')

@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
{{-- <script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script> --}}
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>



<div class="row">
  <div class="col-12"> <div class="page-title padleft20">Occupancy</div></div>
  <div class="col-12"><div class="bottom-border"></div></div>
</div><!--/.row-->
<div class="col-12">
  <form  action="" method="get" autocomplete="off">
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
      <div class="form-group col-xs-12 col-sm-12 col-md-3 ">
          {!! Form::select('zone_id',$zones, app('request')->input('zone_id'), array('placeholder'=>'Select Zone','class' => 'form-control')) !!}
    </div>

        <div class="col-sm-12 col-md-3">
          <?php 
            $filter_date =app('request')->input('filter_date') ;
            if($filter_date){
              $filter_date=date('d-m-Y',strtotime($filter_date));
            }
            ?>
          <input type="text" id="filter_date" value="{{ $filter_date }}" class="form-control datepicker" name="filter_date" placeholder="" autocomplete="off">
        </div>
        <div class="col-sm-12 col-md-3">
          <?php 
            
            if(app('request')->input('interval')){
              $selectedInterval=app('request')->input('interval');
            }
            $intervalLists=['15'=>'15 Minute','30'=>'30 Minute','60'=>'1 Hour'];
            ?>
         {!! Form::select('interval',$intervalLists, $selectedInterval, array('placeholder'=>'Select Interval','class' => 'form-control','id'=>'interval')) !!}

        </div>

        <div class="col-sm-12 col-md-3">
          <input type="submit" value="Search" class="btn btn-primary full-width">
        
        </div>
        

            

      </div><!--/.row-->  
    </div><!--/./col-6-->
 
  </div><!--/.row-->
</form>
</div><!--/.col-12-->


<div class="card">
  <div class="card-body">
    <?php
    $selectedTimeBreak="";

    if(is_array($zonesList['zones'])){
      foreach ($zonesList['zones'] as $key => $zone) {
       
       ?>
       <div class="content-box white-bg box-radius max-height">
        <div class="container-fluid">
    <div style="height: 200px">
      <canvas id="zone_{{$zone['id']}}" style="height: 200px"></canvas>
    </div>
   <h3 class="graph_title"> {{$zone['zone_name']}}</h3>
        </div></div>
    <?php } }?>
    

    <script>
      var options = {
  maintainAspectRatio: false,
  scales: {
    yAxes: [{
      afterBuildTicks: (x) => {
        console.log(x);
      },
      ticks: {
        // callback: (value) => {
        //   console.log(value);
        //   return value;
        // },
        beginAtZero: true,
        callback: function(value) {if (value % 1 === 0) {return value;}}
      },
    }]
  }
};
let labels=[];
  let zoneChart;
  let data=[];
    

<?php
$slots="";

  if(is_array($timeSlots)){
      $slots=$timeSlots['slots'];
      if(is_array($slots)){
        foreach($slots as $key=>$item){
       
          ?>
          labels.push('{{$item}}');
          console.log(labels);
          <?php
        }
      }
  }
?>
window.onload = function() {

  <?php 
  
 
  
 
  if(is_array($zonesList['zones'])){
      foreach ($zonesList['zones'] as $key => $zone) {
        ?>
          data=[];
         
          <?php
        $zone_id= $zone['id'];
        $zone_data=$zoneData[$zone_id];
        if(is_array($zone_data)){
          foreach($zone_data as $k=>$v){
          ?>
          data.push('{{$v}}');
         
          <?php
        }}
       ?>
       zoneChart = document.getElementById("zone_{{$zone['id']}}").getContext("2d");
  zone_{{$zone['id']}} = new Chart(zoneChart, {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
    label: "Occupancy",
    backgroundColor:'rgba(251, 85, 85, 0.4)',
    data: data
  }]
    },
    options: options
  });
 
 <?php }}?>

};
function addData(chart, label, data) {
  // console.log('Update');
  // zone_13='zone_'+chart;
  // zone_13.data.labels.push(label);
  // zone_13.data.datasets.forEach((dataset) => {
  //       dataset.data.push(data);
  //   });
  //   zone_13.update();

  // zoneChart = document.getElementById("zone_13").getContext("2d");
  //  new Chart(zoneChart, {
  //   type: 'bar',
  //   data: {
  //     labels: labels,
  //     datasets: [{
  //   label: "Occupancy",
  //   backgroundColor:'rgba(251, 85, 85, 0.4)',
  //   data: [1,2]
  // }]
  //   },
  //   options: options
  // });


}


// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;
  var pusher = new Pusher('7a79691af7c99ed6f2e7', {
    cluster: 'ap2'
  });

  var channel = pusher.subscribe('my-channel');
  channel.bind('home', function(data) {
    console.log("Pusher Data",data);
    //alert(JSON.stringify(data));
    let response = JSON.stringify(data);
    response = JSON.parse(response);
    console.log('res',response);

    //Zone current occupancy update    
    let zone_id=response['zone_id'];
    let filter_date=document.getElementById('filter_date').value;
    let interval=document.getElementById('interval').value;
    fetch(`getZoneOccupancy?zone_id=${zone_id}&selectedDate=${filter_date}&interval=${interval}`)
    .then(response => response.json())
    .then(result=>{
      //console.log("Api Response",result);
      let response = JSON.stringify(result);
      response = JSON.parse(response);
      //console.log("Api Response",response['data']);
      let data=response['data'];
    
      if(Array.isArray(data)){
         console.log('Tes',data);
      }
      console.log('ZONEID',zone_id);
     ////// zone_`${zone_id}`.destroy();
     newZone=`zone_${zone_id}`;
      zoneChart = document.getElementById(`zone_${zone_id}`).getContext("2d");
      newZone=new Chart(zoneChart, {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
    label: "Occupancy",
    backgroundColor:'rgba(251, 85, 85, 0.4)',
    data: data
  }]
    },
    options: options
  });

    })  
  });



    </script>



 
</div>
</div>

@endsection
