@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12">
            <h5> Occupancy Violation</h5>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-md-12">
            <form action="" method="get" autocomplete="off">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-12 col-md-2 ">
                        {!! Form::select('zone_id', $data['zone_names'], app('request')->input('zone_id'), ['placeholder' => 'Select Zone', 'class' => 'form-control']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <?php
                        $filter_date = app('request')->input('filter_date');
                        if ($filter_date) {
                            $filter_date = date('d-m-Y', strtotime($filter_date));
                        }
                        ?>
                        <input type="text" id="filter_date" value="{{ $filter_date }}" class="form-control datepicker"
                            name="filter_date" placeholder="" autocomplete="off">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <input type="submit" value="GO" class="btn btn-primary full-width">

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--/.col-12-->

    <div class="row">
        <div class="col-12">
        <div class="card zoom-card panel-box radius-10">
            <div class="card-body">
                <p class="no-bottom-margin zoom-info"><span class="bold-text">Zoomable Statistic</span> : Select the range in the chart you want to zoom in</p>
            </div>
        </div>
        </div>
    </div>

    <?php
    $zones = $data['zones'];

    if ($zones) {
    foreach ($zones as $key => $zone) {
    $zone_id = $zone['id']; ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box radius-10">
                <div class="card-header">
                    <h6>{{ $zone['zone_name'] }} Overview
                        @if (!$zone['max_occupancy'])
                            | <a href="{{ route('zone.edit', $zone_id) }}">Set Threshold</a>
                        @endif
                    </h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1">
                        @if ($zone['max_occupancy'])
                            <div id="zone_chart_{{ $zone['id'] }}" class="chart-container-1"></div>
                        @else
                            <div id="watermark-{{ $zone['id'] }}" class="watermark">Max threshold not set</div>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>



    <?php
    }
    }
    ?>
    <script>
        <?php if ($zones) {
                foreach ($zones as $key => $zone) {

                    $file_url = $zone->file_url;
                    $zoneId = $zone->id;
                    ?>
        chartData = {
            ...highchartOption
        };
        //var updateChart = window[`zone_${zone_id}`];

        //window[`highChart_{{ $zoneId }}`].data.csvURL = occupancyFileUrl;
        chartData.data.csvURL = `{{ $file_url }}`;
        Highcharts.chart("zone_chart_{{ $zoneId }}", chartData);
        <?php
                }
            } ?>
    </script>








@endsection
