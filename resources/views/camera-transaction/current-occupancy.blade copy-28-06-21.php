@extends('layouts.admin')

@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script>


<div class="row">
  <div class="col-12"> <div class="page-title padleft20">Occupancy</div></div>
  <div class="col-12"><div class="bottom-border"></div></div>
</div><!--/.row-->
<div class="col-12">
  <form  action="" method="get" autocomplete="off">
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
      <div class="form-group col-xs-12 col-sm-12 col-md-3">
          {!! Form::select('zone_id',$zones, app('request')->input('zone_id'), array('placeholder'=>'Select Zone','class' => 'form-control')) !!}
    </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php 
            $filter_date =app('request')->input('filter_date') ;
            if($filter_date){
              $filter_date=date('d-m-Y',strtotime($filter_date));
            }
            ?>
          <input type="text" value="{{ $filter_date }}" class="form-control datepicker" name="filter_date" placeholder="" autocomplete="off">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
          <input type="submit" value="Search" class="btn btn-primary full-width">
        
        </div>
        

            

      </div><!--/.row-->  
    </div><!--/./col-6-->
 
  </div><!--/.row-->
</form>
</div><!--/.col-12-->


<div class="card">
  <div class="card-body">
    <table class="table table-striped table-responsive1 mt-4">
        <thead>
          <tr>
            <th>Slno</th>
            <th>Company Name</th> 
            <th>Branch Name</th> 
            <th>Zone Name</th> 
            <th>Max Occupany</th>
            <th>Occupancy</th> 
            {{-- <th>Date</th> --}}
          </tr>

        </thead>
        <tbody>

      <script>
                            
        $(document).ready(function(){
        
        $(".green").easyPieChart({
                size:50,
                barColor : '#008000',
                scaleColor:false,
                lineWidth:6,
                trackColor:'#dcdcdc',
                lineCap:'circle',
                animate:1700
              });
        
              $(".red").easyPieChart({
                size:50,
                barColor : '#FF0000',
                scaleColor:false,
                lineWidth:6,
                trackColor:'#dcdcdc',
                lineCap:'circle',
                animate:1700
              });
              
              
        });
        
                                    </script>

        @forelse($data as $i=>$row)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$row['company_name']}}</td>
              <td>{{$row['branch_name']}}</td>
              <td>{{$row['zone_name']}}</td>
              <td>{{$row['max_occupancy']}}</td>
              <td>
                <?php
                $current=$row['occupancy_percentage'];
                $colorCode=" green";
                if($current>100){
                  $colorCode=" red";
                }
                  ?>
                <div class="pie-chart-box occupany-list">
                  <div id="zone" class="chart {{$colorCode}}" data-percent="{{$current}}">
                    <span class="zone_count">{{$current}}%</span>
                          </div>
                </div>

              </td>
              
              
              </tr>

              @empty
              <tr>
              <td colspan="8">
                <p class="text-center text-danger">No record found.</p>
              </td>
              </tr>
@endforelse

          
        </tbody>
      </table>
    

 
</div>
</div>

@endsection
