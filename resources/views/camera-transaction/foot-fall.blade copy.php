@extends('layouts.admin')

@section('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    {{-- <script src="{{ asset('bootstrap/js/jquery.easypiechart.min.js')}}"></script> --}}
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>



    <div class="row">
        <div class="col-12">
            <h5>Foot Fall</h5>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-md-12">
            <form action="" method="get" autocomplete="off">
                <div class="row">
                    <div class="form-group col-xs-6 col-sm-3 col-md-3 ">
                        {!! Form::select('zone_id', $zones, app('request')->input('zone_id'), ['placeholder' => 'Select Zone', 'class' => 'form-control']) !!}
                    </div>

                    <div class="col-sm-3">
                        <?php
                        $filter_date = app('request')->input('filter_date');
                        if ($filter_date) {
                        $filter_date = date('d-m-Y', strtotime($filter_date));
                        }
                        ?>
                        <input type="text" id="filter_date" value="{{ $filter_date }}" class="form-control datepicker"
                            name="filter_date" placeholder="" autocomplete="off">
                    </div>
                    <div class="col-sm-3">
                        <?php
                        if (app('request')->input('interval')) {
                        $selectedInterval = app('request')->input('interval');
                        }
                        $intervalLists = ['15' => '15 Minute', '30' => '30 Minute', '60' => '1 Hour'];
                        ?>
                        {!! Form::select('interval', $intervalLists, $selectedInterval, ['placeholder' => 'Select Interval', 'class' => 'form-control', 'id' => 'interval']) !!}

                    </div>

                    <div class="col-sm-3">
                        <input type="submit" value="Search" class="btn btn-primary full-width">

                    </div>



                </div>

            </form>
        </div>
    </div>



    <?php
    $selectedTimeBreak = '';

    if (is_array($zonesList['zones'])) {
    foreach ($zonesList['zones'] as $key => $zone) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card panel-box-width panel-box radius-10">
                <div class="card-header">
                    <h6> {{ $zone['zone_name'] }}- {{ $zone['id'] }} Overview</h6>
                </div>
                <div class="card-body">
                    <div class="chart-container-1" id="zone_container_{{ $zone['id'] }}">
                        <canvas id="zone_{{ $zone['id'] }}"></canvas>
                        <div id="watermark-{{ $zone['id'] }}" class="watermark none">No Data</div>

                    </div>

                </div>

            </div>
        </div>
    </div>


    <?php }
    }
    ?>


    <script>
        var options = {
            ...chartOptions
        };
        let labels = [];
        let zoneChart;
        let data = [];


        <?php
            $slots = '';

            if (is_array($timeSlots)) {
                $slots = $timeSlots['slots'];
                if (is_array($slots)) {
                    foreach ($slots as $key => $item) { ?>
        labels.push('{{ $item }}');
        // console.log(labels);
        <?php }
                }
            }
            ?>
        window.onload = function() {

            <?php if (is_array($zonesList['zones'])) {
                    foreach ($zonesList['zones'] as $key => $zone) { ?>
            data = [];

            <?php
                $zone_id = $zone['id'];
                $zone_data = $zoneData[$zone_id];
                if (is_array($zone_data)) {
                    foreach ($zone_data as $k => $v) { ?>
            data.push('{{ $v }}');

            <?php }
                }
                ?>
            displayWaterMark('watermark-{{ $zone_id }}', data);
            zoneChart = document.getElementById("zone_{{ $zone['id'] }}").getContext("2d");
            zone_{{ $zone['id'] }} = new Chart(zoneChart, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: "Foot Fall",
                        backgroundColor: `${CHART_COLOR}`,
                        data: data
                    }]
                },
                options: options
            });

            <?php }
                } ?>

        };



        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;
        var pusher = new Pusher('{{ Config::get('app.pusher_app_key') }}', {
            cluster: 'ap2'
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('home', function(data) {
            //console.log("Pusher Data", data);
            //alert(JSON.stringify(data));
            let response = JSON.stringify(data);
            response = JSON.parse(response);
            // console.log('res', response);

            //Zone current occupancy update    
            let zone_id = response['zone_id'];
            // console.log("Zone", zone_id);
            // console.log("new Zone", response['new_zone_id']);
            let filter_date = document.getElementById('filter_date').value;
            let interval = document.getElementById('interval').value;
            let zoneContainerId = document.getElementById(`zone_container_${zone_id}`);
            if (zoneContainerId) {
                console.log('Zone found', zone_id);
                fetch(
                        `getZoneOccupancy?zone_id=${zone_id}&selectedDate=${filter_date}&interval=${interval}&dataType=in`
                    )
                    .then(response => response.json())
                    .then(result => {
                        let response = JSON.stringify(result);
                        response = JSON.parse(response);
                        let data = response['chart'];
                        // console.log('Response Data', data);

                        // if (Array.isArray(data)) {
                        //     console.log('Tes', data);
                        // }

                        var updateChart = window[`zone_${zone_id}`];

                        // console.log('Get chart data', updateChart);
                        if (updateChart && updateChart.data) {
                            console.log('Yes');
                            displayWaterMark(`watermark-${zone_id}`, data);
                            updateChart.data.datasets.forEach((dataset) => {
                                data.forEach((element, index) => {
                                    if (dataset.data[index] != element) {
                                        dataset.data[index] = element;
                                    }
                                });
                                updateChart.update();
                            })
                        };
                    })
            } else {
                console.log('Zone not found');
            }

        });
    </script>


@endsection
