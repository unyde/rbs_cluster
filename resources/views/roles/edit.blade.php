@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title ">Roles Management</h5>
        </div>
        <div class="col-2 text-right">
            <a href="{{ route('roles.index') }}" class=" btn-primary btn  ">Back </a>
        </div>

    </div>

    @if (count($errors) > 0)
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert-message alert alert-danger">
                    <Label>Whoops!</Label> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif


    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Edit Role</h6>
                </div>

                <div class="card-body">

                    {!! Form::model($role, ['method' => 'PATCH', 'class' => 'form', 'route' => ['roles.update', $role->id]]) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name<span class="star">*</span></strong>
                                {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Permission<span class="star">*</span></strong>
                                <br />
                                <?php if (is_array($permissions) && count($permissions)) {
                                foreach ($permissions as $key => $permission) { ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $key; ?>
                                        <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="panel-body">
                                        <?php if (is_array($permission) && count($permission)) {
                                        foreach ($permission as $value) { ?>
                                        <label
                                            class="label-permission">{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, ['class' => 'name']) }}
                                            {{ $value->name }}</label>
                                        <?php }
                                        } ?>

                                    </div>
                                    <!-- /.box-body -->
                                </div><!-- /.box -->
                                <?php }
                                } ?>

                                <div class="form-group ">
                                    <strong>Status </strong>
                                    {!! Form::select('status', [1 => 'Active', 0 => 'Inactive'], null, ['class' => 'form-control']) !!}


                                </div>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function() {


                            $('.form').validate({
                                rules: {
                                    name: {
                                        required: true
                                    },
                                    email: {
                                        required: true
                                    },
                                    password: {
                                        required: true,
                                        minlength: 6,
                                        maxlength: 15,
                                    },
                                    'confirm-password': {
                                        required: true
                                    },
                                    roles: {
                                        required: true
                                    }
                                }
                            });
                        });
                    </script>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
