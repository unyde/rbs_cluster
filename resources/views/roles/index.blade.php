@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-10">
            <h5 class="page-title ">Role Management</h5>
        </div>
        @can('role-create')
            <div class="col-2 text-right">
                <a href="{{ route('roles.create') }}" class=" btn-primary btn  ">Add New Role </a>

            </div>
        @endcan
    </div>



    @if ($message = Session::get('success'))
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="alert alert-message alert-success">
                    {{ $message }}
                </div>
            </div>
        </div>

    @endif





    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card panel-box radius-10">
                <div class="card-header">
                    <h6>Add New Role</h6>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-responsive1 mt-4">
                        <thead>
                            <tr>
                                <th>Slno</th>
                                <th>Role Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            <input type="hidden" name="posted" value="1">

                            @foreach ($roles as $key => $role)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>

                                        <?php
                                        $status = $role->status;
                                        if ($status) { ?>
                                        <span for="" class="badge badge-success">active</span>
                                        <?php } else { ?>
                                        <span for="" class="badge badge-danger">Inactive</span>

                                        <?php }
                                        ?>

                                    </td>


                                    <td class="action">


                                        @can('role-edit')
                                            <a href="{{ route('roles.edit', $role->id) }}">
                                                <i class="fas fa-pencil"></i> Edit
                                            </a>
                                        @endcan
                                        &nbsp;&nbsp;&nbsp;


                                        @can('role-delete')
                                            <!-- Delete action -->
                                            <a class="delete-icon" data-toggle="modal"
                                                data-target="#delete<?php echo $role->id; ?>"></a>
                                            <div class="modal fade pp-modal"
                                                id="delete<?php echo $role->id; ?>" tabindex="-1"
                                                role="dialog" aria-labelledby="ppModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <div class="pp-icon"><i class="fas fa-question-square"></i></div>
                                                            <h5 class="modal-title">Confirm</h5>
                                                            <p>Are you sure, You want to delete <br> this record.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="btn-list" style="display:block;">
                                                                {!! Form::open(['method' => 'DELETE', 'class' => 'delete', 'route' => ['roles.destroy', $role->id]]) !!}
                                                                {!! Form::submit('Yes, Delete', ['class' => 'btn btn-danger ok']) !!}
                                                                {!! Form::close() !!}

                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End delete action  -->
                                        @endcan


                                    </td>

                                </tr>

                            @endforeach


                        </tbody>
                    </table>
                    <div class="pull-right">
                        {!! $roles->render() !!}
                    </div>


                </div>
            </div>

        @endsection
